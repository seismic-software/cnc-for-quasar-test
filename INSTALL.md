
### Installation Procedure

1. Download the latest installtion package from cnc-for-quasar repo.  It is a tar.gz package located at [CNC Release Location](https://code.usgs.gov/asl/q330/cnc-for-quasar/-/releases).  Open the latest release and you should see the download link under the "Other" assets section, it will be a tar.gz file in the form cnc-quasar-v###.tar.gz.
2. This code requires PYTHON 3.+ to be installed on slate.
    use `sudo yum install python3.x86_64 python3-devel.x86_64 python3-setuptools.noarch` to install PYTHON 3.
3. Install virtualenv using python3, this command requires root permission to modify install location  `sudo python3 -m pip install virtualenv`
4. Edit maint users `~/.bashrc file` and add venv PATH and PYTHONPATH pointing to `/opt/cnc`:
   * `export PATH=$PATH:~/.local/bin:/opt/cnc/bin`
   * `export PYTHONPATH=$PYTHONPATH:/opt/cnc`
5. Create a directory at `/opt/cnc`  `sudo mkdir cnc` initially will require sudo as opt is owned by root.
6. Change ownership to maint `sudo chown maint:maint cnc`
7. Extract the installaion files from the tar.gz installion package.
   * `cp cnc-quasar-v2.1.0.tar.gz /opt/cnc`
   * `cd /opt/cnc`
   * `gunzip cnc-quasar-v2.1.0.tar.gz`
   * `tar xvf cnc-quasar-v2.1.0.tar`
8. Change directory to code location `cd cnc-quasar-v2.1.0`
9. run setup.bash `./setup.bash`
    This bash file should:
    1. create a new python3 virtual environment under venv directory
    2. install pip and python setup modules
    3. install pyserial for serial port communication
    4. install quasar module, this is in `venv/lib/python3.6/site-packages/quasar-1.0.0-py3.6.egg`

### SSL Errors
If you are on the DOI network and are getting SSL or cert errors when attempting to load PYTHON modules then use the following process.
1. If you don't have sudo on ASP then log in as root as you will need root permissions.
2. Go out to `/APPS/src/internal-ssl-setup/` (also avaiable at https://code.usgs.gov/asl/internal-ssl-setup) and copy the following files to the ASP:
   * DOIIMCA22.cer
   * DOIRootCA2.cer
   * fix1_general_setup.bash
   * fix3_pip_conf_global_setup.bash
3. Run `./fix1_general_setup.bash` this puts the cert files in the proper place on the system.
4. Run `./fix3_pip_conf_global_setup.bash` this points pip to the proper cert file locations.
