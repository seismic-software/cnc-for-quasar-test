# CNC for Quasar

These are command line scripts to interface with remote Q330 Digitizers.  

See [Slate Command & Control.doc](Slate%20Command%20&amp;%20Control.doc) for valid commands.

### Slate Installation

See [INSTALL.md](INSTALL.md)

### Build ASP Installation Package

1. Update version number in code.json and check code.json into repo, the release build script copies the version into QVersion.py for display in the scripts and also uses the version number in creating the installation package.   
2. In the [cnc-for-quasar repo](https://code.usgs.gov/asl/q330/cnc-for-quasar) create a Tag on the current version [CNC Tag Location](https://code.usgs.gov/asl/q330/cnc-for-quasar/-/tags). Name the tag with the version number, like cnc-quasar-release-v#.#.#.  When you create a tag it will kick off a process to create a release. The release will be a tar.gz file with all the required installation files.  This can be downloaded from [CNC Release Location](https://code.usgs.gov/asl/q330/cnc-for-quasar/-/releases). 
