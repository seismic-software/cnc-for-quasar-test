## =======================================
## === Q330 Supplemental Configuration ===
## =======================================

#User_Tag=RUBS1   # [0-9A-Z]{0-6}

#GPS_Power_Mode=MAX_TIME_OR_GPS_LOCK   # CONTINUOUS | MAX_TIME | MAX_TIME_OR_PLL_LOCK | MAX_TIME_OR_GPS_LOCK
#GPS_Max_On_Time=30 # minutes

## === Sensor Control Mapping ======
## <sensor>, <polarity>, <action>
## sensor:   A, B
## polarity: HIGH, LOW
## action:   CALIBRATION, CENTERING, CAPACITIVE, LOCK, UNLOCK, AUX1, AUX2
## 
## If there is not line for a definition, the existing definition in 
## the Q330 will be retained
##
## If a definition is supplied with no arguments, it will be set 
## to 0 (No action, active-low) in the mapping
## 
## Examples:
## Sensor_Control_1 = A, HIGH, CALIBRATION   (Sensor A, active high, cal. enables line)
## Sensor_Control_2 =                        (Definition will be set to 0)
##
#Sensor_Control_1 = A, HIGH, CALIBRATION
#Sensor_Control_2 = 
#Sensor_Control_3 =
#Sensor_Control_4 = A, HIGH, CENTERING
#Sensor_Control_5 =
#Sensor_Control_6 =
#Sensor_Control_7 =
#Sensor_Control_8 =

## === Base Port ===================
## Range is 1 - 65430
##
#Base_Port=5330

## === Ethernet Port ===============
#Eth_IP_Address=192.168.0.2
#Eth_MAC_Address=00:11:22:33:44:55
#Eth_Data_Port=1
#Eth_POC_IP_Address=
#Eth_Block_Non_POC_ICMP=NO
#Eth_Block_Non_POC_TCP=NO
#Eth_Block_Non_POC_QDP=NO
#Eth_Baler_Power_Mode=CONTINUOUS   # NO_BALER | DTR_CONTROL | BREAK_CONTROL | CONTINUOUS

## === Serial Ports ================
#S1_IP_Address=10.1.1.1
#S1_Data_Port=4
#S1_POC_IP_Address=10.1.1.2
#S1_Block_Non_POC_ICMP=NO
#S1_Block_Non_POC_TCP=NO
#S1_Block_Non_POC_QDP=NO
#S1_Baler_Power_Mode=CONTINUOUS   # NO_BALER | DTR_CONTROL | BREAK_CONTROL | CONTINUOUS

#S2_IP_Address=10.2.2.1
#S2_Data_Port=4
#S2_POC_IP_Address=10.2.2.2
#S2_Block_Non_POC_ICMP=NO
#S2_Block_Non_POC_TCP=NO
#S2_Block_Non_POC_QDP=NO
#S2_Baler_Power_Mode=CONTINUOUS   # NO_BALER | DTR_CONTROL | BREAK_CONTROL | CONTINUOUS

#S3_IP_Address=10.3.3.2
#S3_Data_Port=4
#S3_POC_IP_Address=10.3.3.1
#S3_Block_Non_POC_ICMP=NO
#S3_Block_Non_POC_TCP=NO
#S3_Block_Non_POC_QDP=NO
#S3_Baler_Power_Mode=CONTINUOUS   # NO_BALER | DTR_CONTROL | BREAK_CONTROL | CONTINUOUS

## === Data Ports ==================
#DP1_Freeze_Output=NO    # YES | NO
#DP1_Freeze_Buffering=NO # YES | NO
#DP1_Buffer_Oldest=OFF   # ON | OFF
#DP1_Hot_Swap=OFF        # ON | OFF
#DP1_Base_96=OFF         # ON | OFF
#DP1_Network=XX
#DP1_Station=TEST

#DP2_Freeze_Output=NO    # YES | NO
#DP2_Freeze_Buffering=NO # YES | NO
#DP2_Buffer_Oldest=OFF   # ON | OFF
#DP2_Hot_Swap=OFF        # ON | OFF
#DP2_Base_96=OFF         # ON | OFF
#DP2_Network=XX
#DP2_Station=TEST

#DP3_Freeze_Output=NO    # YES | NO
#DP3_Freeze_Buffering=NO # YES | NO
#DP3_Buffer_Oldest=OFF   # ON | OFF
#DP3_Hot_Swap=OFF        # ON | OFF
#DP3_Base_96=OFF         # ON | OFF
#DP3_Network=XX
#DP3_Station=TEST

#DP4_Freeze_Output=NO    # YES | NO
#DP4_Freeze_Buffering=NO # YES | NO
#DP4_Buffer_Oldest=OFF   # ON | OFF
#DP4_Hot_Swap=OFF        # ON | OFF
#DP4_Base_96=OFF         # ON | OFF
#DP4_Network=XX
#DP4_Station=TEST


## === Info Channels ===============
## 
## The Info Channels contain messages, time, and configuration information.
##
## MsgLog>[dataport],[location],[channel]
## TimeLog>[dataport],[location],[channel]
## CfgStream>[dataport],[location],[channel]
##
##   dataport:  1-4  (if blank, applies to all dataports)
##   location:  desired location code  (if blank, the existing value is retained)
##   channel:   desired channel name  (if blank, the existing value is retained)
##
#MsgLog    > 1, 91, LOG
#MsgLog    > 2, 91, LOG
#MsgLog    > 4, 91, LOG
#TimeLog   > , 91, ACE
#CfgStream > , 91, OCF


## === LCQs ========================
## LCQ>[dataport],[location],<channel>,[source],[rate],[telemeter],[new_location],[new_channel]
##
## dataport:     1-4  (if blank, applies to all dataports)
## location:     wildcard expression  (if blank, matches channels without a location code)
## channel:      wildcard exprsesion  (cannot be blank)
## source:       1-6  (if blank, the current source is used)
## rate:         1, 10, 20, 40, 50, 100, 200  (if blank, the current rate is used)
## telemeter:    on/off  (if blank, the current state is kept)
## new_location: new SEED location code  (if blank, the existing value is retained)
## new_channel:  new SEED channel name  (if blank, the existing value is retained)
##
## EXAMPLES:
##
##LCQ > , *, BH?, , 40, on, ,
## For all data ports for any location code where the channel name begins 
## with BH, use the current source, change rate to 40 Hz, and switch telemetry on
##
##LCQ > , , BC?, 4, 40, on, ,
## For all data ports where the LCQ has no location code and channel name
## begins with BC, use channel 4 as the source, set the rate to 40 Hz, 
## and switch telemetry on
##
##LCQ > 1, 10, BHZ, 4, 20, on, 11, SHZ
## For data port 1 where location code is 10 and channel is BHZ,
## use channel 4 as source, set rate to 20 Hz, switch telemetry on, 
## set new location code to 11, set new channel name to SHZ

#LCQ >  ,  *, BH?,  , 40, on, ,
#LCQ >  ,  *, LH?,  ,   , on, ,
#LCQ >  ,  *, VH?,  ,   , on, ,
#LCQ >  ,   , BC?,  , 40, on, ,
#LCQ >  , 9?,   *,  ,   , on, ,

