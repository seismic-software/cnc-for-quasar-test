#!/usr/bin/python -O
import os
import shutil
import sys
import clean
import compile
from build import utils

clean_dirs = [ 
    'bin',
    'CnC',
    'build'
]

compile_dirs = []

copy_dirs_compiled = []

copy_dirs_uncompiled = [
    ('CnC',    'CnC'    , 'build/CnC/CnC')
]

copy_dirs = [
    ('CnC', 'bin' , 'build/CnC/bin')
]

copy_files = [
    ('CnC',    'build/install_CnC.py'    , 'build/CnC/install_CnC.py'),
    ('CnC',    'build/install.py'        , 'build/CnC/install.py'),
    ('CnC',    'build/utils.py'          , 'build/CnC/utils.py')
]

archives = [
    ('CnC',    'CnC.tar.bz2'    , 'build' , 'CnC')
]

version_file = 'build/CnC/CnC/__version__.py'

class ExMessage(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return str(self.value)

def ignore_uncompiled(directory, file_list):
    ignore = []
    for file_name in file_list:
        full_name = "%s/%s" % (directory, file_name)
        if ((len(file_name) > 3) and (file_name[-3:] in ['.py'])):
            continue
        elif (file_name != '.svn') and os.path.isdir(full_name):
            continue
        ignore.append(file_name)
    return ignore

def ignore_compiled(directory, file_list):
    ignore = []
    for file_name in file_list:
        full_name = "%s/%s" % (directory, file_name)
        if file_name[0:11] == '__init__.py':
            if file_name[-4:] not in ['.pyc', '.pyo']:
                continue
        elif ((len(file_name) > 4) and (file_name[-4:] in ['.pyc', '.pyo'])):
            continue
        elif (file_name != '.svn') and os.path.isdir(full_name):
            continue
        ignore.append(file_name)
    return ignore

def ignore_svn(direcotyr, file_list):
    ignore = []
    for file_name in file_list:
        full_name = "%s/%s" % (directory, file_name)
        if (file_name != '.svn'):
            continue
        ignore.append(file_name)
    return ignore


if __name__ == "__main__":

    try:
        package_all = False
        package_ids = {}
        if len(sys.argv) < 2:
            package_all = True
        else:
            for id in sys.argv[1:]:
                if id == 'all':
                    package_all = True
                    break
                else:
                    package_ids[id] = True

        master_dir = os.getcwd()
        
        try:
            print("Cleaning...")
            for directory in clean_dirs:
                clean.clean_dir(directory)
        except IOError as e:
            raise ExMessage("Cleaning failed. %s" %  e.__str__())

        try:
            print("Compiling...")
            for directory in compile_dirs:
                compile.compile_dir(directory)
        except IOError as e:
            raise ExMessage("Compile failed. %s" %  e.__str__())

        try:
            print("Copying compiled directories...")
            for (id, source, destination) in copy_dirs_compiled:
                if (not package_all) and (id not in package_ids):
                    continue
                if os.path.exists(destination):
                    shutil.rmtree(destination)
                utils.copy_tree(source, destination, symlinks=True, ignore=ignore_compiled)
        except IOError as e:
            raise ExMessage("Directory Copy failed. %s" % e.__str__())
            
        try:
            print("Copying uncompiled directories...")
            for (id, source, destination) in copy_dirs_uncompiled:
                if (not package_all) and (id not in package_ids):
                    continue
                if os.path.exists(destination):
                    shutil.rmtree(destination)
                utils.copy_tree(source, destination, symlinks=True, ignore=ignore_uncompiled)
        except IOError as e:
            raise ExMessage("Directory Copy failed. %s" % e.__str__())
            
        try:
            print("Copying directories...")
            for (id, source, destination) in copy_dirs:
                if (not package_all) and (id not in package_ids):
                    continue
                if os.path.exists(destination):
                    shutil.rmtree(destination)
                utils.copy_tree(source, destination, symlinks=True, ignore=ignore_svn)
        except IOError as e:
            raise ExMessage("Directory Copy failed. %s" % e.__str__())

        try:
            print("Copying individual files...")
            for (id, source, destination) in copy_files:
                if (not package_all) and (id not in package_ids):
                    continue
                shutil.copy(source, destination)
        except IOError as e:
            raise ExMessage("File Copy failed. %s" % e.__str__())

        try:
            version_string = os.popen("git describe --tags --always").readlines()[-1].strip()
            print("Creating version file... (%s)")
            open(version_file, 'w+').write("version=\"%s\"\n" % version_string)
        except IOError as e:
            raise ExMessage("Could not create version file. %s" % str(e))

        try:
            print("Generating Package(s)...")
            for (id, pkg, dir, source) in archives:
                if (not package_all) and (id not in package_ids):
                    continue
                package   = master_dir + '/' + pkg
                directory = master_dir + '/' + dir
                package_command = "cd %s; tar cvjf %s %s" % (directory, package, source)
                md5_command = "md5sum %s" % package

                result = os.popen(package_command).read()
                if len(result):
                    print(result)
                result = os.popen(md5_command).read()
                if len(result):
                    print(result)

        except IOError as e:
            raise ExMessage("Copy failed. %s" % e.__str__())

        print("Done.")
    except ExMessage as e:
        print("E:", e.__str__())

