#!/usr/bin/python -O

import CnC.autocal_sequences as seq
from CnC.autocal_sequences import CalibrationSequences

#try:
#    seq.verify_sequences()
#except Exception, e:
#    print str(e)

for (sensor, sequence) in list(CalibrationSequences.items()):
    if sensor == 'END':
        continue
    print("%s:" % sensor)
    for cal in sequence:
        if 'comment' in cal or 'END' in cal:
            continue
        if cal['waveform'] == 'sine':
            print("  %(waveform)s cal. @ %(frequency)s Hz, amp. %(amplitude)s dB, duration=%(duration)s, settle=%(settle)s, sleep=%(sleep)s, trailer=%(trailer)s" % cal)
        elif cal['waveform'] == 'step':
            print("  %(waveform)s cal., %(polarity)s polarity, amp. %(amplitude)s dB, duration=%(duration)s, settle=%(settle)s, sleep=%(sleep)s, trailer=%(trailer)s" % cal)
        elif cal['waveform'] == 'random':
            print("  %(waveform)s cal., noise=%(noise)s, amp. %(amplitude)s dB, duration=%(duration)s, settle=%(settle)s, sleep=%(sleep)s, trailer=%(trailer)s" % cal)

        

