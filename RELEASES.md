# Release Notes

#### Version 2.1.2
* Add virtualenv install as a local wheel
* isAlive is deprecated, use is_alive in python threads module

#### Version 2.1.1
* Add pyserial install as a local wheel

#### Version 2.1.0
* Update repo to build release package
* Set character encoding to ascii for serial port communication. 
* Use updated pyserial for serial port driver
* Fix e330 routines

#### Version 1.0.0
* Convert from Python 2 to Python 3 using virtual environment to run code.  
