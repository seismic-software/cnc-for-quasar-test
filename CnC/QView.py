import socket
import struct
import time

from QAction import QAction
from QTools import q330_to_unix_time, QExMessage
from Quasar import Cmds
from Quasar.CmdID import CmdID


def T(expr, if_true, if_false):
    if expr:
        return if_true
    else:
        return if_false


def decode_frequency(code):
    if code & 0x80:
        if (code ^ 0xff) == 0:
            frequency = 0.1
        elif (code & 0x0f) == 4:
            frequency = 200.0
        elif (code & 0x0f) == 5:
            frequency = 250.0
        elif (code & 0x0f) == 6:
            frequency = 300.0
        elif (code & 0x0f) == 7:
            frequency = 400.0
        elif (code & 0x0f) == 8:
            frequency = 500.0
        elif (code & 0x0f) == 9:
            frequency = 800.0
        elif (code & 0x0f) == 10:
            frequency = 1000.0
        else:
            frequency = 0.0
    else:
        frequency = float(code)
    return frequency


def format_AMR(results):
    section = results[0]
    amr = results[1]
    print(section_map[section][0])
    print("  Sensor A:")
    print("    Maximum Tries --------- %d" % amr.getMaximumTries1())
    print("    Normal Interval ------- %d" % amr.getNormalInterval1())
    print("    Squelch Interval ------ %d" % amr.getSquelchInterval1())
    print("    Sensor Control Bitmap - %08x" % amr.getSensorControlBitmap1())
    print("    Duration -------------- %d ms" % (amr.getDuration1() * 10,))
    print("    Tolerances")
    print("      Mass 1 - %d" % amr.getTolerance1A())
    print("      Mass 2 - %d" % amr.getTolerance1B())
    print("      Mass 3 - %d" % amr.getTolerance1C())
    print("  Sensor B:")
    print("    Maximum Tries --------- %d" % amr.getMaximumTries2())
    print("    Normal Interval ------- %d" % amr.getNormalInterval2())
    print("    Squelch Interval ------ %d" % amr.getSquelchInterval2())
    print("    Sensor Control Bitmap - %08x" % amr.getSensorControlBitmap2())
    print("    Duration -------------- %d ms" % (amr.getDuration2() * 10,))
    print("    Tolerances")
    print("      Mass 1 - %d" % amr.getTolerance2A())
    print("      Mass 2 - %d" % amr.getTolerance2B())
    print("      Mass 3 - %d" % amr.getTolerance2C())


def format_BCF(results):
    section = results[0]
    bcf = results[1]
    print(section_map[section][0])
    print("  Block Size ---------------- %d bytes" % bcf.getBlockSize())
    interface_num = bcf.getPhysicalInterfaceNumber()
    interface_str = "Unrecognized"
    if type(interface_num) == int:
        if interface_num == 0:
            interface_str = "Serial 1"
        elif interface_num == 1:
            interface_str = "Serial 2"
        elif interface_num == 3:
            interface_str = "Ethernet"
    print("  Physical Interface Number - %d (%s)" % (interface_num, interface_str))
    print("  Baler Type ---------------- %d" % bcf.getBalerType())
    print("  Version ------------------- %d" % bcf.getVersion())
    print("  Configuration String ------ %s" % bcf.getBalerConfigurationString())


def format_DCP(results):
    section = results[0]
    dcp = results[1]
    print(section_map[section][0])
    print("  Counts with Grounded Input:")
    for i in range(1, 7): print("    Channel %d - %d" % (i, dcp.getGetFunction("Channel%dCountsWithGroundedInput" % i)()))
    print("  Counts with Reference Input:")
    for i in range(1, 7): print("    Channel %d - %d" % (i, dcp.getGetFunction("Channel%dCountsWithReferenceInput" % i)()))


def format_DEV(results):
    section = results[0]
    dev = results[1]
    print(section_map[section][0])

    for index in range(1, dev.getNumDevs() + 1):
        print("  Device %d:" % index)
        print("    Port Number ----------- %s" % str(dev.getGetFunction("PortNumber_%d" % index)()))
        print("    Unit ID --------------- %s" % str(dev.strUnitID(index)))
        print("    Version --------------- %s" % str(dev.getGetFunction("Version_%d" % index)()))
        print("    Options --------------- %s" % str(dev.getGetFunction("Options_%d" % index)()))
        sn = dev.getGetFunction("SerialNumber_%d" % index)()
        if sn is not None:
            sn = "%016x" % sn
        print("    Serial Number --------- %s" % str(sn))
        print("    Device Static Storage - %s" % str(dev.getGetFunction("DeviceStaticStorage_%d" % index)()))
        print("    Seconds Since Heard --- %s" % str(dev.getGetFunction("SecondsSinceHeard_%d" % index)()))


def format_DP(results):
    section = results[0]
    dp = results[1]
    fix = results[2]
    print(section_map[section][0])

    print("  Flags:")
    print("    Fill Mode ------------------------------- %s" % T(dp.getFlags() & 0x0001, "Enabled", "Disabled"))
    print("    Time Based Packet Buffer Flush ---------- %s" % T(dp.getFlags() & 0x0002, "Enabled", "Disabled"))
    print("    Data Port Output Frozen ----------------- %s" % T(dp.getFlags() & 0x0004, "Yes", "No"))
    print("    Packet Buffer Input Frozen -------------- %s" % T(dp.getFlags() & 0x0008, "Yes", "No"))
    print("    Packet Buffer Keeps Oldest Data --------- %s" % T(dp.getFlags() & 0x0010, "Yes", "No"))
    print("    DP Piggyback Status Requests ------------ %s" % T(dp.getFlags() & 0x0100, "Enabled", "Disabled"))
    print("    Fault LED if Buffer Above 5%% Post Flush - %s" % T(dp.getFlags() & 0x0200, "Enabled", "Disabled"))
    print("    Hot Swap Allowed ------------------------ %s" % T(dp.getFlags() & 0x0400, "Yes", "No"))
    print("    Time Based Sliding Window Buffer Flush -- %s" % T(dp.getFlags() & 0x0800, "Enabled", "Disabled"))
    print("    Base-96 Encode Data Packets ------------- %s" % T(dp.getFlags() & 0x4000, "Yes", "No"))
    print("  Packet Buffer Usage ----- %0.2f %%" % (dp.getPercentOfPacketBuffer() / 2.56,))
    print("  MTU --------------------- %d" % dp.getMTU())
    print("  Group Count ------------- %d" % dp.getGroupCount())
    print("  Group Timeout------------ %d" % dp.getGroupTimeout())
    print("  Minimum Re-Send Timeout - %d" % dp.getMinimumResendTimeout())
    print("  Maximum Re-Send Timeout - %d" % dp.getMaximumResendTimeout())
    print("  Window Size ------------- %d" % dp.getWindowSize())
    print("  Data Sequence Number ---- %d" % dp.getDataSequenceNumber())
    print("  Frequencies:")
    for i in range(1, 7):
        map = dp.getGetFunction("Channel%dFreqs" % i)()
        print("    Channel %d: " % i, end=' ')
        for j in range(0, 8):
            if not map & (1 << j):
                continue
            frequency = decode_frequency(fix.getGetFunction("Bit%dFrequency" % j)())
            print("%0.1f Hz " % frequency, end=' ')
        print()
    print("  Acknowledge Count ------- %d" % dp.getAcknowledgeCount())
    print("  Acknowledge Timeout ----- %d" % dp.getAcknowledgeTimeout())
    print("  Old Data Threshold Time - %d" % dp.getOldDataThresholdTime())
    print("  Ethernet Throttle ------- %d" % dp.getEthernetThrottle())
    print("  Alarm Percent ----------- %0.2f" % (dp.getAlarmPercent() / 2.56,))
    print("  Automatic Filters ------- %04x" % dp.getAutomaticFilters())
    print("  Manual Filters ---------- %04x" % dp.getManualFilters())


def format_EPC(results):
    section = results[0]
    epc = results[1]
    ep1c = []
    ep2c = []
    for i in range(1, epc.getChannelCount() + 1):
        channel = epc.getGetFunction("Channel_%d" % i)()
        mask = epc.getGetFunction("OutputMask_%d" % i)()
        ports = []
        for j in range(0, 4):
            if mask & (1 << j):
                ports.append(j + 1)
        channel_str = "      Channel %d\n      Data Ports: %s" % (channel & 0x7f, ', '.join(map(str, ports)))
        if channel & 0x80:
            ep2c.append(channel_str)
        else:
            ep1c.append(channel_str)

    print(section_map[section][0])
    print("  Serial 1")
    print("    WXT520 Heater - %s" % T(epc.getFlagsSerial1EP() & 0x00000001, "Continuous", "Command Only"))
    print("    Channels [%d]" % len(ep1c))
    print("\n".join(ep1c))
    print("  Serial 2")
    print("    WXT520 Heater - %s" % T(epc.getFlagsSerial2EP() & 0x00000001, "Continuous", "Command Only"))
    print("    Channels [%d]" % len(ep2c))
    print("\n".join(ep2c))


def format_ETH(results):
    section = results[0]
    phy1 = results[1]
    phy2 = results[2]
    flags = phy1.getEthernetFlags()
    print(section_map[section][0],
          "[%s] -> Data Port %d" % (T(flags & 0x0001, "Enabled", "Disabled"), phy2.getDataPortNumber() + 1))
    print("  IP Address -- %s" % phy1.strEthernetIPAddress())
    print("  MAC Address - %s:%s" % (phy1.strEthernetMACAddressHigh32(), phy1.strEthernetMACAddressLow16()))
    print("  Base Port --- %d" % phy1.getBasePort())
    print("  Mode -------- %s" % T(flags & 0x0002, "Always On", "Link Detect"))
    print("  Access Restrictions:")
    print("    ICMP Pings -- %s" % T(flags & 0x0010, "Registered and POC", "Un-restricted"))
    print("    QDP Pings --- %s" % T(flags & 0x0080, "Registered and POC", "Un-restricted"))
    print("    TCP Packets - %s" % T(flags & 0x0020, "Registered and POC", "Un-restricted"))
    print("  Routing Packets ------- %s" % T(flags & 0x0040, "Enabled", "Disabled"))
    ip_method = flags & 0x0300
    print("  IP Address Method ----- %s" % T(ip_method == 0, "Programmed",
                                             T(ip_method == 1, "BOOTP -> Programmed",
                                               T(ip_method == 2, "Programmed -> BOOTP", "Unrecognized"))))
    print("  Non-Broadcast Polling - %s" % T(flags & 0x0400, "Allowed", "Disallowed"))
    print("  Memory Trigger -------- %s" % T(phy2.getMemoryTriggerLevel() > 255,
                                             "%d bytes" % phy2.getMemoryTriggerLevel(),
                                             "%d" % int(phy2.getMemoryTriggerLevel() / 2.56)))
    print("  Web Server Rate Limit - %s" % T(phy2.getWebServerBPSLimit() == 0, "Unlimited",
                                             "%d bps" % phy2.getWebServerBPSLimit() * 10))
    print("  Flags")
    flags = phy2.getFlags()
    print("    Serial Protocol ----- %s" % T(flags & 0x0001, "PPP", "SLIP"))
    print("    Serial Setup -------- %s" % T(flags & 0x0002, "Dial-In/Out", "Baler/Telemetry Only"))
    print("    Dial-In ------------- %s" % T((flags & 0x000c) == 0, "Disabled",
                                             T((flags & 0x000c) == 1, "CHAP Authentication",
                                               T((flags & 0x000c) == 2, "PAP Authentication",
                                                 "No Authentication Required"))))
    print("    Baler Power Control - %s" % T((flags & 0x0030) == 0, "No Baler",
                                             T((flags & 0x0030) == 1, "DTR",
                                               T((flags & 0x0030) == 2, "Break", "Continuous"))))
    print("    Enable Baler/Dial-Out at Completion of Recording Window ---- %s" % T(flags & 0x0040, "Yes", "No"))
    print("    Enable Baler/Dial-Out When Data Port Trigger Level Reached - %s" % T(flags & 0x0080, "Yes", "No"))
    print("    Enable Baler/Dial-Out at Interval -------------------------- %s" % T(flags & 0x0100, "Yes", "No"))
    print("    Allow Baler to Issue Calibration Commands ------------------ %s" % T(flags & 0x0200, "Yes", "No"))
    print("    Use DTR to Control Modem for Dial-In/Out ------------------- %s" % T(flags & 0x0400, "Yes", "No"))
    print("    Allow NAPT to Baler ------ %s" % T(flags & 0x0800, "Yes", "No"))
    print("    Transparent Routing Mode - %s" % T(flags & 0x1000, "Enabled", "Disabled"))
    print("    Baler Type --------------- %s" % T(flags & 0x2000, "Baler44", "Baler14"))
    print("    Simplex Operation -------- %s" % T(flags & 0x8000, "Enabled", "Disabled"))
    print("  Baler or POC Address ------------- %s" % socket.inet_ntoa(
        struct.pack('>L', phy2.getPointOfContactOrBalerIPAddress())))
    if phy2.getDialInIPSuggestionOrBalerSecondaryIPAddress() is None:
        sip = "Disabled"
    else:
        sip = socket.inet_ntoa(struct.pack('>L', phy2.getDialInIPSuggestionOrBalerSecondaryIPAddress()))
    print("  Dial-In IP or Baler Secondary IP - %s" % sip)
    print("  POC Port Number ------------------ %s" % phy2.getPointOfContactPortNumber())
    print("  Baler Retries -------------------- %d" % phy2.getBalerRetries())
    if phy2.getBalerRegistrationTimeout() is None:
        timeout = "Not Set"
    else:
        timeout = "%d minutes" % phy2.getBalerRegistrationTimeout()
    print("  Baler Registration Timeout ------- %s" % timeout)
    if phy2.getRoutedPacketTimeout() is None:
        timeout = "Not Set"
    else:
        timeout = "%d minutes" % phy2.getRoutedPacketTimeout()
    print("  Routed Packets Timeout ----------- %s" % timeout)


def format_FC(results):
    section = results[0]
    fix = results[1]
    print(section_map[section][0])
    print("  Last Reboot ----------------------- %s" % time.strftime('%Y/%m/%d %H:%M:%S', time.gmtime(q330_to_unix_time(fix.getTimeOfLastReboot()))))
    print("  Total Number of Reboots ----------- %d" % fix.getTotalNumberOfReboots())
    print("  KMI Property Tag ------------------ %d" % fix.getKMIPropertyTag())
    print("  System Serial Number -------------- %016x" % fix.getSystemSerialNumber())
    print("  Analog Mother Board Serial Number - %016x" % fix.getAnalogMotherBoardSerialNumber())
    print("  Seismometer 1 Serial Number ------- %016x" % fix.getSeismometer1SerialNumber())
    print("  Seismometer 2 Serial Number ------- %016x" % fix.getSeismometer2SerialNumber())
    print("  QAPCHP 1 Serial Number ------------ %08x" % fix.getQAPCHP1SerialNumber())
    print("  QAPCHP 2 Serial Number ------------ %08x" % fix.getQAPCHP2SerialNumber())
    backup = fix.getBackupDataStructureBitmap()
    default = fix.getDefaultDataStructureBitmap()
    print("  Data Structures Used")
    print("    Slave Processor Parameters ---- %s" % T(default & 0x00000001, "Default",
                                                       T(backup & 0x00000001, "Backup", "Standard")))
    print("    Manufacturer's Area ----------- %s" % T(default & 0x00000002, "Default",
                                                       T(backup & 0x00000002, "Backup", "Standard")))
    print("    Authentication Code ----------- %s" % T(default & 0x00000004, "Default",
                                                       T(backup & 0x00000004, "Backup", "Standard")))
    print("    Physical Interfaces ----------- %s" % T(default & 0x00000008, "Default",
                                                       T(backup & 0x00000008, "Backup", "Standard")))
    print("    Data Port 1 ------------------- %s" % T(default & 0x00000010, "Default",
                                                       T(backup & 0x00000010, "Backup", "Standard")))
    print("    Data Port 2 ------------------- %s" % T(default & 0x00000020, "Default",
                                                       T(backup & 0x00000020, "Backup", "Standard")))
    print("    Data Port 3 ------------------- %s" % T(default & 0x00000040, "Default",
                                                       T(backup & 0x00000040, "Backup", "Standard")))
    print("    Data Port 4 ------------------- %s" % T(default & 0x00000080, "Default",
                                                       T(backup & 0x00000080, "Backup", "Standard")))
    print("    Global Programming ------------ %s" % T(default & 0x00000100, "Default",
                                                       T(backup & 0x00000100, "Backup", "Standard")))
    print("    Record Keeping ---------------- %s" % T(default & 0x00000200, "Default",
                                                       T(backup & 0x00000200, "Backup", "Standard")))
    print("    Sensor Control Mapping -------- %s" % T(default & 0x00000400, "Default",
                                                       T(backup & 0x00000400, "Backup", "Standard")))
    print("    Power Supply Parameters ------- %s" % T(default & 0x00000800, "Default",
                                                       T(backup & 0x00000800, "Backup", "Standard")))
    print("    Advanced Physical on Serial 1 - %s" % T(default & 0x00001000, "Default",
                                                       T(backup & 0x00001000, "Backup", "Standard")))
    print("    Advanced Physical on Serial 2 - %s" % T(default & 0x00002000, "Default",
                                                       T(backup & 0x00002000, "Backup", "Standard")))
    print("    Advanced Physical on Ethernet - %s" % T(default & 0x00008000, "Default",
                                                       T(backup & 0x00008000, "Backup", "Standard")))
    print("    GPS and PLL Parameters -------- %s" % T(default & 0x00010000, "Default",
                                                       T(backup & 0x00010000, "Backup", "Standard")))
    print("    Automatic Recenter ------------ %s" % T(default & 0x00020000, "Default",
                                                       T(backup & 0x00020000, "Backup", "Standard")))
    print("    Recording Windows ------------- %s" % T(default & 0x00040000, "Default",
                                                       T(backup & 0x00040000, "Backup", "Standard")))
    print("    Announcements ----------------- %s" % T(default & 0x00080000, "Default",
                                                       T(backup & 0x00080000, "Backup", "Standard")))
    print("    Baler Link -------------------- %s" % T(default & 0x00100000, "Default",
                                                       T(backup & 0x00100000, "Backup", "Standard")))
    print("    Baler Configuration ----------- %s" % T(default & 0x00200000, "Default",
                                                       T(backup & 0x00200000, "Backup", "Standard")))
    print("    Environmental Processor Conf. - %s" % T(default & 0x00400000, "Default",
                                                       T(backup & 0x00400000, "Backup", "Standard")))
    print("  Internal Data Memory Size --------- %d" % fix.getInternalDataMemorySize())
    print("  Internal Data Memory Used --------- %d" % fix.getInternalDataMemoryUsed())
    print("  External Data Memory Size --------- %d" % fix.getExternalDataMemorySize())
    print("  External Data Memory Used --------- %d" % fix.getExternalDataMemoryUsed())
    print("  Flash Memory Size ----------------- %d" % fix.getFlashMemorySize())
    print("  Data Port 1 Packet Memory Size ---- %d" % fix.getDataPort1PacketMemorySize())
    print("  Data Port 2 Packet Memory Size ---- %d" % fix.getDataPort2PacketMemorySize())
    print("  Data Port 3 Packet Memory Size ---- %d" % fix.getDataPort3PacketMemorySize())
    print("  Data Port 4 Packet Memory Size ---- %d" % fix.getDataPort4PacketMemorySize())
    flags = fix.getFlags()
    print("  Q330 Flags")
    print("    Software Can Process")
    print("      Dynamic IP Address Status Request - %s" % T(flags & 0x0001, "Yes", "No"))
    print("      Auxiliary Board Status Request ---- %s" % T(flags & 0x0002, "Yes", "No"))
    print("      Expanded C1_WEB Commands ---------- %s" % T(flags & 0x0004, "Yes", "No"))
    print("      Serial Sensor Status Request ------ %s" % T(flags & 0x0008, "Yes", "No"))
    print("    Ethernet Installed ----------------- %s" % T(flags & 0x0010, "Yes", "No"))
    print("    Can Report > 255ma Supply Current -- %s" % T(flags & 0x0020, "Yes", "No"))
    print("    Environmental Processor(s) Present - %s" % T(flags & 0x0040, "Yes", "No"))
    print("    Integrated Baler44 on Serial 2 ----- %s" % T(flags & 0x0080, "Yes", "No"))
    print("  Calibrator Type ------------ %s" % T(fix.getCalibratorType() == 33, "QCAL330", "Unknown"))
    print("  Calibrator Version --------- %d.%d" % (fix.getCalibratorVersion() / 256, fix.getCalibratorVersion() % 256))
    print("  Aux. Type ------------------ %s" % T(fix.getAuxType() == 32, "AuxAD", "Unknown"))
    print("  Aux. Version --------------- %d.%d" % (fix.getAuxVersion() / 256, fix.getAuxVersion() % 256))
    print("  Clock Type ----------------- %s" % T(fix.getClockType() == 0, "No Clock",
                                                  T(fix.getClockType() == 1, "Motorola M12", "Unknown")))
    print("  System Version ------------- %d.%d" % (fix.getSystemVersion() / 256, fix.getSystemVersion() % 256))
    print("  Slave Processor Version ---- %d.%d" % (fix.getSlaveProcessorVersion() / 256, fix.getSlaveProcessorVersion() % 256))
    print("  PLD Version ---------------- %d.%d" % (fix.getPLDVersion() / 256, fix.getPLDVersion() % 256))
    print("  Memory Block Size ---------- %d" % fix.getMemoryBlockSize())
    print("  Frequencies: ")
    for i in range(0, 8):
        frequency = decode_frequency(fix.getGetFunction("Bit%dFrequency" % i)())
        if frequency != 0.0:
            print("    %0.1f Hz " % frequency)
            print(
                "      Channel 1-3 Delay - %d Usec" % fix.getGetFunction("Channels1To3Bit%dFrequencyDelayInUSec" % i)())
            print(
                "      Channel 4-6 Delay - %d Usec" % fix.getGetFunction("Channels4To6Bit%dFrequencyDelayInUSec" % i)())


def format_GP(results):
    section = results[0]
    glob = results[1]
    fix = results[2]
    print(section_map[section][0])
    print("  Clock Timeout ------------ %d seconds" % glob.getClockTimeout())
    print("  Server Timeout ----------- %d seconds" % glob.getServerTimeout())
    print("  Initial VCO -------------- %d" % (glob.getInitialVCO() & 0x0fff,))
    print("  Automatic Setting by PLL - %s" % T(glob.getInitialVCO() & 0x1000, "Enabled", "Disabled"))
    print("  GPS Backup Power --------- %s" % T(glob.getGPSBackupPower() == 1, "Enabled", "Disabled"))
    print("  Web Port ----------------- %d" % glob.getWebPort())
    print(
        "  Drift Tolerance ---------- %s" % T(glob.getDriftTolerance() == 0, "Infinite", str(glob.getDriftTolerance())))
    print("  Jump Filter -------------- %d" % glob.getJumpFilter())
    print("  Jump Threshold ----------- %d usec" % glob.getJumpThresholdInUSec())
    print("  Calibrator Offset -------- %d" % glob.getCalibratorOffset())
    print("  Sensor Control Bitmap ---- 0x%08x" % glob.getSensorControlBitmap())
    print("  Sampling Phase ----------- %d" % glob.getSamplingPhase())
    print("  GPS Cold-Start After ----- %d seconds" % glob.getGPSColdStartSeconds())
    print("  User Tag ----------------- %d" % glob.getUserTag())
    asw_flags = glob.getAuxAndStatusSamplingRates()
    print("  Aux. Reporting Interval -- %s" % T((asw_flags & 0x0003) == 0, "Not Reported",
                                                T((asw_flags & 0x0003) == 1, "1Hz", "Unrecognized")))
    print("  Status Report Interval --- %s" % T(((asw_flags & 0x0300) >> 8) == 0, "Not Reported",
                                                T(((asw_flags & 0x0300) >> 8) == 1, "1Hz", "Unrecognized")))
    print("  Web Page")
    print("    Require Authentication for Baler Power Control - %s" % T(asw_flags & 0x04, "No", "Yes"))
    print("    Require Authentication for Baler Link Control -- %s" % T(asw_flags & 0x08, "No", "Yes"))
    print("    Display Q330 Serial Number --------------------- %s" % T(asw_flags & 0x08, "Yes", "No"))
    gain_bitmap = glob.getGainBitmap()
    filter_bitmap = glob.getFilterBitmap()
    input_bitmap = glob.getInputBitmap()
    for r_id in range(1, 3):
        s_channel = r_id + ((r_id - 1) * 2)
        filter_shift = (r_id - 1) * 2
        input_shift = (r_id - 1) * 2 + 8
        filter = (filter_bitmap >> filter_shift) & 0x0003
        input = (input_bitmap >> input_shift) & 0x0003
        print("  Channels %d-%d" % (s_channel, s_channel + 2))
        print("    Linear Phase Filters - %s" % T(filter == 0, "All Frequencies", T(filter == 1, "Below 100Hz",
                                                                                    T(filter == 2, "Below 40Hz",
                                                                                      "Below 20Hz"))))
        print("    Input ---------------- %s" % T(input == 0, "1PPS Input", T(input == 1, "Reference Input",
                                                                              T(input == 2, "Calibrator Output",
                                                                                "Grounded Input"))))
        for channel in range(s_channel, s_channel + 3):
            gain_shift = (channel - 1) * 2
            input_shift = channel - 1
            gain = (gain_bitmap >> gain_shift) & 0x0003
            input = (input_bitmap >> input_shift) & 0x0001
            print("    Channel %d" % channel)
            print("      State -- %s" % T(gain == 0, "Disabled",
                                          T(gain == 1, "Pre-amp Off", T(gain == 2, "Pre-amp On", "Unknown"))))
            print("      Input -- %s" % T(input == 1, "Special Input MUX", "Normal Input Signal"))
            print("      Offset - %d" % (glob.getGetFunction("Channel%dOffset" % channel)() * 100))
            print("      Gain --- %0.4f" % (glob.getGetFunction("Channel%dGain" % channel)() / 1024.0))
            print("      Frequency Multipliers")
            for i in range(0, 8):
                frequency = decode_frequency(fix.getGetFunction("Bit%dFrequency" % i)())
                multiplier = glob.getGetFunction("Channel%dFreqBit%dScale" % (channel, i))()
                if frequency != 0.0:
                    print(
                        f"        {f'{frequency:0.1f}'.rjust(6)} Hz - {multiplier:d}")
    print("  Message Bitmap - 0x%08x" % glob.getMessageBitmap())


def format_GPS(results):
    section = results[0]
    gid = results[1]
    gps = results[2]
    print(section_map[section][0])
    mode = gps.getTimingMode()
    main = mode & 0x0007
    print("  Timing Mode --------- %s" % T(main == 0, "Internal GPS",
                                           T(main == 1, "External GPS",
                                             T(main == 2, "External SeaScan",
                                               T(main == 3, "Network Timing",
                                                 T(main == 4, "External TX/RX Access to Internal GPS", "Unknown"))))))
    print("    Export NMEA and 1PPS Output - %s" % T(mode & 0x0004, "Yes", "No"))
    print("    Import/Export --------------- %s" % T(mode & 0x0010, "RS-422 1PPS", "RS-232"))
    print("    Serial DGPS Input ----------- %s" % T(mode & 0x0020, "Yes", "No"))
    print("    QDP Network DGPS Input ------ %s" % T(mode & 0x0040, "Yes", "No"))
    mode = gps.getPowerCyclingMode()
    print("  Power Cycling Mode -- %s" % T(mode == 0, "Continuous Operation",
                                           T(4 > mode > 0,
                                             "Turn Off After Maximum On Time%s" % T(mode == 2, " or PLL Locks",
                                                                                    T(mode == 3, " or GPS Locks", "")),
                                             "")))

    print("  GPS Re-Sync Hour ---------- %d" % gps.getGPSResyncHour())
    print("  GPS Off Time -------------- %d minutes" % gps.getGPSOffTime())
    print("  GPS Maximum On Time ------- %d minutes" % gps.getGPSMaximumOnTime())
    print("  PLL Update Interval ------- %d seconds" % gps.getPLLUpdateIntervalInSeconds())
    print("  PLL Lock ------------------ %d usec" % gps.getPLLLockUSecs())
    pll_flags = gps.getPLLFlags()
    print("  Enable PLL By Default ----- %s" % T(pll_flags & 0x0001, "Yes", "No"))
    print("  Lock Required ------------- %s" % T(pll_flags & 0x0002, "2-D", "3-D"))
    print("  Temperature Corrected VCO - %s" % T(pll_flags & 0x0004, "Yes", "No"))
    print("  Pfrac --------------------- %0.4f" % gps.getPfrac())
    print("  VCO Slope ----------------- %0.4f" % gps.getVCOSlope())
    print("  VCO Intercept ------------- %0.4f" % gps.getVCOIntercept())
    print("  Max. Initial KM RMS ------- %0.4f" % gps.getMaxInitialKMRMS())
    print("  Initial KM Weight --------- %0.4f" % gps.getInitialKMWeight())
    print("  KM Weight ----------------- %0.4f" % gps.getKMWeight())
    print("  Best VCO Weight ----------- %0.4f" % gps.getBestVCOWeight())
    print("  KM Delta ------------------ %0.4f" % gps.getKMDelta())
    print("  GSP ID Strings")
    for i in range(1, 10):
        print("   ID #%d - %s" % (i, gid.getGetFunction("ID%d" % i)()))


def format_MA(results):
    section = results[0]
    man = results[1]
    print(section_map[section][0])
    print("  Password: ****")
    print("  QAPCHP Channels 1-3:")
    print("    Type ---- %s" % T(man.getQAPCHP1To3Type() != 0, "Installed", "Not Installed"))
    version = man.getQAPCHP1To3Version()
    if type(version) == int:
        version_string = "%d.%d" % (version / 256, version % 256)
    else:
        version_string = "N/A"
    print("    Version - %s" % version_string)
    print("    S/N ----- %016x" % man.getQAPCHP1To3SerialNumber())
    print("    26-bit -- %s" % T(man.getFlags() & 0x0008, "Yes", "No"))
    print("    Channel 1 Expected Counts w/ Reference - %d" % man.getQAPCHPChannel1ExpectedCountsWithReferenceApplied())
    print("    Channel 2 Expected Counts w/ Reference - %d" % man.getQAPCHPChannel2ExpectedCountsWithReferenceApplied())
    print("    Channel 3 Expected Counts w/ Reference - %d" % man.getQAPCHPChannel3ExpectedCountsWithReferenceApplied())
    print("  QAPCHP Channels 4-6:")
    print("    Type ---- %s" % T(man.getQAPCHP4To6Type() != 0, "Installed", "Not Installed"))
    version = man.getQAPCHP4To6Version()
    if type(version) == int:
        version_string = "%d.%d" % (version / 256, version % 256)
    else:
        version_string = "N/A"
    print("    Version - %s" % version_string)
    print("    S/N ----- %016x" % man.getQAPCHP4To6SerialNumber())
    print("    26-bit -- %s" % T(man.getFlags() & 0x0010, "Yes", "No"))
    print("    Channel 4 Expected Counts w/ Reference - %d" % man.getQAPCHPChannel4ExpectedCountsWithReferenceApplied())
    print("    Channel 5 Expected Counts w/ Reference - %d" % man.getQAPCHPChannel5ExpectedCountsWithReferenceApplied())
    print("    Channel 6 Expected Counts w/ Reference - %d" % man.getQAPCHPChannel6ExpectedCountsWithReferenceApplied())
    print("  Born ---------------------- %s (%0.2f hours ago)" % (time.strftime('%Y/%d/%m %H:%M:%S', time.gmtime(q330_to_unix_time(man.getBornOnTime()))),q330_to_unix_time(man.getBornOnTime()) / 3600.0))
    print("  Packet Memory ------------- %d bytes" % man.getPacketMemoryInstalledInBytes())
    print("  Clock Type ---------------- %s" % T(man.getClockType() == 1, "Motorola M12", "No Clock"))
    print("  Model Number -------------- %d" % man.getModelNumber())
    print("  Default Calibrator Offset - %d" % man.getDefaultCalibratorOffset())
    print("  NTP Operation ------------- %s" % T(man.getFlags() & 0x0020, "Enabled", "Disabled"))
    print("  Integrated Baler44 -------- %s" % T(man.getFlags() & 0x0040, "Yes", "No"))
    print("  Analog P/S Voltage -------- %0.2f" % (5.45 + (((man.getFlags() & 0xff00) >> 8) * 0.05),))
    print("  KMI Property Tag ---------- %d" % man.getKMIPropertyTag())
    print("  Maximum Power On Seconds -- %d" % man.getMaximumPowerOnSeconds())


def format_RT(results):
    section = results[0]
    rt = results[1]
    print(section_map[section][0])
    for i in range(1, rt.getNumEntries() + 1):
        ip = socket.inet_ntoa(struct.pack('>L', rt.getGetFunction("IPAddress_%d" % i)()))
        pi = rt.getGetFunction("PhysicalInterface_%d" % i)()
        dp = rt.getGetFunction("DataPort_%d" % i)()
        print("  %s --%s--> %s (aged %d seconds)" % (ip.rjust(15),
                                                     # Physical Interface
                                                     T(pi == 0, "[Serial 1]",
                                                       T(pi == 1, "[Serial 2]",
                                                         T(pi == 3, "[Ethernet]",
                                                           "[Unknown ]"))),
                                                     # Data Port
                                                     T(dp < 4, "Data Port %d" % (dp + 1,),
                                                       T(dp == 4, "Configuration",
                                                         T(dp == 5, "Special Functions", "Unknown"))),
                                                     # Last Heard
                                                     rt.getGetFunction("SecondsSinceHeard_%d" % i)()))


def format_RW(results):
    section = results[0]
    win = results[1]
    print(section_map[section][0])
    flags = win.getRecordingFlags()
    print("  Main Power Controlled by --- %s" % T(flags & 0x0001, "Recording Windows", "Main Power Input"))
    print("  Analog Power Controlled by - %s" % T(flags & 0x0002, "Recording Windows", "Analog Power Input"))
    print("  Recording Controlled by ---- %s" % T(flags & 0x0004, "Recording Windows", "Trigger Input"))
    for window in range(1, win.getNumberOfWindows() + 1):
        print("  Window %d" % window)
        print("    Start Recording Time - %s" % time.strftime('%Y/%m/%d %H:%M:%S', time.gmtime(
            q330_to_unix_time(win.getGetFunction("StartRecordingTime_%d" % window)()))))
        print("    Stop Recording Time -- %s" % time.strftime('%Y/%m/%d %H:%M:%S', time.gmtime(
            q330_to_unix_time(win.getGetFunction("StopRecordingTime_%d" % window)()))))


def format_SCM(results):
    section = results[0]
    win = results[1]
    print(section_map[section][0])
    for i in range(1, 9):
        print("  Sensor Output %d - %s" % (i, win.getStrFunction("SensorOutput%dDefinition" % i)()))


def format_COM(results):
    section = results[0]
    com = results[1]
    print(section_map[section][0])


def format_SI(results):
    section = results[0]
    phy1 = results[1]
    phy2 = results[2]
    port = phy2.getPhysicalInterfaceNumber() + 1
    flags = phy1.getGetFunction("SerialInterface%dFlags" % port)()
    print(section_map[section][0],
          "[%s] -> Data Port %d" % (T(flags & 0x0001, "Enabled", "Disabled"), phy2.getDataPortNumber() + 1))
    print("  IP Address ---------- %s" % phy1.getStrFunction("SerialInterface%dIPAddress" % port)())
    print("  Base Port ----------- %d" % phy1.getBasePort())
    print("  Baud Rate ----------- %s" % phy1.getBaudString(phy1.getGetFunction("SerialInterface%dBaud" % port)()))
    throttle = phy1.getGetFunction("SerialInterface%dThrottle" % port)()
    if throttle <= 0:
        throttle_string = "Unlimited"
    else:
        throttle_string = "%d Bps" % (1024000 / throttle,)
    print("  Throttle ------------ %s" % throttle_string)
    print("  RTS/CTS Handshaking - %s" % T(flags & 0x0004, "Enabled", "Disabled"))
    print("  Break Detect -------- %s" % T(flags & 0x0008, "Enabled", "Disabled"))
    print("  Access Restrictions:")
    print("    ICMP Pings -- %s" % T(flags & 0x0010, "Registered and POC", "Un-restricted"))
    print("    QDP Pings --- %s" % T(flags & 0x0080, "Registered and POC", "Un-restricted"))
    print("    TCP Packets - %s" % T(flags & 0x0020, "Registered and POC", "Un-restricted"))
    print("  Routing Packets ------- %s" % T(flags & 0x0040, "Enabled", "Disabled"))
    ip_method = flags & 0x0300
    print("  IP Address Method ----- %s" % T(ip_method == 0, "Programmed",
                                             T(ip_method == 1, "BOOTP -> Programmed",
                                               T(ip_method == 2, "Programmed -> BOOTP", "Unrecognized"))))
    print("  Non-Broadcast Polling - %s" % T(flags & 0x0400, "Allowed", "Disallowed"))

    # C2_PHY2
    print("  Modem Initialization -- %s" % phy2.getModemInititialization())
    print("  Phone Number ---------- %s" % T(phy2.getPhoneNumber(), phy2.getPhoneNumber(), "None"))
    print("  Dial-Out User Name ---- %s" % T(phy2.getDialOutUserName(), phy2.getDialOutUserName(), "None"))
    print("  Dial-Out Password ----- %s" % T(phy2.getDialOutPassword(), phy2.getDialOutPassword(), "None"))
    print("  Dial-In User Name ----- %s" % T(phy2.getDialInUserName(), phy2.getDialInUserName(), "None"))
    print("  Dial-In Password ------ %s" % T(phy2.getDialInPassword(), phy2.getDialInPassword(), "None"))
    print("  Dial-Out/Log Interval - %d" % phy2.getInterval())
    print("  Memory Trigger -------- %s" % T(phy2.getMemoryTriggerLevel() > 255,
                                             "%d bytes" % phy2.getMemoryTriggerLevel(),
                                             "%d" % int(phy2.getMemoryTriggerLevel() / 2.56)))
    print("  Web Server Rate Limit - %s" % T(phy2.getWebServerBPSLimit() == 0, "Unlimited",
                                             "%d bps" % phy2.getWebServerBPSLimit() * 10))
    print("  Flags")
    flags = phy2.getFlags()
    print("    Serial Protocol ----- %s" % T(flags & 0x0001, "PPP", "SLIP"))
    print("    Serial Setup -------- %s" % T(flags & 0x0002, "Dial-In/Out", "Baler/Telemetry Only"))
    print("    Dial-In ------------- %s" % T((flags & 0x000c) == 0, "Disabled",
                                             T((flags & 0x000c) == 1, "CHAP Authentication",
                                               T((flags & 0x000c) == 2, "PAP Authentication",
                                                 "No Authentication Required"))))
    print("    Baler Power Control - %s" % T((flags & 0x0030) == 0, "No Baler",
                                             T((flags & 0x0030) == 1, "DTR",
                                               T((flags & 0x0030) == 2, "Break", "Continuous"))))
    print("    Enable Baler/Dial-Out at Completion of Recording Window ---- %s" % T(flags & 0x0040, "Yes", "No"))
    print("    Enable Baler/Dial-Out When Data Port Trigger Level Reached - %s" % T(flags & 0x0080, "Yes", "No"))
    print("    Enable Baler/Dial-Out at Interval -------------------------- %s" % T(flags & 0x0100, "Yes", "No"))
    print("    Allow Baler to Issue Calibration Commands ------------------ %s" % T(flags & 0x0200, "Yes", "No"))
    print("    Use DTR to Control Modem for Dial-In/Out ------------------- %s" % T(flags & 0x0400, "Yes", "No"))
    print("    Allow NAPT to Baler ------ %s" % T(flags & 0x0800, "Yes", "No"))
    print("    Transparent Routing Mode - %s" % T(flags & 0x1000, "Enabled", "Disabled"))
    print("    Baler Type --------------- %s" % T(flags & 0x2000, "Baler44", "Baler14"))
    print("    Simplex Operation -------- %s" % T(flags & 0x8000, "Enabled", "Disabled"))
    print("  Baler or POC Address ------------- %s" % socket.inet_ntoa(
        struct.pack('>L', phy2.getPointOfContactOrBalerIPAddress())))
    if phy2.getDialInIPSuggestionOrBalerSecondaryIPAddress() is None:
        sip = "Disabled"
    else:
        sip = socket.inet_ntoa(struct.pack('>L', phy2.getDialInIPSuggestionOrBalerSecondaryIPAddress()))
    print("  Dial-In IP or Baler Secondary IP - %s" % sip)
    print("  POC Port Number ------------------ %s" % phy2.getPointOfContactPortNumber())
    print("  Baler Retries -------------------- %d" % phy2.getBalerRetries())
    if phy2.getBalerRegistrationTimeout() is None:
        timeout = "Not Set"
    else:
        timeout = "%d minutes" % phy2.getBalerRegistrationTimeout()
    print("  Baler Registration Timeout ------- %s" % timeout)
    if phy2.getRoutedPacketTimeout() is None:
        timeout = "Not Set"
    else:
        timeout = "%d minutes" % phy2.getRoutedPacketTimeout()
    print("  Serial Baud Rate ----------------- %s" % phy1.getBaudString(phy2.getSerialBaudRate()))
    print("  Routed Packets Timeout ----------- %s" % timeout)


def format_SPP(results):
    section = results[0]
    spp = results[1]
    print(section_map[section][0])
    print("  Maximum Main Current ---- %s" % T(spp.getMaximumMainCurrent() == 0, "Disabled",
                                               T(125 > spp.getMaximumMainCurrent() > 250, "Invalid",
                                                 "%d" % spp.getMaximumMainCurrent())))
    print("  Minimum Off Time -------- %s" % T(1 > spp.getMinimumOffTime() > 6500, "Invalid",
                                               "%d seconds" % spp.getMinimumOffTime()))
    print("  Minimum P/S Voltage ----- %s" % T(50 > spp.getMinimumPSVoltage() > 255, "Invalid",
                                               "%d" % spp.getMinimumOffTime()))
    print("  Temperature Hysteresis -- %s" % T(1 > spp.getTemperatureHysteresis() > 20, "Invalid",
                                               "%d" % spp.getTemperatureHysteresis()))
    print("  Maximum Antenna Current - %s" % T(5 > spp.getMaximumAntennaCurrent() > 128, "Invalid",
                                               "%d" % spp.getMaximumAntennaCurrent()))
    print("  Minimum Temperature ----- %s" % T(-40 > spp.getMinimumTemperature() > 0, "Invalid",
                                               "%d" % spp.getMinimumTemperature()))
    print("  Maximum Temperature ----- %s" % T(50 > spp.getMaximumTemperature() > 85, "Invalid",
                                               "%d" % spp.getMaximumTemperature()))
    print("  Voltage Hysteresis ------ %s" % T(1 > spp.getVoltageHysteresis() > 20, "Invalid",
                                               "%d" % spp.getVoltageHysteresis()))
    print("  Default VCO Value ------- %s" % T(0 > spp.getDefaultVCOValue() > 4095, "Invalid",
                                               "%d" % spp.getDefaultVCOValue()))


def format_TN(results):
    section = results[0]
    tn = results[1]
    print(section_map[section][0])
    for i in range(1, tn.getNumThreads() + 1):
        print("  %s - %s" % (str("Thread %d" % i).ljust(9), tn.getGetFunction("Thread_%d" % i)()))


section_map = {
    'AMR': ['Automatic Mass Recentering', format_AMR, {'cmd': CmdID.C2_RQAMASS}],
    'BC': ['Baler Configuration', format_BCF, {'cmd': CmdID.C3_RQBCFG}],
    'COM': ['Communications', format_COM, {'cmd': CmdID.C1_RQCOM}],
    'DCP': ['Digitizer Calibration Packet', format_DCP, {'cmd': CmdID.C1_RQDCP}],
    'DEV': ['CNP Device Info', format_DEV, {'cmd': CmdID.C1_RQDEV}],
    'DP1': ['Data Port 1', format_DP, {'cmd': CmdID.C1_RQLOG, 'DataPortNumber': 0},
            {'cmd': CmdID.C1_RQFIX}],
    'DP2': ['Data Port 2', format_DP, {'cmd': CmdID.C1_RQLOG, 'DataPortNumber': 1},
            {'cmd': CmdID.C1_RQFIX}],
    'DP3': ['Data Port 3', format_DP, {'cmd': CmdID.C1_RQLOG, 'DataPortNumber': 2},
            {'cmd': CmdID.C1_RQFIX}],
    'DP4': ['Data Port 4', format_DP, {'cmd': CmdID.C1_RQLOG, 'DataPortNumber': 3},
            {'cmd': CmdID.C1_RQFIX}],
    'EPC': ['Environmental Processor Configuration', format_EPC, {'cmd': CmdID.C2_RQEPCFG}],
    'FV': ['Fixed Values After Reboot', format_FC, {'cmd': CmdID.C1_RQFIX}],
    'GLP': ['Global Programming', format_GP, {'cmd': CmdID.C1_RQGLOB},
            {'cmd': CmdID.C1_RQFIX}],
    'GPS': ['GPS Info', format_GPS, {'cmd': CmdID.C1_RQGID},
            {'cmd': CmdID.C2_RQGPS}],
    'ETH': ['Ethernet', format_ETH, {'cmd': CmdID.C1_RQPHY},
            {'cmd': CmdID.C2_RQPHY, 'PhysicalInterfaceNumber': 3}],
    'MA': ["Manufacturer's Area", format_MA, {'cmd': CmdID.C1_RQMAN}],
    'RT': ['Routing Table', format_RT, {'cmd': CmdID.C1_RQRT}],
    'RW': ['Recording Windows', format_RW, {'cmd': CmdID.C2_RQWIN}],
    'SCM': ['Sensor Control Mapping', format_SCM, {'cmd': CmdID.C1_RQSC}],
    'SI1': ['Serial Interface 1', format_SI, {'cmd': CmdID.C1_RQPHY},
            {'cmd': CmdID.C2_RQPHY, 'PhysicalInterfaceNumber': 0}],
    'SI2': ['Serial Interface 2', format_SI, {'cmd': CmdID.C1_RQPHY},
            {'cmd': CmdID.C2_RQPHY, 'PhysicalInterfaceNumber': 1}],
    'SPP': ['Slave Processor Parameters', format_SPP, {'cmd': CmdID.C1_RQSPP}],
    'TN': ['Thread Names', format_TN, {'cmd': CmdID.C1_RQTHN}],
}

sections = {
    "Q330": [
        'AMR',
        'BC',
        'DCP',
        'DEV',
        'DP1',
        'DP2',
        'DP3',
        'DP4',
        'EPC',
        'FV',
        'GLP',
        'GPS',
        'ETH',
        'MA',
        'RT',
        'RW',
        'SCM',
        'SI1',
        'SI2',
        'SPP',
        'TN',
    ],
    "Q335": [
        'AMR',
        'COM',
        'DCP',
        'DP1',
        'DP2',
        'DP3',
        'DP4',
        'FV',
        'GLP',
        'GPS',
        'MA',
        'RT',
        'SCM',
        'SPP',
    ],
}


class QView(QAction):
    def __init__(self):
        self.private = [
            'sections',
        ]
        QAction.__init__(self)

    def val_sections(self, value):
        if type(value) not in (list, tuple):
            raise TypeError('sections')
        matches = []
        for section in value:
            for key in list(section_map.keys()):
                if key.lower().find(section.lower()) == 0:
                    matches.append(key)
        if len(matches) == 0:
            raise ValueError('sections')
        value = list(sorted(set(matches)))
        return value

    def _execute(self, q330):
        if self.sections is None:
            raise QExMessage("No sections were selected")
        for section in self.sections:
            if section not in sections[q330.getDeviceType()]:
                print("Skipping section '%s', not supported by %s" % (section_map[section][0], q330.getDeviceType()))
                continue
            if section not in section_map:
                raise QExMessage("Invalid section '%s'" % section)

            results = [section]
            for cmd_map in section_map[section][2:]:
                request = Cmds.cmdToClass[cmd_map['cmd']]()
                for key in list(cmd_map.keys()):
                    if key != 'cmd':
                        request.getSetFunction(key)(cmd_map[key])
                results.append(q330.sendCommand(request))

            section_map[section][1](results)
