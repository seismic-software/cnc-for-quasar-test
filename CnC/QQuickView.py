
import struct
import sys

from QAction import QAction
from QTools import expand_int_list_set
from Quasar.Commands.c2_rqqv import c2_rqqv


def T(expr, if_true, if_false):
    if expr:
        return if_true
    else:
        return if_false


class QQuickView(QAction):
    def __init__(self):
        self.private = [
            'channels',
        ]
        QAction.__init__(self)

        self.verbosity = 0

    def val_channels(self, value):
        if type(value) != str:
            raise TypeError('channels')
        try:
            channels = expand_int_list_set(value)
        except ValueError:
            raise ValueError('channels')
        value = int(0)
        for channel in channels:
            if 1 > channel > 6:
                raise ValueError('channels')
            value |= 1 << (channel - 1)
        if 1 > value > 63:
            raise ValueError('channels')
        return value

    def _execute(self, q330):
        request = c2_rqqv()
        request.setChannelMap(self.channels)
        next_seq = 0
        while 1:
            request.setLowestSequenceNumber(next_seq)
            result = q330.sendCommand(request)

            sequence = result.getStartingSequenceNumber()
            seconds = result.getSecondsCount()
            entries = result.getNumberOfEntries()
            channels = result.getActualChannelMask()

            for i in range(0, entries):
                channel = result.getGetFunction("Channel_%d" % i)()
                shift = result.getGetFunction("ShiftCount_%d" % i)()
                offset = result.getGetFunction("SecondsOffset_%d" % i)()
                start = result.getGetFunction("StartingValue_%d" % i)()
                data = result.getGetFunction("Differences_%d" % i)()

                if self.verbosity > 0:
                    print("")
                    print("Sequence Number: %d (channel %d)" % (sequence, channel + 1))

                minimum = 2 ** 31 - 1
                maximum = 2 ** 31 * -1

                values = []
                for j in range(0, 40):
                    diff = 0
                    if j > 0:
                        diff = struct.unpack('>b', data[j - 1:j])[0]
                    value = (diff * (1 << shift)) + start
                    if value < minimum:
                        minimum = value
                    if value > maximum:
                        maximum = value
                    values.append(value)

                parts = []
                for value in values:
                    parts.append('%d' % value)
                if self.verbosity > 0:
                    justify = max(list(map(len, parts))) + 1
                    print('\n'.join([s.rjust(justify) for s in parts]))
                    print()

                lines = 10
                span = float(maximum - minimum)
                step = float(span / lines)
                print("Channel %d Trace (sequence %d)" % (channel + 1, sequence))
                for j in range(0, lines):
                    for value in values:
                        if (minimum + (j * step)) < value:
                            sys.stdout.write(' ')
                        else:
                            sys.stdout.write('.')
                    if j == (lines - 1):
                        print(' ', minimum)
                    elif j == 0:
                        print(' ', maximum)
                    else:
                        print()
                print()

            next_seq = sequence + 1
