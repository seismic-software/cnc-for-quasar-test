from QAction import QAction
from Quasar.Commands.c1_pollsn import c1_pollsn


class QPoll(QAction):
    def __init__(self):
        self.private = []
        QAction.__init__(self)

    def _execute(self, q330):
        poll = c1_pollsn()
        poll.setSerialNumberMask(0)
        poll.setSerialNumberMatch(0)
        responses = q330.sendCommand(poll, 1)
        self._add_results(responses)
