"""
Provides a wrapper around special private items in the Child class. If a
validator or translator function is provided for a private variable, it
acts as a gateway for the assignment or retrieval of that variable.

The validator function val_<VARIABLE_NAME> can perform type and value
validations on a item prior to it being stored. It can also modify the
item, allowing multiple input types resulting in a single storage type.

The translator function tr_<VARIABLE_NAME> can verify that an item has
been set correctly (is of the right storage type) before returning
the value. This is useful to detect "illegal" storage types and values.
It can also modify the storage type prior to returning.

IMPORTANT: Child classes must define the contents of the list 'self.private'
           BEFORE calling the QClass constructor.
"""
from QTools import qassert


class QClass(object):
    def __init__(self):
        if self.__class__.__name__ == 'QClass':
            raise NotImplementedError("QClass is an abstract class")

        self._generate_private()

    """
    Here we associate the property() methods (getter, setter, deller, validator) 
    for the privated elements in the child class. If any of these methods have been 
    created for an element we associated it with that element, otherwise we use 
    the default methods.
    """
    def _generate_private(self):
        if 'private' in self.__dict__:
            qassert(type(self.__dict__['private']) == list)
            self.__dict__['data'] = {}
            for key in self.private:
                qassert(type(key) == str)
                self.__dict__['data'][key] = None

    """
    The get() and set() methods provide access to the private elements of the
    child class based on the element key.
    """
    def get(self, key):
        return self.__dict__[key]

    def set(self, key, value):
        self.__dict__[key] = value

    """
    If the 'required' array is populated, check that the required elements
    have been populated
    """
    def required_set(self):
        is_ready = True
        if 'required' in list(self.__dict__.keys()):
            for key in self.required:
                if self.get(key) is None:
                    is_ready = False
        return is_ready

    """
    Validator methods  -- verify the value, and optionally modify it before 
                          storing to the target location
    Translator methods -- modify the contents of an item before it is
                          returned to the requester

    Creating item specific validators and translators
        val_<item> -- validator method for 'item'
        tr_<item>  -- translator method for 'item'

    If you want to add default functionality to _validate or _translate
    override the _default_validator and _default_translator methods instead,
    that is why they exist. They are used when validator and/or translator 
    methods do not exist for an item.
    """

    def _validate(self, key, value):
        val_method = self._default_validator
        val_method_name = 'val_' + key
        if val_method_name in dir(self): 
            val_method = getattr(self, val_method_name)
        return val_method(value)

    def _translate(self, key, value):
        tr_method = self._default_translator
        tr_method_name = 'tr_' + key
        if tr_method_name in dir(self):
            tr_method = getattr(self, tr_method_name)
        return tr_method(value)

    def _default_validator(self, value):
        return value

    def _default_translator(self, value):
        return value

    """
    CAUTION:
        Do not modify these methods unless you are absolutely sure you
        know what you are doing. If you are not careful, you can easily
        throw a program into an infinite loop.
    """
    def __getattribute__(self, name):
        try:
            if name[0:2] == '__':
                raise AttributeError
            if 'private' not in self.__dict__:
                raise AttributeError
            if 'data' not in self.__dict__:
                raise AttributeError
            if name not in self.__dict__['private']:
                raise AttributeError
            if name not in self.__dict__['data']:
                raise AttributeError
            value = self.__dict__['data'][name]
            return self._translate(name, value)
        except AttributeError:
            return object.__getattribute__(self, name)

    def __setattr__(self, name, value):
        try:
            if name[0:2] == '__':
                raise AttributeError
            if 'private' not in self.__dict__:
                raise AttributeError
            if 'data' not in self.__dict__:
                raise AttributeError
            if name not in self.__dict__['private']:
                raise AttributeError
            if name not in self.__dict__['data']:
                raise AttributeError
            if value is not None:
                value = self._validate(name, value)
            self.__dict__['data'][name] = value
        except AttributeError:
            object.__setattr__(self, name, value)

    """
    The class will behave like a map concerning data items.
    """
    def __getitem__(self, key):
        if not self.__contains__(key):
            raise KeyError(key)
        return self.__getattribute__(key)

    def __setitem__(self, key, value):
        return self.__setattr__(key, value)

    def __delitem__(self, key):
        if not self.__contains__(key):
            raise KeyError(key)
        return self.__setattr__(key, None)

    """
    Allow for checking if an item exists and is populated.
    """
    def __contains__(self, key):
        return key in self.__dict__['data'] and (self.__dict__['data'][key] is not None)

    def contains(self, key):
        return self.__contains__(key)

    """
    Run the operation associated with this class
    """
    def execute(self, arg=None):
        raise NotImplementedError("method execute() is not implemented")
