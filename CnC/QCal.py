
import re
import time

from E300 import E300_RAW
from QAction import QAction
from QTools import expand_int_list_set, QExMessage, q330_to_unix_time
from Quasar import Q330
from Quasar.Commands.c1_qcal import c1_qcal
from Quasar.Commands.c1_cerr import c1_cerr
from QPing import QPing

CouplingMap = {
    'C': "capacitive",
    'R': "resistive",
}

WaveformMap = {
    'Q330': {
        'sine': 0,
        'red': 1,
        'white': 2,
        'step': 3,
        'random': 4,
    },
    'Q335': {
        'sine': 0,
        'red': 1,
        'white': 2,
        'step': 3,
        'random': 4,
        'timing': 7,
    },
}

SineFrequencyMap = {
    'Q330': {
        1.0: 0x0001,  # 1p
        0.5: 0x0002,  # 2p
        0.25: 0x0004,  # 4p
        0.2: 0x0005,  # 5p
        0.125: 0x0008,  # 8p
        0.1: 0x000A,  # 10p
        0.0625: 0x0010,  # 16p
        0.05: 0x0014,  # 20p
        0.04: 0x0019,  # 25p
        0.025: 0x0028,  # 40p
        0.02: 0x0032,  # 50p
        0.0125: 0x0050,  # 80p
        0.01: 0x0064,  # 100p
        0.008: 0x007D,  # 125p
        0.005: 0x00C8,  # 200p
        0.004: 0x00FA,  # 250p
    },
    'Q335': {
        20.0: 0x8014,  # 20m
        19.0: 0x8013,  # 19m
        18.0: 0x8012,  # 18m
        17.0: 0x8011,  # 17m
        16.0: 0x8010,  # 16m
        15.0: 0x800F,  # 15m
        14.0: 0x800E,  # 14m
        13.0: 0x800D,  # 13m
        12.0: 0x800C,  # 12m
        11.0: 0x800B,  # 11m
        10.0: 0x800A,  # 10m
        9.0: 0x8009,  # 9m
        8.0: 0x8008,  # 8m
        7.0: 0x8007,  # 7m
        6.0: 0x8006,  # 6m
        5.0: 0x8005,  # 5m
        4.0: 0x8004,  # 4m
        3.0: 0x8003,  # 3m
        2.0: 0x8002,  # 2m
        1.0: 0x8001,  # 1m
        1.0: 0x0001,  # 1p
        0.5: 0x0002,  # 2p
        0.25: 0x0004,  # 4p
        0.2: 0x0005,  # 5p
        0.125: 0x0008,  # 8p
        0.1: 0x000A,  # 10p
        0.0625: 0x0010,  # 16p
        0.05: 0x0014,  # 20p
        0.04: 0x0019,  # 25p
        0.025: 0x0028,  # 40p
        0.02: 0x0032,  # 50p
        0.0125: 0x0050,  # 80p
        0.01: 0x0064,  # 100p
        0.008: 0x007D,  # 125p
        0.005: 0x00C8,  # 200p
        0.004: 0x00FA,  # 250p
    },
}

RandomFrequencyMap = {
    'Q330': {
        125.0000: 0x0001,  # 1d
        62.5000: 0x0002,  # 2d
        31.2500: 0x0004,  # 4d
        25.0000: 0x0005,  # 5d
        15.6250: 0x0008,  # 8d
        12.5000: 0x000A,  # 10d
        7.8125: 0x0010,  # 16d
        6.2500: 0x0014,  # 20d
        5.0000: 0x0019,  # 25d
        2.5000: 0x0032,  # 50d
        1.5625: 0x0050,  # 80d
        1.2500: 0x0064,  # 100d
        1.0000: 0x007D,  # 125d
        0.6250: 0x00C8,  # 200d
        0.5000: 0x00FA,  # 250d
    },
    'Q3550': {
        2500.0000: 0x8014,  # 20m
        2375.0000: 0x8013,  # 19m
        2250.0000: 0x8012,  # 18m
        2125.0000: 0x8011,  # 17m
        2000.0000: 0x8010,  # 16m
        1875.0000: 0x800F,  # 15m
        1750.0000: 0x800E,  # 14m
        1625.0000: 0x800D,  # 13m
        1500.0000: 0x800C,  # 12m
        1375.0000: 0x800B,  # 11m
        1250.0000: 0x800A,  # 10m
        1125.0000: 0x8009,  # 9m
        1000.0000: 0x8008,  # 8m
        875.0000: 0x8007,  # 7m
        750.0000: 0x8006,  # 6m
        625.0000: 0x8005,  # 5m
        500.0000: 0x8004,  # 4m
        375.0000: 0x8003,  # 3m
        250.0000: 0x8002,  # 2m
        125.0000: 0x8001,  # 1m
        125.0000: 0x0001,  # 1d
        62.5000: 0x0002,  # 2d
        31.2500: 0x0004,  # 4d
        25.0000: 0x0005,  # 5d
        15.6250: 0x0008,  # 8d
        12.5000: 0x000A,  # 10d
        7.8125: 0x0010,  # 16d
        6.2500: 0x0014,  # 20d
        5.0000: 0x0019,  # 25d
        2.5000: 0x0032,  # 50d
        1.5625: 0x0050,  # 80d
        1.2500: 0x0064,  # 100d
        1.0000: 0x007D,  # 125d
        0.6250: 0x00C8,  # 200d
        0.5000: 0x00FA,  # 250d
    },
}


class QCal(QAction):
    def __init__(self, wave_form, device_type="Q330"):
        self._device_type = device_type
        self.private = [
            'channels',
            'cal_monitor',
            'amplitude',
            'coupling',
            'duration',
            'settle',
            'trailer',
            'sleep',
            'wave_form',
            'frequency',
            'polarity',
            'noise',
            'auto',
            'start_time'
        ]
        QAction.__init__(self)

        self.wave_form = wave_form
        self.auto = False
        self.auto_timing = True
        self.e300 = False
        self.e300_input = E300_RAW
        self.start_time = 0
        self.q330_time = 0
        self.group = None
        self.quiet = False

    def val_channels(self, value):
        if type(value) != str:
            raise TypeError('channels')
        try:
            values = sorted(expand_int_list_set(value))
        except ValueError:
            raise ValueError('channels')
        value = []
        for v in values:
            if 1 > v > 6:
                raise ValueError('channels')
            value.append(v)
        return value

    def val_cal_monitor(self, value):
        if type(value) != str:
            raise TypeError('cal_monitor')
        try:
            values = sorted(expand_int_list_set(value))
        except:
            raise ValueError('cal_monitor')
        value = []
        for v in values:
            if 0 > v > 6:
                raise ValueError('cal_monitor')
            if v != 0:
                value.append(v)
        return value

    def val_amplitude(self, value):
        try:
            value = int(value)
        except:
            raise TypeError('amplitude')
        if value not in [-42, -36, -30, -24, -18, -12, -6]:
            raise ValueError('amplitude')
        return value

    def val_coupling(self, value):
        if type(value) != str:
            raise TypeError('coupling')
        if value not in list(CouplingMap.keys()):
            raise ValueError('coupling')
        return value

    def val_duration(self, value):
        try:
            if re.compile(r'^\d+$').search(value):
                value = int(value)
            elif re.compile(r'^\d+s$').search(value):
                value = int(value[0:-1])
            elif re.compile(r'^\d+m$').search(value):
                value = int(value[0:-1]) * 60
            elif re.compile(r'^\d+h$').search(value):
                value = int(value[0:-1]) * 3600
            else:
                raise Exception()
        except:
            raise TypeError('duration')
        if (value < 0) or (value > 65535):
            raise ValueError('duration')
        return value

    def val_settle(self, value):
        try:
            if re.compile(r'^\d+$').search(value):
                value = int(value)
            elif re.compile(r'^\d+s$').search(value):
                value = int(value[0:-1])
            elif re.compile(r'^\d+m$').search(value):
                value = int(value[0:-1]) * 60
            elif re.compile(r'^\d+h$').search(value):
                value = int(value[0:-1]) * 3600
            else:
                raise Exception()
        except:
            raise TypeError('settle')
        if (value < 0) or (value > 65535):
            raise ValueError('settle')
        return value

    def val_trailer(self, value):
        try:
            if re.compile(r'^\d+$').search(value):
                value = int(value)
            elif re.compile(r'^\d+s$').search(value):
                value = int(value[0:-1])
            elif re.compile(r'^\d+m$').search(value):
                value = int(value[0:-1]) * 60
            elif re.compile(r'^\d+h$').search(value):
                value = int(value[0:-1]) * 3600
            else:
                raise Exception()
        except:
            raise TypeError('trailer')
        if (value < 0) or (value > 65535):
            raise ValueError('trailer')
        return value

    def val_sleep(self, value):
        try:
            if re.compile(r'^\d+$').search(value):
                value = int(value)
            elif re.compile(r'^\d+s$').search(value):
                value = int(value[0:-1])
            elif re.compile(r'^\d+m$').search(value):
                value = int(value[0:-1]) * 60
            elif re.compile(r'^\d+h$').search(value):
                value = int(value[0:-1]) * 3600
            else:
                raise Exception()
        except:
            raise TypeError('sleep')
        if (value < 0) or (value > 65535):
            raise ValueError('sleep')
        return value

    def val_wave_form(self, value):
        if type(value) != str:
            raise TypeError('wave_form')
        wave_forms = ['sine', 'random', 'step']
        if self._device_type == 'Q335':
            wave_forms.append('timing')
        if value not in wave_forms:
            raise ValueError('wave_form')
        return value

    def val_frequency(self, value):
        try:
            if self.wave_form == 'sine':
                if re.compile(r'^\d+p$').search(value):
                    value = int(value[:-1])
                elif (self._device_type == 'Q335') and re.compile(r'^\d+m$').search(value):
                    value = 0x8000 | int(value[:-1])
                else:
                    value = SineFrequencyMap[self._device_type][float(value)]
            elif self.wave_form == 'random':
                if re.compile(r'^\d+d$').search(value):
                    value = int(value[:-1])
                elif (self._device_type == 'Q335') and re.compile(r'^\d+m$').search(value):
                    value = 0x8000 | int(value[:-1])
                else:
                    value = RandomFrequencyMap[self._device_type][float(value)]
            else:
                raise Exception()
            if 0 > value > 255:
                if (self._device_type != 'Q335') or (0x8000 > value > 0x8014):
                    raise ValueError('frequency')
        except KeyError as e:
            raise ValueError('frequency')
        except ValueError as e:
            raise ValueError('frequency')
        except:
            raise TypeError('frequency')
        return value

    def val_polarity(self, value):
        if type(value) != str:
            raise TypeError('polarity')
        if value not in ['positive', 'negative']:
            raise ValueError('polarity')
        return value

    def val_noise(self, value):
        if type(value) != str:
            raise TypeError('noise')
        if value not in ['red', 'white', 'none']:
            raise ValueError('noise')
        return value

    def val_auto(self, value):
        if type(value) not in (str, bool, int):
            raise TypeError('auto')
        if value not in ['true', 'false', True, False, 1, 0]:
            raise ValueError('auto')
        return value in ['true', True, 1]

    def val_start_time(self, value):
        try:
            value = int(value)
        except:
            raise TypeError('start_time')
        if (value < (-1 * (2 ** 31))) or (value > ((2 ** 31) - 1)):
            raise ValueError('start_time')
        return value

    def _execute(self, q330):
        # Arguments from the user
        arg_cal_monitor = self.cal_monitor
        arg_amplitude = self.amplitude
        arg_coupling = self.coupling
        arg_frequency = self.frequency

        # Derived arguments
        arg_coupling_comment = None

        ping = QPing(q330.getDeviceType())
        ping.action = 'status'
        ping.status = ['Global']
        ping.suppress_stdout()
        ping.execute(q330)
        result_data = ping.get_result_data()[0]
        sec_off = result_data['SecondsOffset'][2]
        cdsn = result_data['CurrentDataSequenceNumber'][2]
        self.q330_time = sec_off + cdsn
        extra_seconds = time.gmtime(q330_to_unix_time(self.q330_time))[5]
        settle_adjust = 0
        if self.settle > 0:
            settle_adjust = self.settle + 60 - (self.settle % 60)
        if self.auto_timing:
            if extra_seconds:
                self.start_time = self.q330_time + (120 - extra_seconds) + settle_adjust
            else:
                self.start_time = self.q330_time + 60 + settle_adjust
        else:
            self.start_time = 0

        # Fixed arguments
        arg_start_time = self.start_time
        arg_autocal = self.auto

        arg_duration = self.duration
        arg_settle = self.settle
        arg_trailer = self.trailer

        arg_divider = 1
        if self.frequency:
            arg_divider = self.frequency

        arg_channels = self.channels

        arg_negative_step = False
        if (self.wave_form == 'step') and (self.polarity == 'negative'):
            arg_negative_step = True

        wave_form_key = self.wave_form
        if self.wave_form == 'random':
            if self.noise != 'none':
                wave_form_key = self.noise

        arg_amplitude_code = int(((self.amplitude * -1) / 6) - 1)

        # construct the waveform bitmap
        arg_waveform = WaveformMap[q330.getDeviceType()][wave_form_key]
        if arg_negative_step:
            arg_waveform |= 0x1 << 6
        if arg_autocal:
            arg_waveform |= 0x1 << 7

        sensors = {}
        # construct the calibration bitmap
        # (which channels to calibrate)
        channel_map = 0
        for channel in arg_channels:
            channel_map |= 1 << (channel - 1)
            if 0 < channel < 4:
                sensors['A'] = True
            elif 3 < channel < 7:
                sensors['B'] = True
        arg_calibration_bitmap = channel_map

        # construct the sensor control bitmap
        # (which lines to activate during the calibration)
        actions = []

        # Check sensor control bitmap in Q330
        for sensor in list(sensors.keys()):
            try:
                q330.getSensorControlBitmap([('calibration', sensor)])
                actions.append(('calibration', sensor))
            except Q330.Q330ExSODDisabled as e:
                pass

        if self.coupling == 'C':
            for sensor in list(sensors.keys()):
                actions.append(('coupling', sensor))
        arg_sensor_control_bitmap = 0
        if len(actions):
            try:
                arg_sensor_control_bitmap = int(q330.getSensorControlBitmap(actions))
            except Q330.Q330ExSODDisabled as e:
                self._print(str(e))
                raise QExMessage(str(e))
            except Q330.Q330ExSODDuplicate as e:
                self._print(str(e))
                raise QExMessage(str(e))

        # construct the monitor channel bitmap
        # (which channels digitize the output of the calibrator)
        arg_cal_monitor_channel_bitmap = 0
        for monitor in arg_cal_monitor:
            arg_cal_monitor_channel_bitmap |= 1 << (monitor - 1)

        # construct the 12 coupling bytes 
        arg_coupling_bytes = CouplingMap[self.coupling].encode()

        cal_request = c1_qcal()
        cal_request.setStartingTime(arg_start_time)
        cal_request.setWaveform(arg_waveform)
        cal_request.setAmplitude(arg_amplitude_code)
        cal_request.setDurationInSeconds(arg_duration)
        cal_request.setSettlingTimeInSeconds(arg_settle)
        cal_request.setCalibrationBitmap(arg_calibration_bitmap)
        cal_request.setTrailerTimeInSeconds(arg_trailer)
        cal_request.setSensorControlBitmap(arg_sensor_control_bitmap)
        cal_request.setMonitorChannelBitmap(arg_cal_monitor_channel_bitmap)
        cal_request.setFrequencyDivider(arg_divider)
        cal_request.setCouplingBytes(arg_coupling_bytes)

        response = q330.sendCommand(cal_request, 1)

        # TODO: is this checking response type? should probably be isinstance
        if response[0] == c1_cerr:
            raise QExMessage("Calibrate - Failed to Start Calibration: %s" % response[0].getErrorText())

        if not self.quiet:
            if self.start_time:
                cal_time_string = time.strftime("%Y/%m/%d %H:%M:%S UTC",
                                                time.gmtime(q330_to_unix_time(self.start_time)))
                self._print("Scheduling calibration for %s..." % cal_time_string)
            else:
                cal_time_string = time.strftime("%Y/%m/%d %H:%M:%S UTC", time.gmtime(q330_to_unix_time(self.q330_time)))
                self._print("Starting calibration now (%s)..." % cal_time_string)

            # self._print("  Sensor     = %s" % self.sensor)
            # self._print("    Bitmap : %s" % hex(arg_sensor_control_bitmap))
            # self._print("  Channel(s) = %s" % ", ".join(map(str,arg_channels)))
            # self._print("    Bitmap : %s" % hex(arg_calibration_bitmap))
            # self._print("  Monitor    = %d" % arg_cal_monitor)
            # self._print("    Bitmap : %s" % hex(arg_cal_monitor_channel_bitmap))
            # if waveform_key in ['red', 'white']:
            #    self._print("  Waveform   = random (%s noise)" % waveform_key)
            # else:
            #    self._print("  Waveform   = %s" % waveform_key)
            # self._print("    Code : %d" % arg_waveform)
            # self._print("  Duration   = %d seconds" % arg_duration)
            # self._print("  Amplitude  = %d dB" % arg_amplitude)
            # self._print("    Code : %d" % arg_amplitude_code)
            # if waveform_key == 'sine':
            #    self._print("  Frequency  = %.04f" % arg_frequency)
            #    self._print("    Divider : %d" % arg_divider)
            # self._print("  Coupling   = %s" % arg_coupling_comment)
            # self._print("  Start Time = %d" % arg_start_time)
            # self._print("  Settling   = %d seconds" % arg_settle)
            # self._print("  Trailer    = %d seconds" % arg_trailer)
