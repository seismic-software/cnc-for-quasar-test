
import optparse
import time

from QPing import QPing
from Quasar import QDPDevice
from QTools import assert_arg_count, QExArgs, qassert
from QScript import QScript


class QSMonitor(QScript):
    def __init__(self):
        self.option_list = [
            optparse.make_option("-a", "--address", type="string", action="store", dest="address"),
            optparse.make_option("-d", "--delay", type="int", action="store", dest="delay"),
            optparse.make_option("-i", "--info", action="store_true", dest="info"),
            optparse.make_option("-p", "--port", type="int", action="store", dest="port"),
            optparse.make_option("--show-unavailable", action="store_true", dest="show_unavailable", default=False),
            optparse.make_option("--show-restricted", action="store_true", dest="show_restricted", default=False),
        ]

        QScript.__init__(self)

    def local_usage(self):
        return """usage: %prog [options] <device> [status]
The default behavior (no status selected) is to perform latency tests.

device:  integer value identifying the Q330
status:  status to display
            ARP
            AuxiliaryBoard
            Baler
            BoomPosition
            DataPort1
            DataPort2
            DataPort3
            DataPort4
            DynamicIP
            EnvironmentalProcessor
            Ethernet
            Global
            Gps
            PLL
            Satellites
            Serial1
            Serial2
            Serial3
            SerialSensor

         Restricted Statuses (require --show-restricted option):
            PowerSupply
            Thread

Status options are case insensitive, and require that only
a sufficient portion of the string be entered such that it
is unique within the list of status options."""

    def _run(self):
        assert_arg_count(self.args, 'gte', 1)

        arg_device_id = self.args[0]
        self.comm.set_from_file(arg_device_id, self.options.reg_file)

        arg_status = ['Detect']
        if self.options.address:
            self.comm.set_address(self.options.address)
        if self.options.port:
            self.comm.set_port(self.options.port)
        if self.options.info:
            arg_status = ['info']

        if len(self.args) > 1:
            arg_status = self.args[1:]

        arg_delay = 3  # 3 second default delay
        if self.options.delay:
            arg_delay = self.options.delay

        action = QPing(self.comm.get_device_type(), self.options.show_restricted, self.options.show_unavailable)
        try:
            if 'all' in arg_status:
                raise ValueError('status')
            if arg_status is not None:
                if 'Detect' in arg_status:
                    action.action = 'detect'
                elif 'info' in map(lambda a: a.lower(), arg_status):
                    action.action = 'info'
                else:
                    action.action = 'status'
                    action.status = arg_status
            else:
                action.action = 'monitor'
        except TypeError as msg:
            raise QExArgs("invalid type for argument '%s'" % msg.__str__())
        except ValueError as msg:
            raise QExArgs("invalid value for argument '%s'" % msg.__str__())

        while True:
            try:
                self.comm.execute(action)
            except QDPDevice.TimeoutException as e:
                print("Command Timed Out. Trying Again...")
            time.sleep(arg_delay)


script = QSMonitor()
script.run()
