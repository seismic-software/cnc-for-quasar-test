
import getpass

from Config import raw_to_config
from QAction import QAction
from QTools import QExMessage, QExArgs
from Quasar.Commands.c1_sauth import c1_sauth
from Store import StoreFile


CFG_PROMPTS = {'SER': 'Q330 Serial Number',
               'CFG': 'Auth. Code for the Configuration Port',
               'SPF': 'Auth. Code for the Special Functions Port',
               'DP1': 'Auth. Code for Data Port 1',
               'DP2': 'Auth. Code for Data Port 2',
               'DP3': 'Auth. Code for Data Port 3',
               'DP4': 'Auth. Code for Data Port 4'}
CFG_KEYS = ['SER', 'CFG', 'SPF', 'DP1', 'DP2', 'DP3', 'DP4']


def verify_code(hex_str):
    if type(hex_str) != str:
        raise TypeError("Invalid type for hex code")
    hex_str = hex_str.strip()
    if len(hex_str) > 16:
        raise ValueError("Code is too long")
    hex_code = 0xffffffffffffffff & int('0x%s' % hex_str, 16)
    return hex_code


class QAuth(QAction):
    def __init__(self):
        self.private = [
        ]
        QAction.__init__(self)

        self.file = ''
        self.config = {
            'SER': 0,
            'CFG': 0,
            'SPF': 0,
            'DP1': 0,
            'DP2': 0,
            'DP3': 0,
            'DP4': 0,
        }

    def _get_config(self):
        for key in CFG_KEYS:
            prompt = CFG_PROMPTS[key]
            done = False
            while not done:
                try:
                    code = verify_code(getpass.getpass("%s: " % prompt))
                    self.config[key] = code
                    done = True
                except ValueError as e:
                    print("Invalid value. Must be a hexidecimal string no longer than 16 digits.")

    def _get_config_from_file(self):
        try:
            raw = StoreFile(self.file).read()
        except IOError as e:
            raise QExMessage(str(e))
        try:
            config = raw_to_config(raw)
            for key, value in list(config.items()):
                if key not in CFG_KEYS:
                    raise QExArgs("Invalid key encountered for AUTH_FILE")
                try:
                    self.config[key] = verify_code(value)
                except ValueError as e:
                    QExMessage("Invalid value for key '%s' in AUTH_FILE" % key)
        except ValueError as e:
            raise QExMessage(str(e))

    def _execute(self, q330):
        if self.file:
            self._get_config_from_file()
        else:
            self._get_config()

        # if not self.config['SER']:
        #    raise QExMessage("Q330 Serial Number must be populated, and cannot evaluate to zero.")

        for key in CFG_KEYS[1:]:
            if not self.config[key]:
                print("No %s. Auth-code will be removed." % CFG_PROMPTS[key])

        selection = 'M'
        first = True
        while selection.upper() not in ('Y', 'N', ''):
            if first:
                first = False
            else:
                print("Invalid response.")
            selection = input("Do you wish to write new authorization codes to the Q330? (y/N): ")

        if selection.upper() in ('N', ''):
            print("You have elected not to apply new authorization codes.")
        else:
            print("Writing new authorization codes...")
            auth = c1_sauth()
            auth.setSerialNumber(self.config['SER'])
            auth.setDataPort1AuthorizationCode(self.config['DP1'])
            auth.setDataPort2AuthorizationCode(self.config['DP2'])
            auth.setDataPort3AuthorizationCode(self.config['DP3'])
            auth.setDataPort4AuthorizationCode(self.config['DP4'])
            auth.setConfigurationPortAuthorizationCode(self.config['CFG'])
            auth.setSpecialFunctionsPortAuthorizationCode(self.config['SPF'])

            print(auth)

            response = q330.sendCommand(auth, 0)
            print("Response:", response)
            print("Done. Reboot to apply.")
