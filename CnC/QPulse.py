
from QAction import QAction
from QTools import QExMessage, QExArgs
from Quasar.Q330 import QDP_C1_CERR, Q330ExArgs, Q330ExSODDisabled, Q330ExSODDuplicate


class QPulse(QAction):
    def __init__(self):
        self.private = [
            'action',
            'sensor',
            'duration'
        ]
        QAction.__init__(self)

    def val_action(self, value):
        if type(value) != str:
            raise TypeError('action')
        if value not in ('lock', 'unlock', 'recenter'):
            raise ValueError('action')
        return value

    def val_sensor(self, value):
        if type(value) != str:
            raise TypeError('sensor')
        if value not in ('A', 'B'):
            raise ValueError('sensor')
        return value

    def val_duration(self, value):
        try:
            value = int(value)
        except:
            raise TypeError('duration')
        if (value < 1) or (value > 2000):
            raise ValueError('duration')
        return value

    def _execute(self, q330):
        try:
            q330.pulse(self.action, self.sensor, self.duration)
            self._print("%s Pulse Sent" % self.action.upper())
        except QDP_C1_CERR as e:
            raise QExMessage("%s - Send Failed" % self.action)
        except Q330ExArgs as e:
            raise QExArgs("%s - Invalid Arguments" % self.action)
        except Q330ExSODDisabled as e:
            raise QExMessage(e.__str__())
        except Q330ExSODDuplicate as e:
            raise QExMessage(e.__str__())
