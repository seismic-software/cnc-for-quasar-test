
from QAction import QAction
from Quasar.Commands.c1_stop import c1_stop


class QCalStop(QAction):
    def __init__(self):
        self.private = []
        QAction.__init__(self)

    def _execute(self, q330):
        cal_request = c1_stop()
        response = q330.sendCommand(cal_request, 1)
        self._print("Stoping Calibration...")
