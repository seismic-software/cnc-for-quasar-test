
import optparse

from QPing import QPing
from QTools import assert_arg_count, QExArgs
from QScript import QScript


class QSPing(QScript):
    def __init__(self):
        self.option_list = [
            optparse.make_option("-a", "--address", type="string", action="store", dest="address"),
            optparse.make_option("-d", "--detect", action="store_true", dest="detect"),
            optparse.make_option("-i", "--info", action="store_true", dest="info"),
            optparse.make_option("-p", "--port", type="int", action="store", dest="port"),
            optparse.make_option("--show-unavailable", action="store_true", dest="show_unavailable", default=False),
            optparse.make_option("--show-restricted", action="store_true", dest="show_restricted", default=False),
        ]

        QScript.__init__(self)

    def local_usage(self):
        return """usage: %prog [options] <device> [status[ status...]]

device:  integer value identifying the Q330
status:  status to display ('all' to display all of them)
            ARP
            AuxiliaryBoard
            Baler
            BoomPosition
            DataPort1
            DataPort2
            DataPort3
            DataPort4
            DynamicIP
            EnvironmentalProcessor
            Ethernet
            Global
            Gps
            PLL
            Satellites
            Serial1
            Serial2
            Serial3
            SerialSensor

         Restricted Statuses (require --show-restricted option)
            PowerSupply
            Thread
            
Status options are case insensitive, and require that only
a sufficient portion of the beginning of at least one string 
be entered. Results are returend for every match."""

    def _run(self):
        assert_arg_count(self.args, 'gte', 1)

        arg_device_id = self.args[0]
        self.comm.set_from_file(arg_device_id, self.options.reg_file)

        if self.options.address:
            self.comm.set_address(self.options.address)
        if self.options.port:
            self.comm.set_port(self.options.port)

        action = QPing(self.comm.get_device_type(), self.options.show_restricted, self.options.show_unavailable)
        try:
            if self.options.detect:
                action.action = 'detect'
            elif self.options.info:
                action.action = 'info'
            elif len(self.args) > 1:
                action.action = 'status'
                action.status = self.args[1:]
            else:
                action.action = 'monitor'
        except TypeError as msg:
            raise QExArgs("invalid type for argument '%s'" % msg.__str__())
        except ValueError as msg:
            raise QExArgs("invalid value for argument '%s'" % msg.__str__())

        self.comm.execute(action)


script = QSPing()
script.run()
