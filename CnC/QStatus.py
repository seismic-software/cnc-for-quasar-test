
import calendar
import logging
import time

from Quasar import Status
from QTools import Counter, q330_to_unix_time, fancy_integer, format_ip, format_mac


# We format the status information here so we don't have to
# hardcode the transformations into the applications using
# the status results.


def formatGlobal(values):
    if not values:
        return None
    values = Status.StatusDict(values, prefix="Global_")
    idx = Counter()
    output = {'AcquisitionControl': (
    idx.inc(), "Acquisition Control", values['AcquisitionControl'])}

    value = values['ClockQuality']
    result_str = ""
    if ((value & 0xc0) >> 6) == 1:
        result_str += "PLL Hold"
    elif ((value & 0xc0) >> 6) == 2:
        result_str += "PLL Tracking"
    elif ((value & 0xc0) >> 6) == 3:
        result_str += "PLL Locked"
    else:
        result_str += "PLL not enabled"
    if not (value & 0x01):
        result_str += " (Clock has never been locked)"
    elif value & 0x20:
        result_str += " (Speculative Lock based on internal clock)"
    elif value & 0x10:
        result_str += " (Timemarks currently frozen due to filtering)"
    elif value & 0x04:
        result_str += " (Clock has 3-D lock)"
    elif value & 0x02:
        result_str += " (Clock has 2-D lock)"
    elif value & 0x08:
        result_str += " (Clock has 1-D lock)"
    else:
        result_str += " (Clock has been locked)"
    output['ClockQuality'] = (idx.inc(), "Clock Quality", result_str)
    output['MinutesSinceLoss'] = (idx.inc(), "Last Locked", "Clock last locked %d minutes ago" % values['MinutesSinceLoss'])
    output['AnalogVoltageControlValue'] = (idx.inc(), "Analog Voltage Control Value", values['AnalogVoltageControlValue'])
    output['SecondsOffset'] = (idx.inc(), "Seconds Offset", values['SecondsOffset'])
    output['USecOffset'] = (idx.inc(), "Microseconds Offset", values['USecOffset'])
    output['TotalTimeInSeconds'] = (idx.inc(), "Total Time in Seconds", values['TotalTimeInSeconds'])
    output['PowerOnTimeInSeconds'] = (idx.inc(), "Power on Time in Seconds", values['PowerOnTimeInSeconds'])

    seconds = values['TimeOfLastResync']
    time_station = q330_to_unix_time(seconds)
    output['TimeOfLastResync'] = (idx.inc(), "Time of Last Resync", time.strftime("%Y/%m/%d %H:%M:%S", time.gmtime(time_station)))
    output['TotalNumberOfResyncs'] = (idx.inc(), "Total Number of Resyncs", values['TotalNumberOfResyncs'])
    output['GPSStatus'] = (idx.inc(), "GPS Status", values['GPSStatus'])

    value = values['CalibratorStatus']
    result_str = ""
    if value & 0x01:
        result_str += "Enabled"
        if value & 0x02:
            result_str += " [Signal On]"
        else:
            result_str += " [Signal Off]"
        if value & 0x04:
            result_str += " (Calibrator should be generating a signal, but isn't)"
    else:
        result_str += "Disabled"
    output['CalibratorStatus'] = (idx.inc(), "Calibrator Status", result_str)
    output['SensorControlBitmap'] = (idx.inc(), "Sensor Control Bitmap", hex(values['SensorControlBitmap']))
    output['CurrentVCO'] = (idx.inc(), "Current VCO", values['CurrentVCO'])
    output['DataSequenceNumber'] = (idx.inc(), "Data Sequence Number", values['DataSequenceNumber'])
    value = "No"
    if values['PLLRunningIfSet']:
        value = "Yes"
    output['PLLRunningIfSet'] = (idx.inc(), "PLL Running", value)
    output['StatusInputs'] = (idx.inc(), "Status Inputs", values['StatusInputs'])
    output['MiscInputs'] = (idx.inc(), "Miscellaneous Inputs", values['MiscInputs'])
    output['CurrentDataSequenceNumber'] = (idx.inc(), "Current Data Sequence Number", values['CurrentDataSequenceNumber'])

    seconds = values['SecondsOffset'] + values['CurrentDataSequenceNumber']
    usecs = values['USecOffset']
    time_station = q330_to_unix_time(seconds)
    output['StationTime'] = (idx.inc(), "Station Time",
                             f"{time.strftime('%Y/%m/%d %H:%M:%S', time.gmtime(time_station))}.{usecs:06d}")

    return output


def formatGps(values):
    if not values:
        return None
    values = Status.StatusDict(values, prefix="Gps_")
    idx = Counter()
    output = {}

    value = values['GPSOnIfNotZero']
    seconds = values['GPSPowerOnOffTime']
    if not value:
        output['GPSOnIfNotZero'] = (idx.inc(), "GPS enabled", "No")
        output['GPSPowerOnOffTime'] = (idx.inc(), "GPS has been off", "%d seconds" % seconds)
    else:
        output['GPSOnIfNotZero'] = (idx.inc(), "GPS enabled", "Yes")
        output['GPSPowerOnOffTime'] = (idx.inc(), "GPS has been on", "%d seconds" % seconds)

    output['NumberOfSatellitesUsed'] = (idx.inc(), "Number of Satellites in Use", values['NumberOfSatellitesUsed'])
    output['NumberOfSatellitesInView'] = (idx.inc(), "Number of Satellites in View", values['NumberOfSatellitesInView'])
    output['GPSTimeString'] = (idx.inc(), "GPS Time String", values['GPSTimeString'])
    output['GPSDateString'] = (idx.inc(), "GPS Date String", values['GPSDateString'])
    output['GPSFixString'] = (idx.inc(), "GPS Fix String", values['GPSFixString'])
    output['GPSHeightString'] = (idx.inc(), "GPS Height String", values['GPSHeightString'])
    output['GPSLatitudeString'] = (idx.inc(), "GPS Latitude String", values['GPSLatitudeString'])
    output['GPSLongitudeString'] = (idx.inc(), "GPS Longitude String", values['GPSLongitudeString'])

    value = values['TimeOfLastGood1PPS']
    value_string = "Never"
    if value:
        value_string = f"{time.strftime('%Y/%m/%d %H:%M:%S', time.gmtime(q330_to_unix_time(value)))}"
    output['TimeOfLastGood1PPS'] = (idx.inc(), "Last good 1PPS", value_string)
    output['TotalChecksumErrors'] = (idx.inc(), "Total Checksum Errors",
                                     f"{values['TotalChecksumErrors']:d} checksum errors")

    return output


def formatPowerSupply(values):  # SMU Status
    if not values:
        return None
    values = Status.StatusDict(values, prefix="PowerSupply_")
    idx = Counter()
    output = {}
    value = values['ChargingPhase']
    value_string = "Not Charging"
    if value == 1:
        "Bulk"
    elif value == 2:
        "Absorbtion"
    elif value == 3:
        "Float"
    elif value == 4:
        "Equalization"
    output['ChargingPhase'] = (idx.inc(), "Charging Phase", value_string)
    output['BatteryTemperature'] = (idx.inc(), "Battery Temperature",
                                    f"{values['BatteryTemperature']:d} C")
    output['BatteryCapacity'] = (idx.inc(), "Battery Capacity",
                                 f"{values['BatteryCapacity']:d}%")
    output['DepthOfDischarge'] = (idx.inc(), "Depth of Discharge",
                                  f"{values['DepthOfDischarge']:d}%")
    output['BatteryVoltage'] = (idx.inc(), "Battery Voltage",
                                f"{float(values['BatteryVoltage']) / 1000.0:.03f} V")
    output['InputVoltage'] = (idx.inc(), "Input Voltage",
                              f"{float(values['InputVoltage']) / 1000.0:.03f} V")
    value = float(values['BatteryCurrent'])
    value_string = f"{value / 1000.0:.03f} A"
    if value >= 0:
        value_string += " (Charging)"
    output['BatteryCurrent'] = (idx.inc(), "Battery Current", value_string)
    output['AbsorptionSetPoint'] = (idx.inc(), "Absorption Set-Point",
                                    f"{float(values['AbsorptionSetPoint']) / 1000.0:.03f} V")
    output['FloatSetPoint'] = (idx.inc(), "Float Set-Point",
                               f"{float(values['FloatSetPoint']) / 1000.0:.03f} V")
    output['Alerts'] = (idx.inc(), "Alerts", hex(values['Alerts']))
    output['LoadsOff'] = (idx.inc(), "Loads Off", hex(values['LoadsOff']))

    return output


def formatBoomPosition(values):
    if not values:
        return None
    values = Status.StatusDict(values, prefix="BoomPosition_")
    idx = Counter()
    output = {}
    show_images = True

    for i in range(1, 7):
        key = "Channel%dBoom" % i
        if key in values:
            position = values[key]
            result_str = ""
            if show_images:
                offset = 1
                image = "[---------]"
                if position < -105:
                    offset = 1
                elif -105 <= position < -75:
                    offset = 2
                elif -75 <= position < -45:
                    offset = 3
                elif -45 <= position < -15:
                    offset = 4
                elif -15 <= position < 15:
                    offset = 5
                elif 15 <= position < 45:
                    offset = 6
                elif 45 <= position < 75:
                    offset = 7
                elif 75 <= position < 105:
                    offset = 8
                elif 105 <= position:
                    offset = 9

                result_str += " " + image[0:offset]
                result_str += '|'
                result_str += image[offset + 1:]
            result_str += " %d" % position
        else:
            result_str = "unknown"
        output[key] = (idx.inc(), "Boom %d Position" % i, result_str)

    output['AnalogPositiveSupply'] = (idx.inc(), "Analog Positive Supply",
                                      f"{values['AnalogPositiveSupply'] / 100.0:.02f} V")
    output['AnalogNegativeSupply'] = (idx.inc(), "Analog Negative Supply",
                                      f"{values['AnalogPositiveSupply'] / 100.0:.02f} V")
    output['InputPower'] = (idx.inc(), "Input Power",
                            f"{values['InputPower'] * 15.0 / 100.0:.02f} V")
    output['SystemTemperature'] = (idx.inc(), "System Temperature",
                                   f"{values['SystemTemperature']:d} C")
    output['MainCurrent'] = (idx.inc(), "Main Current",
                             f"{values['MainCurrent'] / 1000.0:.03f} A")
    output['GPSAntennaCurrent'] = (idx.inc(), "GPS Antenna Current",
                                   f"{values['GPSAntennaCurrent'] / 1000.0:.03f} A")

    for i in range(1, 3):
        key = f"Seismometer{i:d}Temperature"
        value = values[key]
        result_str = "Unavailable"
        if value != (0x200 | 0x80 | 0x1a):
            result_str = "%d C" % value
        output[key] = (idx.inc(), f"Seismometer {i:d} Temperature", result_str)

    output['SlaveProcessorTimeouts'] = (idx.inc(), "Slave Processor Timeouts", values['SlaveProcessorTimeouts'])

    return output


def formatThread(values):
    if not values:
        return None
    values = Status.StatusDict(values, prefix="Thread_")
    idx = Counter()
    output = {}

    num_threads = values['NumberOfEntries']
    output['NumberOfEntries'] = (idx.inc(), "Number of Threads", num_threads)
    output['SizeOfThisBlock'] = (idx.inc(), "Size of this Block",
                                 f"{values['SizeOfThisBlock']:d} bytes")

    system_time_ms = (values['TotalSystemTimeInHigh32'] << 16) + (values['TotalSystemTimeInLow16'])
    output['TotalSystemTime'] = (idx.inc(), "Total System Time",
                                 f"{system_time_ms:d} ms")

    for i in range(0, num_threads):
        key_total_running_time_high_32 = f"TotalRunningTimeHigh32_{i:d}"
        key_total_running_time_low_16 = f"TotalRunningTimeLow16_{i:d}"
        key_time_last_run_high_32 = f"TimeSinceLastRunHigh32_{i:d}"
        key_time_last_run_low_16 = f"TimeSinceLastRunLow16_{i:d}"

        key_total_running_time = f"TotalRunningTime_{i:d}"
        key_priority = f"Priority_{i:d}"
        key_priority_counter = f"PriorityCounter_{i:d}"
        key_time_last_run = f"TimeSinceLastRun_{i:d}"
        key_flags = f"Flags_{i:d}"

        key_event = f"EventInfo_{i:d}"

        value = (values[key_total_running_time_high_32] << 16) + values[key_total_running_time_low_16]
        output[key_total_running_time] = (idx.inc(), "Total Running Time",
                                          f"{value:d} ms")
        output[key_priority] = (idx.inc(), "Priority", values[key_priority])
        output[key_priority_counter] = (idx.inc(), "Priority Counter", values[key_priority_counter])
        value = (values[key_time_last_run_high_32] << 16) + values[key_time_last_run_low_16]
        output[key_time_last_run] = (idx.inc(), "Time Last Run",
                                     f"{value:d} ms")

        value = values[key_flags]
        result_str = ""
        if value & 0x2000:
            event_value = value & 0x1fff
            result_str += f"Global Event Occured (Value: {event_value:d}). "
        if value & 0x4000:
            result_str += "Waiting on an Event. "
        if value & 0x8000:
            result_str += "Blocked. "
        output[key_flags] = (idx.inc(), "Thread Status", result_str)

    return output


def formatPLL(values):
    if not values:
        return None
    values = Status.StatusDict(values, prefix="PLL_")
    idx = Counter()
    output = {'InitialVCO': (idx.inc(), "Initial VCO", values['InitialVCO']),
              'TimeError': (idx.inc(), "Time Error", values['TimeError']),
              'RMSVCO': (idx.inc(), "RMS VCO", values['RMSVCO']),
              'BestVCO': (idx.inc(), "Best VCO", values['BestVCO']),
              'Spare': (idx.inc(), "Spare", None), 'TicksSinceLastUpdate': (
        idx.inc(), "Ticks Since Last Update", values['TicksSinceLastUpdate']),
              'Km': (idx.inc(), "Km", values['Km'])}

    value = values['State']
    if value == 0x40:
        result_str = "Hold"
    elif value == 0x80:
        result_str = "Tracking"
    elif value == 0xc0:
        result_str = "Locked"
    else:
        result_str = "PLL not enabled"
    output['State'] = (idx.inc(), "State", result_str)
    return output


def formatSatellites(values):
    if not values:
        return None
    values = Status.StatusDict(values, prefix="Satellites_")
    idx = Counter()
    output = {}

    num_entries = values['NumberOfEntries']
    output['NumberOfEntries'] = (idx.inc(), "Number of Satellites", num_entries)
    output['LengthOfThisBlock'] = (idx.inc(), "Length of this Block",
                                   f"{values['LengthOfThisBlock']:d} bytes")

    for i in range(0, num_entries):
        key_satellite_number = f"SatelliteNumber_{i:d}"
        key_elevation = f"Elevation_{i:d}"
        key_azimuth = f"Azimuth_{i:d}"
        key_snr = f"SNR_{i:d}"

        output[key_satellite_number] = (idx.inc(), "Satellite Number", values[key_satellite_number])
        output[key_elevation] = (idx.inc(), "Elevation", values[key_elevation])
        output[key_azimuth] = (idx.inc(), "Azimuth", values[key_azimuth])
        output[key_snr] = (idx.inc(), "Signal-to-Noise Ratio", values[key_snr])

    return output


def formatARP(values):
    if not values:
        return None
    values = Status.StatusDict(values, prefix="ARP_")
    idx = Counter()
    output = {}

    num_entries = values['NumberOfEntries']
    output['NumberOfEntries'] = (idx.inc(), "Number of Entries", num_entries)
    output['LengthOfThisBlock'] = (idx.inc(), "Length of this Block",
                                   f"{values['LengthOfThisBlock']:d} bytes")

    for i in range(0, num_entries):
        key_ip = f"IPAddress_{i:d}"
        key_mac_high_32 = f"MACAddressHigh32_{i:d}"
        key_mac_low_16 = f"MACAddressLow16_{i:d}"
        key_mac = f"MACAddress_{i:d}"
        key_timeout = f"TimeoutInSeconds_{i:d}"
        value_ip = values[key_ip]
        value_mac_high_32 = int(values[key_mac_high_32])
        value_mac_low_16 = int(values[key_mac_low_16])
        value_timeout = int(values[key_timeout])
        output[key_ip] = (idx.inc(), "IP Address", format_ip(value_ip))
        output[key_mac] = (idx.inc(), "MAC Address", format_mac((value_mac_high_32, value_mac_low_16)))
        output[key_timeout] = (idx.inc(), "Timeout",
                               f"{values[key_timeout]:d} seconds")
        # output[key_mac_high_32] = (idx.inc(), "MAC Address High 32", value_mac_high)
        # output[key_mac_low_16] = (idx.inc(), "MAC Address Low 16", value_mac_low)

    return output


def formatDataPort(values, index):
    if not values:
        return None
    values = Status.StatusDict(values, prefix=f"DataPort{index}_")
    idx = Counter()
    output = {'TotalDataPacketsSent': (idx.inc(), "Total Data Packets Sent",
                                       fancy_integer(int(
                                           values['TotalDataPacketsSent']))),
              'TotalPacketsResent': (idx.inc(), "Total Packets Resent",
                                     fancy_integer(
                                         int(values['TotalPacketsResent']))),
              'TotalFillPacketsSent': (idx.inc(), "Total Fill Packets Sent",
                                       fancy_integer(int(
                                           values['TotalFillPacketsSent']))),
              'ReceiveSequenceErrors': (idx.inc(), "Receive Sequence Errors",
                                        fancy_integer(int(
                                            values['ReceiveSequenceErrors']))),
              'BytesOfPacketCurrentlyUsed': (
              idx.inc(), "Bytes of Packet Buffer Currently Used",
              values['BytesOfPacketCurrentlyUsed']),
              'TimeOfLastDataPacketAcked': (
              idx.inc(), "Time of Last Data Packet Acknowledged",
              values['TimeOfLastDataPacketAcked'])}

    value = values['PhysicalInterfaceNumber']
    if_str = "Unrecognized"
    if value in range(0, 3):
        if_str = f"Serial Port {value + 1:d}"
    elif value == 3:
        if_str = "Ethernet Port"
    result_str = "%d (%s)" % (value, if_str)
    output['PhysicalInterfaceNumber'] = (idx.inc(), "Physical Interface Number", result_str)
    output['DataPortNumber'] = (idx.inc(), "Data Port Number", values['DataPortNumber'] + 1)
    output['RetransmissionTimeout'] = (idx.inc(), "Retransmission Timeout",
                                       f"{values['RetransmissionTimeout'] * 10:d} milliseconds")
    value = values['DataPortFlags']
    result_str = ""
    if value & 0x0001:
        result_str += "Baler should disconnect and prepare to reconnect to a data vacuum. "
    if value & 0x0002:
        result_str += "Baler should stay powered on. "
    if value & 0x0004:
        result_str += "Baler should shut down immediately. "
    if value & 0x8000:
        result_str += "Packet memory reduced due to bad packet memory RAM. "
    output['DataPortFlags'] = (idx.inc(), "Data Port Flags", values['DataPortFlags'])

    return output


def formatSerial(values, index):
    if not values:
        return None
    values = Status.StatusDict(values, prefix=f"Serial{index}_")
    idx = Counter()
    output = {'ReceiveChecksumErrors': (
    idx.inc(), "Receive Checksum Errors", values['ReceiveChecksumErrors']),
              'TotalIOErrors': (
              idx.inc(), "Total I/O Errors", values['TotalIOErrors']),
              'PhysicalInterfaceNumber': (
              idx.inc(), "Physical Interface Number",
              values['PhysicalInterfaceNumber']),
              'DestinationUnreachableICMPPacketsReceived': (
              idx.inc(), "Destination Unreachable ICMP Packets Received",
              values['DestinationUnreachableICMPPacketsReceived']),
              'SourceQuenchICMPPacketsReceived': (
              idx.inc(), "Source Quench ICMP Packets Received",
              values['SourceQuenchICMPPacketsReceived']),
              'EchoRequestICMPPacketsReceived': (
              idx.inc(), "Echo Request ICMP Packets Received",
              values['EchoRequestICMPPacketsReceived']),
              'RedirectICMPPacketsReceived': (
              idx.inc(), "Redirect ICMP Packets Received",
              values['RedirectICMPPacketsReceived']), 'CharacterOverruns': (
        idx.inc(), "Character Overruns", values['CharacterOverruns']),
              'FramingErrors': (
              idx.inc(), "Framing Errors", values['FramingErrors'])}

    # output['Spare'] = (idx.inc(), "Spare", values['Spare'])

    return output


def formatEthernet(values):
    if not values:
        return None
    values = Status.StatusDict(values, prefix="Ethernet_")
    idx = Counter()
    output = {'ReceiveChecksumErrors': (
    idx.inc(), "Receive Checksum Errors", values['ReceiveChecksumErrors']),
              'TotalIOErrors': (
              idx.inc(), "Total I/O Errors", values['TotalIOErrors']),
              'PhysicalInterfaceNumber': (
              idx.inc(), "Physical Interface Number",
              values['PhysicalInterfaceNumber']),
              'DestinationUnreachableICMPPacketsReceived': (
              idx.inc(), "Destination Unreachable ICMP Packets Received",
              values['DestinationUnreachableICMPPacketsReceived']),
              'SourceQuenchICMPPacketsReceived': (
              idx.inc(), "Source Quench ICMP Packets Received",
              values['SourceQuenchICMPPacketsReceived']),
              'EchoRequestICMPPacketsReceived': (
              idx.inc(), "Echo Request ICMP Packets Received",
              values['EchoRequestICMPPacketsReceived']),
              'RedirectICMPPacketsReceived': (
              idx.inc(), "Redirect ICMP Packets Received",
              values['RedirectICMPPacketsReceived']),
              'RuntFrames': (idx.inc(), "Runt Frames", values['RuntFrames']),
              'CRCErrors': (idx.inc(), "CRC Errors", values['CRCErrors']),
              'BroadcastFrames': (
              idx.inc(), "Broadcast Frames", values['BroadcastFrames']),
              'UnicastFrames': (
              idx.inc(), "Unicast Frames", values['UnicastFrames']),
              'TotalGoodFrames': (
              idx.inc(), "Total Good Frames", values['TotalGoodFrames']),
              'JabberErrors': (
              idx.inc(), "Jabber Errors", values['JabberErrors']),
              'OutOfWindow': (
              idx.inc(), "Out of Window", values['OutOfWindow']),
              'TransmittedOK': (
              idx.inc(), "Transmitted OK", values['TransmittedOK']),
              'ReceivePacketsMissed': (idx.inc(), "Received Packets Missed",
                                       values['ReceivePacketsMissed']),
              'TransmitCollisions': (
              idx.inc(), "Transmit Collisions", values['TransmitCollisions'])}

    # output['Spare'] = (idx.inc(), "Spare", values['Spare'])
    value = values['CurrentLinkStatus']
    result_str = ""
    if value & 0x8000:
        result_str += "Link [OK] | "
    else:
        result_str += "Link [BAD] | "
    if value & 0x0010:
        result_str += "Polarity [OK]"
    else:
        result_str += "Polarity [BAD]"
    output['CurrentLinkStatus'] = (idx.inc(), "Current Link Status", result_str)
    # output['Spare2'] = ("Spare", values['Spare2'])
    # output['Spare3'] = ("Spare", values['Spare3'])

    return output


def formatBaler(values):
    if not values:
        return None
    values = Status.StatusDict(values, prefix="Baler_")
    idx = Counter()
    output = {}

    interface_names = ["Unknown", "Serial 1", "Serial 2", "Special", "Ethernet"]

    for i in [1, 2, 4]:
        interface_name = interface_names[i]
        key_time = f"Baler{i:d}LastTime"
        key_cycles = f"Baler{i:d}TotalNumberOfCycles"
        key_status = f"Baler{i:d}StatusAndTimeouts"
        key_minutes = f"Baler{i:d}OnOffMinutes"

        key_baler_timeouts = f"Baler{i:d}Timeouts"
        key_baler_power = f"Baler{i:d}PowerStatus"
        key_dialer_timeouts = f"Dialer{i:d}Timeouts"
        key_dialer_phase = f"Dialer{i:d}Phase"
        key_dialer_purpose = f"Dialer{i:d}Purpose"

        key_interface_type = f"Interface{i:d}Type"
        key_special_connected = f"Baler{i:d}Connected"
        key_special_foreign = f"Baler{i:d}Foreign"

        maps_special_connected = [0x00, 0x01, 0x02, 0x00, 0x08]
        maps_special_foreign = [0x00, 0x10, 0x20, 0x00, 0x80]

        key_special_bitmap = "Baler3StatusAndTimeouts"

        output[key_time] = (idx.inc(), "Last Time", values[key_time])
        output[key_cycles] = (idx.inc(), "Total Number of Cycles", values[key_cycles])
        # output[key_status] = (idx.inc(), "Baler/Dial Status and Timeouts", values[key_status])
        output[key_minutes] = (idx.inc(), "Baler/Dial On/Off Minutes", values[key_minutes])

        value = values[key_special_bitmap]
        result_str = "No"
        if (value & 0x00ff) == maps_special_connected[i]:
            result_str = "Yes"
        output[key_special_connected] = (idx.inc(), "Baler Connected", result_str)
        result_str = "No"
        if (value & 0x00ff) == maps_special_foreign[i]:
            result_str = "Yes"
        output[key_special_foreign] = (idx.inc(),
                                       f"Q330 {interface_name} will Accept Foreign Baler", result_str)

        value = values[key_status]
        if value & 0x2000:
            output[key_interface_type] = (idx.inc(), "Interface Type", "Baler")
            output[key_baler_timeouts] = (idx.inc(), "Baler Timeout Count", (value & 0x07ff))
            result_str = ""
            if (value & 0xc000) == 0:
                result_str = "Power is Off"
                if value & 0x1000:
                    result_str += " (forced off for 'minimum off time')"
            elif (value & 0xc000) == 1:
                result_str = "Power turned on by command"
            elif (value & 0xc000) == 2:
                result_str = "Power turned on automatically"
            elif (value & 0xc000) == 3:
                result_str = "Power is continuous"
            output[key_baler_power] = (idx.inc(), "Baler Power Status", value)
        elif value & 0x8000:
            output[key_interface_type] = (idx.inc(), "Interface Type", "Dialer")
            output[key_dialer_timeouts] = (idx.inc(), "Dialer Timeout Count", (value & 0x001f))
            result_str = ""
            if (value & 0x03e0) == 0:
                result_str = "Idle"
            elif (value & 0x03e0) == 1:
                result_str = "Hanging Up"
            elif (value & 0x03e0) == 2:
                result_str = "Waiting for Hang Up"
            elif (value & 0x03e0) == 3:
                result_str = "On Hook"
            elif (value & 0x03e0) == 4:
                result_str = "Off Hook"
            elif (value & 0x03e0) == 5:
                result_str = "Waiting for Attention"
            elif (value & 0x03e0) == 6:
                result_str = "Initializing Modem"
            elif (value & 0x03e0) == 7:
                result_str = "Dialing Number"
            elif (value & 0x03e0) == 8:
                result_str = "Hang Up Requested"
            elif (value & 0x03e0) == 9:
                result_str = "Carrier Wait"
            elif (value & 0x03e0) == 10:
                result_str = "LCP Negotiation"
            elif (value & 0x03e0) == 11:
                result_str = "Loggin In"
            elif (value & 0x03e0) == 12:
                result_str = "NCP Negotiation"
            elif (value & 0x03e0) == 13:
                result_str = "Link Open"
            elif (value & 0x03e0) == 14:
                result_str = "Modem Error"
            elif (value & 0x03e0) == 15:
                result_str = "LCP Error"
            elif (value & 0x03e0) == 16:
                result_str = "Authentication Error"
            elif (value & 0x03e0) == 17:
                result_str = "NCP Error"
            output[key_dialer_phase] = (idx.inc(), "Dialer Phase", result_str)
            result_str = ""
            if (value & 0x1800) == 0:
                result_str = "Idle"
            elif (value & 0x1800) == 1:
                result_str = "Answering Incoming Call"
            elif (value & 0x1800) == 2:
                result_str = "Manual Originate"
            elif (value & 0x1800) == 3:
                result_str = "Automatic Originate"
            output[key_dialer_purpose] = (idx.inc(), "Dialer Purpose for Going Online", result_str)
        else:
            output[key_interface_type] = (idx.inc(), "Interface Type", "Telemetry")

    return output


def formatDynamicIP(values):
    if not values:
        return None
    values = Status.StatusDict(values, prefix="DynamicIP_")
    idx = Counter()
    output = {}

    value_ip = int(values['Serial1IPAddress'])
    output['Serial1IPAddress'] = (idx.inc(), "Serial 1 IP Address", format_ip(value_ip))

    value_ip = int(values['Serial2IPAddress'])
    output['Serial2IPAddress'] = (idx.inc(), "Serial 2 IP Address", format_ip(value_ip))
    output['Reserved'] = (idx.inc(), "Reserved", "N/A")

    value_ip = int(values['EthernetIPAddress'])
    output['EthernetIPAddress'] = (idx.inc(), "Ethernet IP Address", format_ip(value_ip))

    return output


def formatAuxiliaryBoard(values):
    if not values:
        return None
    values = Status.StatusDict(values, prefix="AuxiliaryBoard_")
    idx = Counter()
    output = {}

    num_entries = int(values['SizeOfThisBlock'] / 4)  # Size of conversion map
    output['NumberOfEntries'] = (idx.inc(), "Number of Entries", num_entries)
    output['SizeOfThisBlock'] = (idx.inc(), "Length of this Block", "%d bytes" % values['SizeOfThisBlock'])

    version = values['PacketVersion']
    version_major = version >> 8
    version_minor = version & 0x00ff
    output['PacketVersion'] = (idx.inc(), "Packet Version", "%d.%d" % (version_major, version_minor))

    result_str = "Unknown"
    if values['AuxType'] == 32:
        result_str = "AuxAD"
    output['AuxType'] = (idx.inc(), "Auxiliary Board Type", result_str)

    version = values['AuxVersion']
    version_major = version >> 8
    version_minor = version & 0x00ff
    output['AuxVersion'] = (idx.inc(), "Auxiliary Version", "%d.%d" % (version_major, version_minor))
    for i in range(0, num_entries):
        key_conversion = "Conversion_%d" % i
        if key_conversion in values:
            output[key_conversion] = (idx.inc(), "Conversion %d" % i, values[key_conversion])

    return output


def formatSerialSensor(values):
    if not values:
        return None
    values = Status.StatusDict(values, prefix="SerialSensor_")
    idx = Counter()
    output = {}

    num_entries = values['NumberOfSubBlocks']
    output['TotalSizeOfThisBlock'] = (idx.inc(), "Total Size of this Block", "%d bytes" % values['TotalSizeOfThisBlock'])
    output['NumberOfSubBlocks'] = (idx.inc(), "Number of Sub-Blocks", num_entries)

    for i in range(0, num_entries):
        key_size_of_this_sub_block = "SizeOfThisSubBLock_%d" % i
        key_sensor_type = "SensorType_%d" % i
        key_serial_interface = "SerialInterface_%d" % i
        key_seconds_per_sample = "SecondsPerSample_%d" % i
        key_units = "Units_%d" % i
        key_integration_time = "IntegrationTime_%d" % i
        key_fractional_digits = "FractionalDigits_%d" % i
        key_valid_fields = "ValidFields_%d" % i
        key_pressure_measurement = "PressureMeasurement_%d" % i
        key_internal_temperature_measurement = "InternalTemperatureMeasurement_%d" % i
        key_humidity_measurement = "HumidityMeasurement_%d" % i
        key_external_temperature_measurement = "ExternalTemperatureMeasurement_%d" % i

        if key_size_of_this_sub_block in values:
            output[key_size_of_this_sub_block] = (idx.inc(), "Size of this Sub-Block", "%d bytes" % values[key_size_of_this_sub_block])

        if key_sensor_type in values:
            sensor_type = values[key_sensor_type]
            result_str = "Unknown"
            if sensor_type == 1:
                result_str = "Paroscientific RS-232 Pressure Transducer"
            output[key_sensor_type] = (idx.inc(), "Sensor Type", result_str)
        if key_serial_interface in values:
            output[key_serial_interface] = (idx.inc(), "Sensor Interface", values[key_serial_interface])
        if key_seconds_per_sample in values:
            output[key_seconds_per_sample] = (idx.inc(), "Seconds per Sample", values[key_seconds_per_sample])

        unit_string = ""
        if key_units in values:
            units = values[key_units]
            unit_string = "Unknown"
            if units == 1:
                unit_string = "psi"
            elif units == 2:
                unit_string = "hPa"
            elif units == 3:
                unit_string = "bar"
            elif units == 4:
                unit_string = "kPa"
            elif units == 5:
                unit_string = "MPa"
            elif units == 6:
                unit_string = "in Hg"
            elif units == 7:
                unit_string = "mm Hg"
            elif units == 8:
                unit_string = "m H2O"
            output[key_units] = (idx.inc(), "Units", unit_string)
        if key_integration_time in values:
            output[key_integration_time] = (idx.inc(), "Integration Time", "%d ms" % values[key_integration_time])
        if key_fractional_digits in values:
            output[key_fractional_digits] = (idx.inc(), "Fractional Digits", values[key_fractional_digits])
        if key_valid_fields in values:
            value = values[key_valid_fields]
            result_str = ""
            if value & 0x1:
                result_str += "Pressure"
            if (value & 0x2) and not (value & 0xC):
                if len(result_str):
                    result_str += ", "
                result_str += "Internal Temperature"
            if value & 0x4:
                if len(result_str):
                    result_str += ", "
                result_str += "Humidity"
            if value & 0x8:
                if len(result_str):
                    result_str += ", "
                result_str += "External Temperature"
            if not len(result_str):
                result_str = "None"
            output[key_valid_fields] = (idx.inc(), "Valid Fields", result_str)
        if key_pressure_measurement in values:
            fractional_digits = values[key_fractional_digits]
            format_string = "%%01.%df %s" % (fractional_digits, unit_string)
            output[key_pressure_measurement] = (idx.inc(), "Pressure", format_string % (values[key_pressure_measurement] / (10 ** fractional_digits)))
        if key_internal_temperature_measurement in values:
            fractional_digits = float(values[key_fractional_digits])
            format_string = "%%01.%df C" % fractional_digits
            output[key_internal_temperature_measurement] = (idx.inc(), "Internal Temperature", format_string % (float(values[key_internal_temperature_measurement]) / (10 ** fractional_digits),))
        if key_humidity_measurement in values:
            output[key_humidity_measurement] = (idx.inc(), "Humidity", "%01.1f%%" % (values[key_humidity_measurement] * 0.1))
        if key_external_temperature_measurement in values:
            output[key_external_temperature_measurement] = (idx.inc(), "External Temperature", "%01.2f C" % (values[key_external_temperature_measurement] * 0.01))

    return output


def formatEnvironmentalProcessor(values):
    if not values:
        return None
    values = Status.StatusDict(values, prefix="EnvironmentalProcessor_")
    idx = Counter()
    output = {
        'InitialVCO': (idx.inc(), "Initial VCO", "%01f" % values['InitialVCO']),
        'TimeError': (idx.inc(), "Time Error", "%01f" % values['TimeError']),
        'BestVCO': (idx.inc(), "Best VCO", "%01f" % values['BestVCO']),
        'TicksSinceLastUpdate': (idx.inc(), "Ticks Since Last Update",
                                 "%d" % values['TicksSinceLastUpdate']),
        'Km': (idx.inc(), "Km", "%d" % values['Km'])}

    # General EP Status

    value = values['State']
    result_str = ""
    if not value:
        result_str = 'PLL not enabled'
    elif value == 0x40:
        result_str = 'Hold'
    elif value == 0x80:
        result_str = 'Tracking'
    elif value == 0xC0:
        result_str = 'Locked'
    else:
        result_str = 'Unrecognized'
    output['State'] = (idx.inc(), "State", result_str)

    value = values['EPSerialNumberHigh32'] << 32
    value |= values['EPSerialNumberLow32']
    output['EPSerialNumber'] = (idx.inc(), "EP Serial Number", "0x%016x" % value)

    value = values['ProcessorID']
    result_str = "ArchRev=%d" % ((value >> 10) & 0x07,)
    result_str += " ArchType=%d" % ((value >> 13) & 0x07,)
    result_str += " ProcRev=%d" % ((value >> 16) & 0x0f,)
    result_str += " ProcType=%d" % ((value >> 24) & 0xff,)
    result_str += " [0x%08x]" % value
    output['ProcessorID'] = (idx.inc(), "Processor ID", result_str)
    output['SecondsSinceBoot'] = (idx.inc(), "Seconds Since Boot", "%d" % values['SecondsSinceBoot'])
    output['SecondsSinceReSync'] = (idx.inc(), "Seconds Since Re-Sync", "%d" % values['SecondsSinceReSync'])
    output['NumberOfReSyncs'] = (idx.inc(), "Number of Re-Syncs", "%d" % values['NumberOfReSyncs'])
    output['Q330CommunicationsErrors'] = (idx.inc(), "Q330 Communications Errors", "%d" % values['Q330CommunicationsErrors'])
    output['EPCommunicationsErrors'] = (idx.inc(), "EP Communications Errors", "%d" % values['EPCommunicationsErrors'])
    output['SDIDevicesActive'] = (idx.inc(), "SDI Devices Active", "%d" % values['SDIDevicesActive'])

    value = values['FirmwareVersion']
    output['FirmwareVersion'] = (idx.inc(), "Firmware Version", "%d.%d" % ((value >> 8) & 0xff, value & 0xff))

    value = values['Flags']
    result_str = "[0x%08x]" % value
    if value & 0x01:
        result_str += " Flash Manufacturing Constants are Invalid."
    if value & 0x02:
        result_str += " Aux. I/O Line 1 Active."
    if value & 0x04:
        result_str += " Aux. I/O Line 2 Active."
    output['Flags'] = (idx.inc(), "Flags", result_str)
    output['AnalogChannels'] = (idx.inc(), "Analog Channels", "%d" % values['AnalogChannels'])

    value = values['EPModel']
    result_str = "Unknown"
    if value == 1:
        result_str = "Base1"
    output['EPModel'] = (idx.inc(), "EP Model", result_str)
    output['EPRevision'] = (idx.inc(), "EP Revision", "%d" % values['EPRevision'])

    value = values['Gains']
    result_str = "Ch1=0x%02x" % ((value >> 24) & 0xff,)
    result_str += "Ch2=0x%02x" % ((value >> 16) & 0xff,)
    result_str += "Ch3=0x%02x" % ((value >> 8) & 0xff,)
    result_str += "Ch4=0x%02x" % (value & 0xff,)
    output['Gains'] = (idx.inc(), "Gains", result_str)
    output['InputVoltageToEP'] = (idx.inc(), "Input Voltage to EP", "%0.1f Volts" % (values['InputVoltageToEP'] * 0.1,))
    output['InternalHumidity'] = (idx.inc(), "Internal Humidity", "%d%%" % values['InternalHumidity'])
    output['BuiltInPressure'] = (idx.inc(), "Built-in Pressure", "%d uBar" % values['BuiltInPressure'])
    output['InternalTemperature'] = (idx.inc(), "Internal Temperature", "%0.1f C" % (values['InternalTemperature'] * 0.1,))
    for i in range(1, 4):
        key = "ADInputChannel%dCounts" % i
        output[key] = (idx.inc(), "A/D Input Channel %d Counts" % i, "%d" % values[key])

    # SDI Device Entry Status
    for i in range(0, 4):
        output["SDIAddress_%d" % i] = (idx.inc(), "[SDI %d] SDI Address" % i, "%d" % values["SDIAddress_%d" % i])

        value = values["Phase_%d" % i]
        result_str = "Unknown"
        if value == 6:
            result_str = "Ready to Run"
        if value == 7:
            result_str = "Sampling"
        if value == 8:
            result_str = "Waiting for Sample Result"
        output["Phase_%d" % i] = (idx.inc(), "[SDI %d] Phase" % i, result_str)

        result_str = "Unknown"
        if value == 1:
            result_str = "Vaisala VXT520 Weather Station"
        value = values["DriverID_%d" % i]
        output["DriverID_%d" % i] = (idx.inc(), "[SDI %d] Driver ID" % i, result_str)
        output["SensorModel_%d" % i] = (idx.inc(), "[SDI %d] Sensor Model" % i, values["SensorModel_%d" % i])
        output["SerialNumber_%d" % i] = (idx.inc(), "[SDI %d] Serial Number (or other)" % i, values["SerialNumber_%d" % i])
        output["SensorVersionNumber_%d" % i] = (idx.inc(), "[SDI %d] Sensor Version Number" % i, values["SensorVersionNumber_%d" % i])

    # ADC Device Entry Status
    value = values['ADCSerialNumberHigh32'] << 32
    value |= values['ADCSerialNumberLow32']
    output['ADCSerialNumber'] = (idx.inc(), "ADC Serial Number", "0x%016x" % value)

    value = values['ADCModel']
    result_str = 'ADC Status Unknown'
    if value == 0:
        result_str = 'No ADC module installed'
    elif value == 1:
        result_str = 'One channel ADC module configured for Setra use'
    elif value == 2:
        result_str = 'Generic one channel ADC module'
    elif value == 3:
        result_str = 'Generic three ADC module'
    output['ADCModel'] = (idx.inc(), "ADC Model", result_str)
    output['ADCRevision'] = (idx.inc(), "ADCRevision", "%d" % values['ADCRevision'])

    return output


def formatCommunications(values):
    if not values:
        return None
    values = Status.StatusDict(values, prefix="Communications_")
    idx = Counter()
    output = {}

    result_str = "%s sec." % (str(values["LastTime"]),)
    output["LastTime"] = (idx.inc(), "Last Comm. Board Cycle", result_str)
    output["TotalNumberOfCycles"] = (idx.inc(), "Total Number of Cycles", values["TotalNumberOfCycles"])

    value = values["StatusAndTimeouts"]
    power_status = (value & 0xC000) >> 14
    result_str = "On %s minutes" % (str(values["OnMinutes"]),)
    if power_status == 1:
        result_str += " (Turned on manually)"
    elif power_status == 2:
        result_str += " (Turned on automatically)"
    elif power_status == 3:
        result_str += " (Continuous)"
    output["Timeouts"] = (idx.inc(), "Timeouts", value & 0x0fff)
    output["PowerStatus"] = (idx.inc(), "Power Status", result_str)
    output["BalerCommunicationsStatus"] = (idx.inc(), "Baler Comm. Status", values["BalerCommunicationsStatus"])
    output["Special"] = (idx.inc(), "Special", values["Special"])
    output["EthernetCommunicationsStatus"] = (idx.inc(), "Ethernet Comm. Status", values["EthernetCommunicationsStatus"])
    for i in range(1, 4):
        output["Data%dHold" % i] = (idx.inc(), "Data %d Hold" % i, "Locked %d hours" % values["Data%dHold" % i])

    return output


def formatFrontEnd(values):
    if not values:
        return None
    values = Status.StatusDict(values, prefix="FrontEnd_")
    idx = Counter()
    output = {}
    numEntries = values["NumberOfEntries"]

    for i in range(0, numEntries):
        board = i + 1
        output[f"InitialVCO_{i:d}"] = (idx.inc(), f"[FE {board:d}] Initial VCO",
                                       f"{values[f'InitialVCO_{i:d}']:0.2f}")
        output[f"TimeError_{i:d}"] = (idx.inc(), f"[FE {board:d}] Time Error",
                                      f"{values[f'TimeError_{i:d}']:0.2f}")
        output[f"BestVCO_{i:d}"] = (idx.inc(), f"[FE {board:d}] Best VCO",
                                    f"{values[f'BestVCO_{i:d}']:0.2f}")
        output[f"TicksSinceLastUpdate_{i:d}"] = \
            (idx.inc(), f"[FE {board:d}] Ticks Since Last Update", values[
                f"TicksSinceLastUpdate_{i:d}"])
        output[f"Km_{i:d}"] = (idx.inc(), f"[FE {board:d}] Km", values[
            f"Km_{i:d}"])

        value = values[f"State_{i:d}"]
        result_str = "Unknown"
        if value == 0x00:
            result_str = "PLL not enabled"
        elif value == 0x40:
            result_str = "Hold"
        elif value == 0x80:
            result_str = "Tracking"
        elif value == 0xC0:
            result_str = "Locked"
        output["State_%d" % i] = (idx.inc(), "[FE %d] State" % board, result_str)
        flags = values["Flags_%d" % i]

        output["SecondsSinceReSync_%d" % i] = (idx.inc(), "[FE %d] Seconds Since Re-Sync" % board, values["SecondsSinceReSync_%d" % i])
        output["NumberOfReSyncs_%d" % i] = (idx.inc(), "[FE %d] Number of Re-Syncs" % board, values["NumberOfReSyncs_%d" % i])
        output["SecondsSinceBoot_%d" % i] = (idx.inc(), "[FE %d] Seconds Since Boot" % board, values["SecondsSinceBoot_%d" % i])
        output["CPCommunicationsErrors_%d" % i] = (idx.inc(), "[FE %d] CP Comm. Errors" % board, values["CPCommunicationsErrors_%d" % i])
        output["InputVoltageToFE_%d" % i] = (idx.inc(), "[FE %d] Input Voltage" % board, "%0.1f" % (values["InputVoltageToFE_%d" % i] * 0.1,))
        output["SensorControlBitmap_%d" % i] = (idx.inc(), "[FE %d] Sensor Control Bitmap" % board, values["SensorControlBitmap_%d" % i])

        gc_status = values["GainAndCalibratorStatus_%d" % i]
        result_str = "Calibrator "
        if gc_status & 0x0001:
            result_str += "Enabled"
        else:
            result_str += "Disabled"
        result_str = "; Signal "
        if gc_status & 0x0002:
            result_str += "On"
        else:
            result_str += "Off"
        if gc_status & 0x0004:
            result_str += " (Signal gen. failure)"
        output["CalibratorStatus_%d" % i] = (idx.inc(), "[FE %d] Calibrator Status" % board,)
        for j in range(1, 4):
            result_str = ""
            gain_status = (gc_status >> (8 + ((j - 1) * 2))) & 0x0003
            if gain_status == 0:
                result_str = "Cfg. for High Volt input"
            elif gain_status == 1:
                result_str = "Cfg. for Low Volt input, test if high possible"
            elif gain_status == 2:
                result_str = "Cfg. for Low Volt input"
            elif gain_status == 3:
                result_str = "Cfg. for Low Volt input, but set high due to range"
            output["Channel%dGain_%d" % (j, i)] = (idx.inc(), "[FE %d] Channel %d Gain" % (i, j), result_str)

        result_str = "%d" % values["SensorTemperature_%d" % i]
        if (flags & 0x0001) == 0:
            result_str += " (Invalid)"
        else:
            result_str += " (Valid)"
        output["SensorTemperature_%d" % i] = (idx.inc(), "[FE %d] Sensor Temperature" % board, result_str)

        result_str = "%016x" % values["SensorSerialNumber_%d" % i]
        if (flags & 0x0001) == 0:
            result_str += " (Invalid)"
        else:
            result_str += " (Valid)"
        output["SensorSerialNumber_%d" % i] = (idx.inc(), "[FE %d] Sensor Serial Number" % board, result_str)

        for j in range(1, 4):
            output["Channel%dBoom_%d" % (j, i)] = (idx.inc(), "[FE %d] Channel %d Boom" % (i, j), values["Channel%dBoom_%d" % (j, i)])

    return output


def formatPingStatus(values):
    if not values:
        return None
    idx = Counter()
    output = {'Type': (idx.inc(), "Ping Type", values['Type']),
              'UserMessageNumber': (
              idx.inc(), "User Message Number", values['UserMessageNumber']),
              'UserMessageCount': (
              idx.inc(), "User Message Count", values['UserMessageCount']),
              'DriftTolerance': (
              idx.inc(), "Drift Tolerance", values['DriftTolerance'])}

    time_reboot = q330_to_unix_time(values['TimeOfLastReboot'])
    time_now = float(calendar.timegm(time.gmtime()))
    uptime = (time_now - time_reboot) / 86400.0
    time_string = "%s UTC" % time.strftime("%Y/%m/%d %H:%M", time.gmtime(time_reboot))
    output['TimeOfLastReboot'] = (idx.inc(), "Boot Time", time_string)
    output['Uptime'] = (idx.inc(), "Uptime", "%.02f days" % uptime)
    # output['Spare1'] = (idx.inc(), "Spare 1", values['Spare1'])
    # output['Spare2'] = (idx.inc(), "Spare 2", values['Spare2'])
    output['StatusBitmap'] = (idx.inc(), "Status Bitmap", values['StatusBitmap'])

    return output


def formatPingInfo(values):
    if not values:
        return None
    idx = Counter()
    output = {'Type': (idx.inc(), "Type", values['Type']),
              'Version': (idx.inc(), "Version", values['Version']),
              'Flags': (idx.inc(), "Flags", "0x%08lx" % values['Flags']),
              'KMIPropertyTag': (
              idx.inc(), "Tag ID", "%d" % values['KMIPropertyTag'])}

    # output['Ignore'] = (idx.inc(), "Ignored", values['Ignore'])

    data_string = str(values['SerialNumber'])
    if values['SerialNumber'] == 0:
        data_string = "Locked"
    else:
        data_string = "%016x" % values['SerialNumber']
    output['SerialNumber'] = (idx.inc(), "Serial Number", "%s" % data_string)

    for i in range(1, 5):
        key = "DataPort%dPacketMemorySize" % i
        buffer_bytes = values[key]
        buffer_size = buffer_bytes / 1048576.0
        result_str = "%0.2f MiB" % buffer_size
        output[key] = (idx.inc(), "Data Port %d Packet Memory Size" % i, result_str)

    key_maker = lambda p, s1, i, s2: f"{p}{s1}{i}{s2}"
    for i in range(1, 5):
        if i != 3:
            prefix = 'Serial'
            infix = ''
            infix_str = ''
            if i == 4:
                prefix = 'Ethernet'
            else:
                infix = "%d" % i
                infix_str = " %d" % i

            key = key_maker(prefix, "Interface", infix, "MemoryTriggerLevel")
            buffer_bytes = values[key]
            buffer_size = buffer_bytes / 1048576.0
            result_str = "%0.2f MiB" % buffer_size
            output[key] = (idx.inc(), "Memory Trigger Level for %s%s" % (prefix, infix_str), result_str)

            key = key_maker(prefix, "Interface", infix, "AdvancedFlags")
            output[key] = (idx.inc(), "Adavanced Flags for %s%s" % (prefix, infix_str), values[key])

            key = key_maker(prefix, "Interface", infix, "DataPortNumber")
            output[key] = (idx.inc(), "Data Port Number for %s%s" % (prefix, infix_str), values[key] + 1)
        else:
            for j in range(1, 4):
                key = "Reserved%d" % j
                # output[key] = (idx.inc(), "Reserved %d" % j, values[key])

    result_str = ""
    bitmap = values['CalibrationErrorBitmap']
    for i in range(0, 16):
        if bitmap & (1 << i):
            result_str = ", Channel %d " % i
    if not len(result_str):
        result_str = "None"
    else:
        result_str = result_str[2:]
    output['CalibrationErrorBitmap'] = (idx.inc(), "Calibration Errors", result_str)

    version = values['SystemSoftwareVersion']
    version_major = version >> 8
    version_minor = version & 0x00ff
    output['SystemSoftwareVersion'] = (idx.inc(), "Firmware Version", "%d.%d" % (version_major, version_minor))

    return output


def formatted(input_dict):
    formatted_dict = {}
    for key, value in input_dict.items():
        result = None
        try:
            if key == 'Global':
                result = formatGlobal(value)
            elif key == 'Gps':
                result = formatGps(value)
            elif key == 'PowerSupply':
                result = formatPowerSupply(value)
            elif key == 'BoomPosition':
                result = formatBoomPosition(value)
            elif key == 'Thread':
                result = formatThread(value)
            elif key == 'PLL':
                result = formatPLL(value)
            elif key == 'Satellites':
                result = formatSatellites(value)
            elif key == 'ARP':
                result = formatARP(value)
            elif key[0:8] == 'DataPort':
                result = formatDataPort(value, int(key[8:9]))
            elif key == 'SerialSensor':
                result = formatSerialSensor(value)
            elif key == 'EnvironmentalProcessor':
                result = formatEnvironmentalProcessor(value)
            elif key[0:6] == 'Serial':
                result = formatSerial(value, int(key[6:7]))
            elif key == 'Ethernet':
                result = formatEthernet(value)
            elif key == 'Baler':
                result = formatBaler(value)
            elif key == 'DynamicIP':
                result = formatDynamicIP(value)
            elif key == 'AuxiliaryBoard':
                result = formatAuxiliaryBoard(value)
            elif key == 'Communications':
                result = formatCommunications(value)
            elif key == 'FrontEnd':
                result = formatFrontEnd(value)
            elif key == 'SenderMessage':
                logging.error("SenderMessage formatting not implemented")
                # result = formatSenderMessage(value)
            elif key == 'ping-status':
                result = formatPingStatus(value)
            elif key == 'ping-info':
                result = formatPingInfo(value)
            if result:
                formatted_dict[key] = result
            else:
                formatted_dict[key] = {'MESSAGE': (0, 'ERROR', 'Could not retrieve results')}
        except KeyError as e:
            formatted_dict[key] = {'MESSAGE': (0, 'ERROR', 'Could not retrieve results')}

    return formatted_dict
