#!/usr/bin/python

import optparse
import os
import queue
import re
import sys
import time
import subprocess

from Quasar import QDPDevice
from QTools import QExMessage, q330_to_unix_time, assert_arg_count, expand_int_list, QExArgs
from QComm import QComm
from QScript import QScript
from QCal import QCal, CouplingMap
from QCalStop import QCalStop
from QPing import QPing
from QLock import QLock, QExLocked
from log import Logger
from E300 import E300_ACCELERATION, E300_RAW, E300_VELOCITY, E300Thread
from ChannelControl import ChannelControl
from ChannelServer import ChannelServer
from Channel import ChannelThread


def T(expression, if_true, if_false):
    if expression:
        return if_true
    return if_false


def expand_sensor_list(list_str):
    regex_sensor_list = re.compile("^([AB])(?:[,]([AB]))*$")
    matches = regex_sensor_list.findall(list_str)
    if matches is None:
        raise ValueError("")
    return list(list_str.split(','))


def parse_cals(filename, log_function=None):
    cals = []
    server = None
    controls = {}
    autocal_number = 0
    channel_number = 0
    if not os.path.exists(filename):
        raise QExMessage("Autocal config file '%s' does not exist" % filename)

    fh = open(filename, 'r')
    line_in = 'GO'
    while len(line_in):
        line_in = fh.readline()
        line = line_in.strip()
        if not len(line):
            continue
        if line[0] == '#':
            continue
        args = line.split('#', 1)[0].split()
        if not len(args):
            continue

        # CHANNEL.SERVER <SERVER> <PORT>
        # SERVER = IP address of the archive server
        # PORT   = TCP port of the archive server
        if args[0].strip() == "CHANNEL.SERVER":
            if server is not None:
                raise QExMessage("cal config contains multiple CHANNEL.SERVER entries")
            if len(args) < 3:
                raise QExMessage("CHANNEL.CONTROL > too few arguments")
            try:
                server = ChannelServer()
                server.ip = args[1]
                server.port = args[2]
            except TypeError as e:
                raise QExMessage("CHANNEL.CONTROL > invalid type for %s" % str(e))
            except ValueError as e:
                raise QExMessage("CHANNEL.CONTROL > invalid value for %s" % str(e))

            continue  # move on to the next line

        # CHANNEL.CONTROL <GROUP_ID> [STATION]-[LOCATION]-<CHANNEL> <CONTEXT> <STATE>
        # GROUP_ID = Unique identifier for this group
        # CONTEXT  = ARCHIVE | IDA
        # STATE    = DEFAULT | OFF | ON
        if args[0].strip() == "CHANNEL.CONTROL":
            channel_number += 1
            if len(args) < 5:
                raise QExMessage("CHANNEL.CONTROL line # %d > too few arguments" % channel_number)
            try:
                channel_control = ChannelControl(args[1])
                try:
                    s, l, c = args[2].split('-', 2)
                except:
                    raise ValueError("channel-identifier")
                channel_control.station = s
                channel_control.location = l
                channel_control.channel = c
                channel_control.context = args[3]
                channel_control.state = args[4]
                if channel_control.group in controls:
                    controls[channel_control.group].append(channel_control)
                else:
                    controls[channel_control.group] = [channel_control]
            except TypeError as e:
                raise QExMessage("CHANNEL.CONTROL line # %d > invalid type for %s" % (channel_number, str(e)))
            except ValueError as e:
                raise QExMessage("CHANNEL.CONTROL line # %d > invalid value for %s" % (channel_number, str(e)))

            continue  # move on to the next line

        autocal_number += 1

        if len(args) < 10:
            raise QExMessage("cal # %d > too few arguments for calibration" % autocal_number)
        if args[9].lower() == 'random' and len(args) < 11:
            raise QExMessage("cal # %d > too few arguments for calibration" % autocal_number)

        try:
            cal_info = {'sensor_type': 'Unspecified',
                        'device': validate_device_id(args[0]),
                        'sensor': args[1], 'index': autocal_number}

            cal_object = QCal(args[8].lower())
            cal_object.set_log_function(log_function)
            cal_object.coupling = 'R'
            cal_object.channels = args[1]
            cal_object.cal_monitor = args[2]
            cal_object.amplitude = args[3]
            cal_object.settle = args[4].lower()
            cal_object.duration = args[5].lower()
            cal_object.trailer = args[6].lower()
            cal_object.sleep = args[7].lower()
            if cal_object.wave_form == 'sine':
                cal_object.frequency = args[9]
                max_arg = 10
            elif cal_object.wave_form == 'step':
                cal_object.polarity = args[9].lower()
                max_arg = 10
            elif cal_object.wave_form == 'random':
                cal_object.frequency = args[9]
                cal_object.noise = args[10]
                max_arg = 11
            # TODO: Add for Q335?
            # elif cal_object.wave_form == 'timing': 
            #    cal_object.???? = args[9]
            #    cal_object.???? = args[10]
            # NOTE: The Timing Test cal requires 1PPS from GPS be active 
            cal_object.auto_timing = True
            cal_object.e300 = False
            cal_object.e300_input = E300_RAW

            flags = args[max_arg:]
            for flag in flags:
                if flag.upper() == 'NOW':
                    cal_object.auto_timing = False
                elif flag.upper() == 'CAPACITIVE':
                    cal_object.coupling = 'C'
                elif flag.upper() in ('E300', 'E300V', 'E300A'):
                    cal_object.e300 = True
                    if flag.upper() == 'E300V':
                        cal_object.e300_input = E300_VELOCITY
                    if flag.upper() == 'E300A':
                        cal_object.e300_input = E300_ACCELERATION
                elif flag[0:7].upper() == 'SENSOR:':
                    if len(flag) > 7:
                        cal_info['sensor_type'] = flag[7:]
                elif flag[0:6].upper() == 'GROUP:':
                    if len(flag) > 6:
                        cal_object.group = flag[6:].upper()
                else:
                    raise QExMessage("cal # %d > invalid flag '%s'" % (autocal_number, flag))

            # Ensure SEED cal blockette reports that this is an autocal
            cal_object.auto = True
            cal_object.suppress_stdout(True)

        except TypeError as e:
            raise QExMessage("cal # %d > invalid type for %s" % (autocal_number, str(e)))
        except ValueError as e:
            raise QExMessage("cal # %d > invalid value for %s" % (autocal_number, str(e)))

        cal_info['total_duration'] = cal_object.duration + cal_object.settle + cal_object.trailer + cal_object.sleep
        cals.append((cal_object, cal_info))

    if len(list(controls.keys())) and not server:
        raise QExMessage("cal config contains CHANNEL.CONTROL entries but no CHANNEL.SERVER entry")

    for cal, info in cals:
        if cal.group and cal.group not in controls:
            raise QExMessage(
                "cal # %d > references no-existent CHANNEL.CONTROL group '%s'" % (info['index'], cal.group))

    return cals, server, controls


def validate_device_id(device_id):
    try:
        int(device_id)
    except:
        raise TypeError('device')
    return device_id


def format_server(server, view='verbose'):
    ret = ""
    if view == 'verbose':
        ret += "CHANNEL.SERVER\n"
        ret += "  ip:   %s\n" % server.ip
        ret += "  port: %s\n" % server.port
    else:
        ret += "CHANNEL.SERVER %s:%d" % (server.ip, server.port)
    return ret


def format_control(control, view='verbose'):
    ret = ""
    if view == 'verbose':
        ret += "CHANNEL.CONTROL (group '%s')\n" % control.group
        ret += "  station:  %s\n" % control.station
        ret += "  location: %s\n" % control.location
        ret += "  channel:  %s\n" % control.channel
        ret += "  context:  %s\n" % control.context
        ret += "  state:    %s\n" % control.state
    else:
        ret += "CHANNEL.CONTROL[%s] %s-%s-%s %s-%s" % (
        control.group, control.station, control.location, control.channel, control.context, control.state)
    return ret


def format_cal(cal, info, view='verbose'):
    ret = ""
    group_id = str(cal.group)
    channels = "%s" % ",".join(map(str, cal.channels))
    if view == 'verbose':
        ret += "  Calibration #%d (duration %d seconds)\n" % (info['index'], info['total_duration'])
        ret += "    channel control group: %s\n" % group_id
        ret += "    digitizer:   #%s\n" % str(info['device'])
        ret += "    sensor:      %s\n" % info['sensor_type']
        if cal.e300:
            if cal.e300_input == E300_VELOCITY:
                ret += "    E300:        Yes (Velocity)\n"
            elif cal.e300_input == E300_ACCELERATION:
                ret += "    E300:        Yes (Acceleration)\n"
            else:
                ret += "    E300:        Yes (Raw)\n"
        else:
            ret += "    E300:        No\n"
        ret += ""
        ret += "    channel(s):  %s\n" % channels
        ret += "    cal monitor: %s\n" % T(cal.cal_monitor == 0, "None", ",".join(map(str, cal.cal_monitor)))
        ret += "    settle:      %d seconds\n" % cal.settle
        ret += "    duration:    %d seconds\n" % cal.duration
        ret += "    trailer:     %d seconds\n" % cal.trailer
        ret += "    sleep:       %d seconds\n" % cal.sleep
        if cal.auto_timing:
            ret += "    auto-timing: enabled\n"
        else:
            ret += "    auto-timing: disabled\n"
        ret += "    waveform:    %s\n" % cal.wave_form
        if cal.wave_form == "sine":
            if cal.frequency & 0x8000:
                freq_mul = cal.frequency & 0x7fff
                freq_val = 1.0 * freq_mul
                freq_str = "%s Hz (1*%s)" % (("%f" % freq_val).rstrip('0').rstrip('.'), ("%f" % freq_mul).rstrip('0').rstrip('.'))
            else:
                freq_div = cal.frequency
                freq_val = 1.0 / freq_div
                freq_str = "%s Hz (1/%s)" % (("%f" % freq_val).rstrip('0').rstrip('.'), ("%f" % freq_div).rstrip('0').rstrip('.'))
            ret += "    frequency:   %s\n" % freq_str
        elif cal.wave_form == "step":
            ret += "    polarity:    %s\n" % cal.polarity
        elif cal.wave_form == "random":
            if cal.frequency & 0x8000:
                freq_mul = cal.frequency & 0x7fff
                freq_val = 125.0 * freq_mul
                freq_str = "%s Hz (125*%s)" % (("%f" % freq_val).rstrip('0').rstrip('.'), ("%f" % freq_mul).rstrip('0').rstrip('.'))
            else:
                freq_div = cal.frequency
                freq_val = 125.0 / freq_div
                freq_str = "%s Hz (125/%s)" % (("%f" % freq_val).rstrip('0').rstrip('.'), ("%f" % freq_div).rstrip('0').rstrip('.'))
            ret += "    frequency:   %s\n" % freq_str
            ret += "    noise type:  %s\n" % T(cal.noise == 'none', 'random-telegraph', cal.noise)
        ret += "    amplitude:   %d dB\n" % cal.amplitude
        ret += "    coupling:    %s\n" % CouplingMap[cal.coupling].capitalize()
    else:
        special_string = ""
        if cal.wave_form == 'sine':
            if cal.frequency & 0x8000:
                freq_mul = cal.frequency & 0x7fff
                freq_val = 1.0 * freq_mul
            else:
                freq_div = cal.frequency
                freq_val = 1.0 / freq_div
            freq_str = "%s Hz" % ("%f" % freq_val).rstrip('0').rstrip('.')
            special_string = "@ %s" % freq_str
        elif cal.wave_form == 'step':
            special_string = "%s polarity" % cal.polarity
        elif cal.wave_form == 'random':
            noise = cal.noise
            if noise == 'none':
                noise = 'tele.'
            if cal.frequency & 0x8000:
                freq_mul = cal.frequency & 0x7fff
                freq_val = 125.0 * freq_mul
            else:
                freq_div = cal.frequency
                freq_val = 125.0 / freq_div
            freq_str = "%s Hz" % ("%f" % freq_val).rstrip('0').rstrip('.')
            special_string = "(%s) %s" % (noise, freq_str)
        ret += "  Calibration #%d on Q330 #%s channels %s (%s cal %s, %d sec., amp. %d)\n" % (info['index'], info['device'], channels, cal.wave_form, special_string, info['total_duration'], cal.amplitude)
    return ret


def default_print(text):
    print(text)


def print_cal_grid(cals, print_func=default_print, extended=False):
    fields = [
        ('cal_num', 'Cal #', 'default'),
        ('signal', 'Signal', 'default'),
        ('amplitude', 'Amplitude', 'default'),
        ('frequency', 'Frequency', 'default'),
        ('polarity', 'Polarity', 'default'),
        ('coupling', 'Coupling', 'default'),
        ('settle', 'Settle', 'default'),
        ('duration', 'Duration', 'default'),
        ('trailer', 'Trailer', 'default'),
        ('sleep', 'Sleep', 'extended'),
        ('timing', 'Timing', 'extended'),
        ('group', 'Group', 'extended'),
        ('digitizer', 'Digitizer', 'extended'),
        ('channels', 'Channels', 'extended'),
        ('cal_monitor', 'Cal. Monitor', 'extended'),
        ('sensor', 'Sensor', 'extended'),
        ('e300', 'E300', 'extended'),
    ]

    cals_formatted = {}
    col_maxes = {}
    for key, desc, _ in fields:
        col_maxes[key] = len(desc)

    count = 0
    for cal, info in cals:
        cals_formatted[count] = {}
        form = cals_formatted[count]

        cal_num = str(count + 1)
        form['cal_num'] = cal_num
        if col_maxes['cal_num'] < len(cal_num):
            col_maxes['cal_num'] = len(cal_num)

        signal = cal.wave_form
        if signal == 'random':
            if cal.noise == 'none':
                signal = 'random telegraph'
            else:
                signal = 'random %s noise' % cal.noise
        form['signal'] = signal
        if col_maxes['signal'] < len(signal):
            col_maxes['signal'] = len(signal)

        amplitude = "%d dB" % cal.amplitude
        form['amplitude'] = amplitude
        if col_maxes['amplitude'] < len(amplitude):
            col_maxes['amplitude'] = len(amplitude)

        frequency = 'N/A'
        if cal.wave_form == 'sine':
            if cal.frequency & 0x8000:
                freq_mul = cal.frequency & 0x7fff
                freq_val = 1.0 * freq_mul
            else:
                freq_div = cal.frequency
                freq_val = 1.0 / freq_div
            frequency = "%s Hz" % ("%f" % freq_val).rstrip('0').rstrip('.')
        elif cal.wave_form == 'random':
            if cal.frequency & 0x8000:
                freq_mul = cal.frequency & 0x7fff
                freq_val = 125.0 * freq_mul
            else:
                freq_div = cal.frequency
                freq_val = 125.0 / freq_div
            frequency = "%s Hz" % ("%f" % freq_val).rstrip('0').rstrip('.')
        form['frequency'] = frequency
        if col_maxes['frequency'] < len(frequency):
            col_maxes['frequency'] = len(frequency)

        polarity = 'N/A'
        if cal.wave_form == 'step':
            polarity = cal.polarity
        form['polarity'] = polarity
        if col_maxes['polarity'] < len(polarity):
            col_maxes['polarity'] = len(polarity)

        coupling = CouplingMap[cal.coupling].capitalize()
        form['coupling'] = coupling
        if col_maxes['coupling'] < len(coupling):
            col_maxes['coupling'] = len(coupling)

        settle = "%d sec." % cal.settle
        form['settle'] = settle
        if col_maxes['settle'] < len(settle):
            col_maxes['settle'] = len(settle)

        duration = "%d sec." % cal.duration
        form['duration'] = duration
        if col_maxes['duration'] < len(duration):
            col_maxes['duration'] = len(duration)

        trailer = "%d sec." % cal.trailer
        form['trailer'] = trailer
        if col_maxes['trailer'] < len(trailer):
            col_maxes['trailer'] = len(trailer)

        sleep = "%d sec." % cal.sleep
        form['sleep'] = sleep
        if col_maxes['sleep'] < len(sleep):
            col_maxes['sleep'] = len(sleep)

        timing = "Immediate"
        if cal.auto_timing:
            timing = "Scheduled"
        form['timing'] = timing
        if col_maxes['timing'] < len(timing):
            col_maxes['timing'] = len(timing)

        group = str(cal.group)
        form['group'] = group
        if col_maxes['group'] < len(group):
            col_maxes['group'] = len(group)

        digitizer = info['device']
        form['digitizer'] = digitizer
        if col_maxes['digitizer'] < len(digitizer):
            col_maxes['digitizer'] = len(digitizer)

        channels = "%s" % ",".join(map(str, cal.channels))
        form['channels'] = channels
        if col_maxes['channels'] < len(channels):
            col_maxes['channels'] = len(channels)

        cal_monitor = "%s" % T(cal.cal_monitor == 0, "None", ",".join(map(str, cal.cal_monitor)))
        form['cal_monitor'] = cal_monitor
        if col_maxes['cal_monitor'] < len(cal_monitor):
            col_maxes['cal_monitor'] = len(cal_monitor)

        sensor = info['sensor_type']
        form['sensor'] = sensor
        if col_maxes['sensor'] < len(sensor):
            col_maxes['sensor'] = len(sensor)

        e300 = 'No'
        if cal.e300:
            if cal.e300_input == E300_VELOCITY:
                e300 = "Yes (Velocity)"
            elif cal.e300_input == E300_ACCELERATION:
                e300 = "Yes (Acceleration)"
            else:
                e300 = "Yes (Raw)"
        form['e300'] = e300
        if col_maxes['e300'] < len(e300):
            col_maxes['e300'] = len(e300)

        count += 1

    header = ''
    for key, desc, x in fields:
        if (x != 'extended') or extended:
            header += ' %s |' % desc.ljust(col_maxes[key])
    print_func(header.rstrip('|'))
    print_func(''.ljust(len(header), '-'))
    for idx in range(0, count):
        cal_line = ''
        for key, _, x in fields:
            if (x != 'extended') or extended:
                cal_line += ' %s |' % cals_formatted[idx][key].rjust(col_maxes[key])
        print_func(cal_line.rstrip('|'))


wait_period = 60.0  # wait for one minute
max_wait = 10  # wait no more than 10 minutes

start_check_period = 5.0  # wait 5 seconds between checks
max_start_checks = 120  # try no more than 120 times (10 minutes)

stop_check_period = 5.0  # wait 5 seconds between checks
max_stop_checks = 12  # try no more than 12 times (1 minute)


class QSAutocal(QScript):
    def __init__(self):
        self.e300_remind_interval = 1800

        self.option_list = [
            optparse.make_option("-c", "--cals", action="store", dest="cals",
                                 help="limit the operation to these calibrations"),
            optparse.make_option("-d", "--digitizers", action="store", dest="digitizers",
                                 help="limit the operation to these digitizers"),
            optparse.make_option("--e300-remind-interval", action="store", dest="e300_remind_interval",
                                 help='E300 will be "reminded" to stay connected every INTERVAL seconds (default=%d).' % self.e300_remind_interval,
                                 metavar="INTERVAL", type="int"),
            optparse.make_option("-f", "--sequence-file", action="store", dest="sequence",
                                 help="specify the file from which to read the autocal sequence"),
            optparse.make_option("-g", "--grid-view", action="store_true", dest="grid",
                                 help="print grid calibration list for 'show/list' command"),
            optparse.make_option("-l", "--log-directory", action="store", dest="log_directory",
                                 help="specify the directory to which log files should be written"),
            optparse.make_option("-n", "--note", action="store", dest="note", help="set the log file note"),
            optparse.make_option("-s", "--sensors", action="store", dest="sensors",
                                 help="limit the operation to these sensors"),
            optparse.make_option("-x", "--extended-view", action="store_true", dest="extended",
                                 help="print extended calibration list for 'show/list' command (expands grid view when used with -g flag)")
        ]
        QScript.__init__(self)

        # Allows easier re-directing of output to another handler
        self._print = self._write
        self.logger = None

        self.filter_digitizers = []
        self.filter_sensors = []
        self.filter_cals = []

        self.channel_server = None
        self.channel_controls = {}

    def local_usage(self):
        return """usage: %s [options] <action>
action:  operation to perform ('start', 'stop', 'list/show', 'status')""" % sys.argv[0].split('/')[-1]

    def run_cals(self, cals, simulate=False, cal_status=False, display='default'):
        total_duration = 0
        num_cals = len(cals)
        has_e300 = False
        has_control = False

        if self.options.e300_remind_interval is not None:
            self.e300_remind_interval = self.options.e300_remind_interval

        e300_connected = False
        for (cal, info) in cals:
            total_duration += info['total_duration']
            if cal.e300:
                has_e300 = True
            if cal.group not in (None, ""):
                has_control = True

        if not cal_status:
            action_msg = "Running"
            if simulate:
                action_msg = "Found"
            self._print("%s %d calibrations total duration of %d seconds" % (action_msg, num_cals, total_duration))

        # List summary of selected calibrations
        if simulate:
            self._print()
            extended = False
            view = 'compact'
            channel_groups = []
            if self.options.verbosity is not None and self.options.verbosity > 1:
                view = 'verbose'
            extended_grid = False
            if self.options.grid is not None:
                view = 'grid'
                if self.options.extended:
                    extended_grid = True
            elif self.options.extended is not None:
                view = 'verbose'

            if view == 'grid':
                print_cal_grid(cals, print_func=self._print, extended=extended_grid)
            for (cal, info) in cals:
                if view != 'grid':
                    self._print(format_cal(cal, info, view))
                if cal.group and cal.group in self.channel_controls:
                    channel_groups.append(cal.group)
            if len(channel_groups) and self.channel_server:
                self._print("")
                self._print(format_server(self.channel_server, view))
            else:
                self._print("")
                self._print("CHANNEL.SERVER %s" % self.channel_server)
            for group_id in sorted(set(channel_groups)):
                if group_id in self.channel_controls:
                    controls = self.channel_controls[group_id]
                    for control in sorted(controls, key=lambda con: (con['group'], con['station'], con['location'], con['channel'])):
                        self._print(format_control(control, view))

        # Check for autocal proceses and Q330s with calibrations running
        elif cal_status:
            devices = []  # devices that are running cals
            for (cal, info) in cals:
                if info['device'] not in devices:
                    devices.append(info['device'])

            for device in devices:
                comm = QComm()
                comm.set_from_file(device, self.options.reg_file)
                comm.set_max_tries(self.max_tries)

                # Make sure there are no cals currently running
                ping = QPing(device_type=comm.get_device_type())
                ping.suppress_stdout()
                ping.action = 'status'
                ping.status = ['Global']
                wait_count = 0

                try:
                    comm.execute(ping)
                except QDPDevice.TimeoutException as e:
                    self._print("Timeout getting status from Q330 #%s\n" % str(device))
                    continue

                cal_stat = ping.get_result_data()[0]['CalibratorStatus'][2]
                if (len(cal_stat) >= 7) and (cal_stat[0:7] == 'Enabled'):
                    self._print("Calibration is running on Q330 #%s\n" % str(device))

                del ping
                del comm

        # Run Calibrations
        else:
            # Get current time from a Q330
            comm = QComm()
            comm.set_from_file(cals[0][1]['device'], self.options.reg_file)
            comm.set_max_tries(self.max_tries)
            ping = QPing(device_type=comm.get_device_type())
            ping.suppress_stdout()
            ping.action = 'status'
            ping.status = ['Global']

            max_time_checks = 3
            time_check_wait = 3.0
            tries = 0
            while 1:
                try:
                    comm.execute(ping)
                    break
                except QDPDevice.TimeoutException as e:
                    message = "Attempt %d/%d: Timeout while retrieving GPS time from Q330.\n" % (tries + 1, max_time_checks)
                    self._print(message, category="warn")
                    if tries > max_time_checks:
                        self._print("Failed to retrieve GPS time from Q330.\n", category="err")
                        raise
                time.sleep(time_check_wait)
                tries += 1

            result_data = ping.get_result_data()[0]
            sec_off = result_data['SecondsOffset'][2]
            cdsn = result_data['CurrentDataSequenceNumber'][2]
            q330_time = sec_off + cdsn
            unix_time = q330_to_unix_time(q330_time)
            time_now = time.gmtime(unix_time)
            time_complete = time.gmtime(unix_time + total_duration)

            self._print("  Current Time: %s" % time.strftime("%Y/%m/%d %H:%M UTC", time_now))
            self._print("  Expected Completion Time: %s" % (time.strftime("%Y/%m/%d %H:%M UTC", time_complete),))

            # Launch the E300 reminder thread if there is an E300
            # somewhere in the line-up
            if has_e300:
                self._threads['E300'] = E300Thread(self._print, remind_interval=self.e300_remind_interval)
                if 'E300' not in self._threads or not self._threads['E300']:
                    message = "E300 reminder thread was not launched. Autocal aborted"
                    self._print(message, category='err')
                    raise QExMessage(message)
                self._threads['E300'].start()

                self._print("E300 requesting initial disconnect", category='dbg')
                try:
                    self._threads['E300'].request_queue.put_nowait('disconnect')
                    if self._threads['E300'].response_queue.get() != 'disconnect':
                        self._print("E300 did not disconnect prior to first cal.", category='warn')
                    else:
                        self._print("E300 successfully disconnected", category='dbg')
                    self._threads['E300'].response_queue.task_done()
                except queue.Full:
                    message = "E300 thread request queue is full"
                    self._print(message, category='err')
                    raise QExMessage(message)
                except queue.Empty:
                    message = "E300 thread response queue is empty"
                    self._print(message, category='err')
                    raise QExMessage(message)
                except ValueError:
                    self._print("E300 called task_done() too many times on thread response queue", category='err')

            if has_control:
                self._threads['CONTROL'] = ChannelThread(self._print, self.channel_server)
                if 'CONTROL' not in self._threads or not self._threads['CONTROL']:
                    message = "Channel control thread was not launched. Autocal aborted"
                    self._print(message, category='err')
                    raise QExMessage(message)
                self._threads['CONTROL'].start()

            last_group = None
            for (cal, info) in cals:
                comm = QComm()
                comm.set_from_file(info['device'], self.options.reg_file)
                comm.set_max_tries(self.max_tries)

                # Prevent the QCal class from printing status information to stdout
                cal.set_log_function(self._print)
                cal.suppress_stdout()
                # cal.quiet = True

                # Make sure there are no cals currently running
                ping = QPing(device_type=comm.get_device_type())
                ping.suppress_stdout()
                ping.action = 'status'
                ping.status = ['Global']
                wait_count = 0
                first_time = int(time.time())
                while 1:
                    cal_running = False
                    comm_timeout = False
                    try:
                        comm.execute(ping)
                        cal_stat = ping.get_result_data()[0]['CalibratorStatus'][2]
                        cal_running = (len(cal_stat) >= 7) and (cal_stat[0:7] == 'Enabled')
                    except QDPDevice.TimeoutException as e:
                        comm_timeout = True

                    if wait_count > max_wait:
                        message = "Calibration already running or comm-timeout, maximum wait time (%d seconds) exceeded" % (
                                    max_wait * wait_period)
                        self._print(message, category='err')
                        raise QExMessage(message)
                    if comm_timeout or cal_running:
                        if not cal.auto_timing:
                            if comm_timeout:
                                self._print("Q330 link timed out, re-trying\n")
                            else:
                                self._print("Calibration already running, trying again\n", category='dbg')
                            self._print("  waited %d seconds, count = %d" % (int(time.time()) - first_time, wait_count),
                                        category='dbg')
                            time.sleep(1.0)
                        else:
                            if comm_timeout:
                                self._print("Q330 link timed out, re-trying\n")
                            else:
                                self._print("Calibration already running, sleeping %d seconds\n" % wait_period)
                            time.sleep(wait_period)
                        wait_count = (int(time.time()) - first_time) / wait_period
                    else:
                        break

                if last_group != cal.group:
                    self._threads['CONTROL'].clean_wait()
                    if cal.group not in (None, ""):
                        for control in self.channel_controls[cal.group]:
                            if not self._threads['CONTROL'].put(("CONTROL", control), timeout=15.0):
                                message = "CHANNEL.CONTROL request timeout"
                                # self._print(message, category='err')
                                raise QExMessage(message)
                            result = self._threads['CONTROL'].get(control, timeout=15.0)
                            if result is None:
                                message = "CHANNEL.CONTROL response timeout"
                                # self._print(message, category='err')
                                raise QExMessage(message)
                            elif result[0] == "OKAY":
                                message = "CHANNEL.CONTROL applied"
                                # self._print(message, category='info')
                            else:
                                message = "CHANNEL.CONTROL failed"
                                # self._print(message, category='err')
                                raise QExMessage(message)
                else:
                    self._print("Channel control group unchanged.", category='dbg')
                last_group = cal.group

                if cal.e300:
                    e300_connected = True
                    # This cal. involves the E300
                    self._print("E300 requesting connect", category='dbg')
                    self._threads['E300'].input = cal.e300_input
                    try:
                        self._threads['E300'].request_queue.put_nowait('connect')
                        if self._threads['E300'].response_queue.get() != 'connect':
                            self._print("E300 still disconnected, skipping cal. #%d" % info['index'], category='warn')
                            continue
                        else:
                            self._print("E300 is connected", category='dbg')
                        self._threads['E300'].response_queue.task_done()
                    except queue.Full:
                        message = "E300 thread request queue is full"
                        self._print(message, category='err')
                        raise QExMessage(message)
                    except queue.Empty:
                        message = "E300 thread response queue is empty"
                        self._print(message, category='err')
                        raise QExMessage(message)
                    except ValueError:
                        self._print("E300 called task_done() too many times on thread response queue", category='err')

                elif has_e300 and e300_connected:
                    e300_connected = False
                    # This cal. does not involve the E300, but there is an E300 cal. in the line-up,
                    # so tell the E300 to disconnect for now
                    self._print("E300 requesting disconnect", category='dbg')
                    try:
                        self._threads['E300'].request_queue.put_nowait('disconnect')
                        if self._threads['E300'].response_queue.get() == 'connect':
                            self._print("E300 still connected", category='dbg')
                        else:
                            self._print("E300 is disconnected", category='dbg')
                        self._threads['E300'].response_queue.task_done()
                    except queue.Full:
                        message = "E300 thread request queue is full"
                        self._print(message, category='err')
                        raise QExMessage(message)
                    except queue.Empty:
                        message = "E300 thread response queue is empty"
                        self._print(message, category='err')
                        raise QExMessage(message)
                    except ValueError:
                        self._print("E300 called task_done() too many times on thread response queue", category='err')

                # Register
                try:
                    comm.register()
                except QExMessage as e:
                    self._print(str(e), category="err")
                    raise

                # Run the calibration
                try:
                    comm.execute(cal)
                except QDPDevice.TimeoutException as e:
                    self._print("Timeout sending cal. command: %s" % str(e), category="err")
                    raise

                # De-Register
                try:
                    comm.deregister()
                except QExMessage as e:
                    self._print(str(e), category="warn")
                    # Keep an eye on this.
                    # It may be better to simply re-raise like we used to...

                # Report calibration start
                cal_time_string = ""
                if cal.start_time:
                    cal_time_string = "at %s" % time.strftime("%Y/%m/%d %H:%M:%S UTC",
                                                              time.gmtime(q330_to_unix_time(cal.start_time)))
                else:
                    cal_time_string = "now (%s)" % time.strftime("%Y/%m/%d %H:%M:%S UTC",
                                                                 time.gmtime(q330_to_unix_time(cal.q330_time)))
                self._print("Sending command for calibration #%d %s...\n" % (info['index'], cal_time_string))

                # Verify that the cal started, and record the start time
                wait_count = 0
                while 1:
                    comm_timeout = False
                    try:
                        comm.execute(ping)
                        cal_stat = ping.get_result_data()[0]['CalibratorStatus'][2]
                        station_time = ping.get_result_data()[0]['StationTime'][2]
                    except QDPDevice.TimeoutException as e:
                        comm_timeout = True

                    if wait_count > max_start_checks:
                        message = "Calibration #%d failed to start, max wait (%d seconds) exceeded" % (info['index'], start_check_period * max_start_checks)
                        self._print(message, category='err')
                        raise QExMessage(message)
                    if comm_timeout:
                        self._print("Timeout on start check %d for calibration #%d\n" % (wait_count + 1, info['index']))
                    elif (len(cal_stat) >= 7) and (cal_stat[0:7] == 'Enabled'):
                        self._print("Calibration #%d has started, expected duration is %d seconds\n" % (info['index'], info['total_duration']))
                        break
                    time.sleep(start_check_period)
                    wait_count += 1

                # Wait until this cal has finished before running the next cal
                if has_e300:
                    try:
                        self._print("Waiting for calibration to complete")
                        e300_msg = self._threads['E300'].response_queue.get(block=True, timeout=info['total_duration'])
                        if e300_msg == 'halt':
                            self._print("E300 thread halted, aborting autocal", category='info')
                            self.stop_cals()
                            break
                        else:
                            self._print("Uh-oh, got a message from the E300 thread: \"%s\"" % e300_msg, category='warn')
                            self.stop_cals()
                        # We must halt any calibration running on the Q330 so
                        # that we can continue with any remaining calibrations.
                    except queue.Empty:
                        pass
                else:
                    sleep_time = info['total_duration'] - 20
                    if sleep_time > 0:
                        time.sleep(float(sleep_time))

            # Verify that the last cal has ended, and record the end time
            wait_count = 0
            while 1 and (comm is not None):
                comm_timeout = False
                try:
                    comm.execute(ping)
                    cal_stat = ping.get_result_data()[0]['CalibratorStatus'][2]
                    station_time = ping.get_result_data()[0]['StationTime'][2]
                except QDPDevice.TimeoutException as e:
                    comm_timeout = True

                if wait_count > max_stop_checks:
                    message = "Could not confirm end of final calibration, max wait (%d seconds) exceeded" % (
                    stop_check_period * max_stop_checks,)
                    self._print(message, category='err')
                    raise QExMessage(message)
                if comm_timeout:
                    self._print("Timeout on stop check %d for final calibration\n" % (wait_count + 1,))
                elif (len(cal_stat) >= 8) and (cal_stat[0:8] == 'Disabled'):
                    self._print("Final calibration is complete\n")
                    break
                time.sleep(stop_check_period)
                wait_count += 1

            # Make sure the E300 thread is done
            if has_e300 and 'E300' in self._threads and self._threads['E300'] and self._threads['E300'].is_alive():
                try:
                    self._print("Halting the E300 reminder thread", category='dbg')
                    self._threads['E300'].request_queue.put_nowait('halt')
                    self._print("Waiting for E300 thread to finish", category='dbg')
                    self._threads['E300'].join(60)
                    del self._threads['E300']
                except:
                    pass
                self._print("E300 thread is finished", category='dbg')

            # Return all channel controls to their default state
            if last_group not in (None, ""):
                self._threads['CONTROL'].clean_wait()

        if (not cal_status) and (not simulate):
            self._print("All calibrations are complete")

    def stop_cals(self):
        comm = QComm()
        comm.set_max_tries(self.max_tries)
        device_ids = comm.get_device_ids(self.options.reg_file)

        for device_id in device_ids:
            comm.set_from_file(device_id, self.options.reg_file)

            action = QCalStop()
            action.suppress_stdout()

            # Ensure that no cals are running
            ping = QPing(device_type=comm.get_device_type())
            ping.suppress_stdout()
            ping.action = 'status'
            ping.status = ['Global']
            wait_count = 0

            # run the cal
            self._print("Sending calibration stop request...")
            try:
                comm.register()
            except:
                self._print("Registration failed. Unable to send stop request", category="warn")
                try:
                    comm.deregister()
                except:
                    pass
                continue

            try:
                comm.execute(action)
            except Exception as e:
                self._print("Calibration stop command failed: %s" % str(e), category="warn")
                try:
                    comm.deregister()
                except:
                    pass
                continue
            self._print("Request sent.")

            try:
                comm.deregister()
            except:
                self._print("Deregistration failed after sending stop request..", category="warn")
                continue

            # record the start time
            wait_count = 0
            while 1:
                comm.execute(ping)
                cal_stat = ping.get_result_data()[0]['CalibratorStatus'][2]
                station_time = ping.get_result_data()[0]['StationTime'][2][:-7]
                if wait_count >= max_start_checks:
                    message = "Failed to stop calibration, max wait (%d min.) exceeded" % (
                    start_check_period * max_start_checks,)
                    self._print(message, category='err')
                    raise QExMessage(message)
                if (len(cal_stat) >= 7) and (cal_stat[0:7] == 'Enabled'):
                    self._print("Calibration is still running...")
                    cal_result = station_time
                else:
                    self._print("Calibration has halted.")
                    break
                time.sleep(start_check_period)
                wait_count += 1

            if action:
                del action

    def spawn_cal(self):
        arg_list = sys.argv[:]
        arg_list[0] = 'autocal'
        for i in range(len(arg_list)):
            if arg_list[i] == 'start':
                arg_list[i] = 'run'
                break
        process = subprocess.Popen(arg_list, shell=False, stdin=None, stdout=None, stderr=None)
        self._write("Spawned autocal process (pid %d)" % process.pid)

    def _write(self, string=None, to_stdout=True, category=None):
        QScript._print(self, string, to_stdout)

    def _log(self, string=None, to_stdout=False, category=None):
        if self.logger:
            if category:
                self.logger.log(string, category)
            else:
                self.logger.log(string)

    def _run(self):
        assert_arg_count(self.args, 'eq', 1)
        arg_action = self.args[0]

        sequence_file = '/etc/q330/autocal.config'
        if not os.path.exists(sequence_file):
            sequence_file = 'autocal.config'
        if not os.path.exists(sequence_file):
            if 'USERPROFILE' in os.environ:
                sequence_file = os.path.abspath(os.environ['USERPROFILE'] + '/autocal.config')
        if not os.path.exists(sequence_file):
            if 'HOME' in os.environ:
                sequence_file = os.path.abspath(os.environ['HOME'] + '/autocal.config')
        if self.options.sequence is not None:
            sequence_file = self.options.sequence

        if self.options.verbosity is not None and self.options.verbosity > 0:
            self._print("sequence file: %s" % sequence_file)

        # Parse config files and verify cals
        device_ids = QComm().get_device_ids(self.options.reg_file)
        cals, self.channel_server, self.channel_controls = parse_cals(sequence_file, self._print)

        num_cals = len(cals)
        arg_name = "unknown"
        f_cals = False
        f_digitizers = False
        f_sensors = False
        digitizer_string = None
        sensor_string = None
        cal_string = None
        try:
            if self.options.digitizers is not None and len(self.options.digitizers):
                arg_name = "digitizers"
                self.filter_digitizers = expand_int_list(self.options.digitizers)
                f_digitizers = True
                digitizer_string = "digitizers: %s" % ','.join(map(str, self.filter_digitizers))
                if arg_action not in ('run', 'start'):
                    self._print(digitizer_string)
            if self.options.sensors is not None and len(self.options.sensors):
                arg_name = "sensors"
                self.filter_sensors = expand_sensor_list(self.options.sensors)
                f_sensors = True
                sensor_string = "sensors: %s" % ','.join(self.filter_sensors)
                if arg_action not in ('run', 'start'):
                    self._print(sensor_string)
            if self.options.cals is not None and len(self.options.cals):
                arg_name = "cals"
                self.filter_cals = expand_int_list(self.options.cals)
                f_cals = True
                cal_string = "cals: %s" % ','.join(map(str, self.filter_cals))
                if arg_action not in ('run', 'start'):
                    self._print(cal_string)
        except ValueError as e:
            raise QExArgs("Invalid value for argument '%s'" % arg_name)

        for device_id in self.filter_digitizers:
            if str(device_id) not in device_ids:
                raise QExArgs("Digitizer #%d not found" % device_id)
        for cal_id in self.filter_cals:
            if (cal_id < 1) or (cal_id > num_cals):
                raise QExArgs("Cal #%d not found" % cal_id)

        dig_list = []
        if f_digitizers:
            for device in self.filter_digitizers:
                for (cal, info) in cals:
                    if int(info['device']) == device:
                        dig_list.append((cal, info))
        else:
            dig_list = cals

        sens_list = []
        if f_sensors:
            for sensor in self.filter_sensors:
                for (cal, info) in cals:
                    if cal.sensor == sensor:
                        sens_list.append((cal, info))
        else:
            sens_list = dig_list

        cal_list = []
        if f_cals:
            for index in self.filter_cals:
                for (cal, info) in cals:
                    if info['index'] == index:
                        cal_list.append((cal, info))
        else:
            cal_list = sens_list

        cals = cal_list

        if len(cals) == 0:
            raise QExMessage('No calibrations in selected sequence.')

        if arg_action == 'stop':
            # Halt any running autocals.
            lock = QLock()
            try:
                lock.set_name('autocal')
                lock.set_pid()
                lock.acquire()
                lock.release()
            except Exception as e:
                # print str(e)
                pid = lock.get_proc_id()
                try:
                    lock.find_proc(pid, name_reg='autocal')
                except ValueError as pid_str:
                    self._print("Sending SIGTERM to autocal process [%d]" % pid)
                    os.kill(pid, 15)
                    max_tries = 15
                    count = max_tries
                    while 1:
                        try:
                            lock.find_proc(pid, name_reg='autocal')
                            print()
                            break
                        except ValueError as pid_str:
                            count_down = ', %d' % count
                            if count == max_tries:
                                count_down = str(count)
                            sys.stdout.write(count_down)
                            sys.stdout.flush()
                            count -= 1
                            time.sleep(1.0)
                            if count <= 0:
                                print()
                                self._print("Sending SIGKILL to autocal process [%d]" % pid)
                                os.kill(pid, 9)
                                break
                            # sys.stdout.write(".")
                            sys.stdout.flush()

            # Halt cals running on all Q330s
            self.stop_cals()
        elif arg_action in ('run', 'start'):
            # Check for running autocals, if any, warn and bail
            lock = QLock()
            lock.set_name('autocal')
            lock.set_pid()
            try:
                lock.acquire()
                if arg_action == 'start':
                    lock.release()
            except QExLocked as e:
                raise QExMessage("There is an autocal process running [%s]." % str(e))

            # Start the autocal
            if arg_action == 'start':
                self.spawn_cal()
            elif arg_action == 'run':
                # Locate a valid log directory
                log_dirs = [
                    '/opt/util/logs',
                    './logs',
                    '.'
                ]

                # Give the following log directory entries precidence 
                if 'AUTOCAL_LOG_DIR' in os.environ:
                    log_dirs.insert(0, os.environ['AUTOCAL_LOG_DIR'])
                if self.options.log_directory is not None:
                    log_dirs.insert(0, self.options.log_directory)

                # Run log directory check
                log_dir = ''
                for d in log_dirs:
                    log_dir = d
                    if os.path.exists(log_dir):
                        break
                    try:
                        os.makedirs(log_dir)
                        if not os.access(log_dir, os.W_OK):
                            raise Exception("Cannot write to directory '%s'" % log_dir)
                        break
                    except Exception as e:
                        log_dir = '.'
                log_note = ''
                if self.options.note is not None:
                    log_note = self.options.note
                self.logger = Logger(directory=log_dir, prefix='autocal_', note=log_note)
                if self.options.verbosity is not None:
                    if self.options.verbosity > 0:
                        self.logger.set_log_debug(True)
                    if self.options.verbosity > 2:
                        self.logger.set_log_to_screen(True)
                self._print = self._log

                self._print("")
                self._print("=================================")
                self._print("=== Autocal Sequence Starting ===")
                self._print("=================================")
                self._print("registration file: %s" % os.path.abspath(self.comm.get_file()))
                self._print("sequence file: %s" % os.path.abspath(sequence_file))
                self._print("log directory: %s" % os.path.abspath(log_dir))
                if digitizer_string is not None:
                    self._print(digitizer_string)
                if sensor_string is not None:
                    self._print(sensor_string)
                if cal_string is not None:
                    self._print(cal_string)

                # Run each cal.
                self.run_cals(cals)
                lock.release()
        elif arg_action in ('list', 'show'):
            self.run_cals(cals, simulate=True)
        elif arg_action == 'status':
            lock = QLock()
            lock.set_name('autocal')
            lock.set_pid()
            try:
                lock.acquire()
                lock.release()
                self._print("There are no autocals running.")
            except QExLocked as e:
                self._print("There is an autocal process running [%s]." % str(e))
            self.run_cals(cals, cal_status=True)
        else:
            raise QExArgs("invalid option for 'action'")


script = QSAutocal()
script.run()
