
import base64


def config_to_raw(config, b64=False):
    raw = ''
    data_lines = []
    for k, v in list(config.items()):
        if type(v) == list:
            for l in v:
                data_lines.append("%s > %s\n" % (k, l))
        else:
            data_lines.append("%s = %s\n" % (k, v))
    raw = ''.join(data_lines)
    if b64:
        raw = base64.standard_b64encode(raw)


def raw_to_config(raw, b64=False):
    config = {}
    count = 0
    if b64:
        raw = base64.standard_b64decode(raw)
    for line in raw.split('\n'):
        count += 1
        if line.strip() == '':
            continue
        if line[0] == '#':
            continue
        line = line.split('#', 1)[0]
        key_index = line.find('=')
        grp_index = line.find('>')
        if key_index == grp_index:
            print(line)
            raise ValueError("Invalid format for line %d in configuration" % (count,))
        if (-1 < key_index < grp_index) or (grp_index == -1):
            parts = [l.strip() for l in line.split('=', 1)]
            if len(parts) != 2:
                raise ValueError("Invalid format for line %d in configuration" % (count,))
            key, value = parts
            if key in config:
                raise ValueError("Duplicate key found on line %d in configuration" % (count,))
            config[key] = value
        else:
            parts = [l.strip() for l in line.split('>', 1)]
            if len(parts) != 2:
                raise ValueError("Invalid format for line %d in configuration" % (count,))
            key, value = parts
            if key not in config:
                config[key] = []
            config[key].append(value)
    return config
