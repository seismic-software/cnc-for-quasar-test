
from QScript import QScript
from QBaler import QBaler
from QTools import assert_arg_count, QExArgs


class QSBaler(QScript):
    def __init__(self):
        self.option_list = []
        QScript.__init__(self)

    def local_usage(self):
        return """usage: %%prog [options] <device> <interface> off
usage: %%prog [options] <device> <interface> on|web <timeout>

device    : integer value identifying the Q330
interface : integer value identifying the baler's connection inteface
%s
action    : switch baler power on or off ('off', 'on', 'web')
              off = turn off baler power
              on  = turn on baler for Q330 access
              web = turn on baler for web access
timeout   : seconds after which baler should turn off (range is 1-4095)""" % (
            "\n".join(["              %d = %s" % (k, QBaler.baler_map[k][1]) for k in sorted(QBaler.baler_map.keys())]))

    def _run(self):
        assert_arg_count(self.args, 'gt', 2)
        arg_device_id = self.args[0]
        try:
            int(arg_device_id)
        except:
            raise QExArgs("invalid type for argument 'device'")
        if not self.comm.set_from_file(arg_device_id, self.options.reg_file):
            raise QExArgs("invalid entry for Q330 #%d in configuration" % int(arg_device_id))

        arg_interface = self.args[1]
        arg_action = self.args[2]
        arg_timeout = 1

        if self.args[2] == 'off':
            assert_arg_count(self.args, 'eq', 3)
        else:
            assert_arg_count(self.args, 'eq', 4)
            arg_timeout = self.args[3]

        try:
            action = QBaler()
            action.interface = arg_interface
            action.action = arg_action
            action.timeout = arg_timeout
            action.verbosity = self.options.verbosity
        except TypeError as msg:
            raise QExArgs("invalid type for argument '%s'" % msg.__str__())
        except ValueError as msg:
            raise QExArgs("invalid value for argument '%s'" % msg.__str__())

        self.comm.register()
        self.comm.execute(action)
        self.comm.deregister()


script = QSBaler()
script.run()
