# Useful tools for conversions, formatting, and the like
import calendar
import os
import re
import struct
import sys
import time
import traceback


class QException(Exception):
    def __init__(self, value: object) -> object:
        self.value = value

    def __str__(self):
        return str(self.value)


class QExArgs(QException):
    def get_trace(self):
        """This returns an abbreviated stack trace with lines that 
           only concern the caller."""
        tblist = traceback.extract_tb(sys.exc_info()[2])
        tblist = list(filter(self.__filter_not_pexpect, tblist))
        tblist = traceback.format_list(tblist)
        return ''.join(tblist)

    def __filter_not_pexpect(self, trace_list_item):
        if trace_list_item[0].find('stations.py') == -1:
            return True
        else:
            return False


class QExMessage(QException):
    """Used to pass information back to main script"""


class QExForce(QException):
    """signals that the operation is only allowed when the force option is set"""


class QExAbstract(QException, NotImplementedError):
    """signals that the member should have been overridden"""


class QExInvalid(QException, TypeError):
    """signals that the member failed validation due to invalid type"""


class QExValue(QException, ValueError):
    """signals that the member failed validation due to invalid value"""


class QExNoMember(QException, KeyError, IndexError):
    """signals that a member does not exist"""


class QExProcessFound(QException):
    """signals that an instance of this process is already running"""


class QExLocked(QException):
    """signals that a lock is in use"""


class QExReboot(QException):
    """Signals that a deregistration attempt is unnecessary"""


mode_map = {
    'r': os.R_OK,
    'w': os.W_OK,
    'x': os.X_OK,
}


def max_elements(items, max):
    if len(items) > max:
        return items[0:max]
    return items


def test_dir(dir, mode):
    if mode.lower() not in ('r', 'w', 'x'):
        raise Exception("Invalid mode for test_dir()")
    test = mode_map[mode.lower()]
    return os.path.isdir(dir) and os.access(dir, test)


def test_file(file, mode):
    if mode.lower() not in ('r', 'w', 'x'):
        raise Exception("Invalid mode for test_file()")
    test = mode_map[mode.lower()]
    return os.path.isfile(file) and os.access(file, test)


DOWN = -1
UP = 1


class Counter:
    def __init__(self, original=0, stride=1, wrap=sys.maxsize, negatives=True):
        self.original = original
        self.stride = stride
        self.negatives = negatives
        self.wrap = wrap
        self.wrap_neg = 0
        if negatives:
            self.wrap_neg = -wrap - 1
        self.reset()

    def reset(self):
        self.value = self.original

    def set_value(self, value):
        self.value = value

    def set_stride(self, stride):
        self.stride = stride

    def _step(self, direction):
        if direction == UP:
            if (self.wrap - self.value) < self.stride:
                self.value = self.wrap_neg + (self.stride - (self.wrap - self.value))
            else:
                self.value += self.stride
        else:
            if (self.wrap_neg - self.value) > (-self.stride):
                self.value = self.wrap - (self.stride - -(self.wrap_neg - self.value))
            else:
                self.value -= self.stride

    def inc(self):
        self._step(UP)
        return self.value

    def dec(self):
        self._step(DOWN)
        return self.value

    def inc_p(self):
        temp = self.value
        self._step(UP)
        return temp

    def dec_p(self):
        temp = self.value
        self._step(DOWN)
        return temp


EPOCH_TUPLE = time.strptime("2000 01 01 00:00:00 UTC", "%Y %m %d %H:%M:%S %Z")
Q330_EPOCH = float(calendar.timegm(EPOCH_TUPLE))


def q330_to_unix_time(offset):
    unix_time = None
    if offset is not None:
        unix_time = Q330_EPOCH + offset
    return unix_time


def unix_to_q330_time(offset):
    q330_time = None
    if offset is not None:
        q330_time = offset - Q330_EPOCH
    return q330_time


def format_mac(xxx_todo_changeme):
    (high_32, low_16) = xxx_todo_changeme
    try:
        qassert(type(high_32) in (int, int))
        qassert(0 <= high_32 <= (2 ** 32))
        qassert(type(low_16) in (int, int))
        qassert(0 <= low_16 <= (2 ** 16))
        mac_segments = [
            "%02x" % ((high_32 & 0xff000000) >> 24),
            "%02x" % ((high_32 & 0x00ff0000) >> 16),
            "%02x" % ((high_32 & 0x0000ff00) >> 8),
            "%02x" % ((high_32 & 0x000000ff) >> 0),
            "%02x" % ((low_16 & 0xff00) >> 8),
            "%02x" % ((low_16 & 0x00ff) >> 0)
        ]
        return ':'.join(mac_segments)
    except:
        raise ValueError("Invalid MAC Address")


def parse_mac(mac_address):
    try:
        qassert(re.compile('^(?:[0-9a-fA-F]{2}[:]){5}[0-9a-fA-F]{2}$').search(mac_address))
        high_32 = 0x00000000
        low_16 = 0x0000
        mac_segments = mac_address.split(':')
        for segment in mac_segments:
            qassert(0 <= int(eval("0x%s" % segment)) <= 255)
        high_32 |= (int(eval("0x%s" % mac_segments[0])) & 0xff) << 24
        high_32 |= (int(eval("0x%s" % mac_segments[1])) & 0xff) << 16
        high_32 |= (int(eval("0x%s" % mac_segments[2])) & 0xff) << 8
        high_32 |= (int(eval("0x%s" % mac_segments[3])) & 0xff) << 0
        low_16 |= (int(eval("0x%s" % mac_segments[4])) & 0xff) << 8
        low_16 |= (int(eval("0x%s" % mac_segments[5])) & 0xff) << 0
        return high_32, low_16
    except:
        raise ValueError("Invalid MAC Address")


def format_ip(ip_value):
    try:
        qassert(type(ip_value) in (int, int))
        qassert(0 <= ip_value <= (2 ** 32))
        ip_segments = [
            str((ip_value & 0xff000000) >> 24),
            str((ip_value & 0x00ff0000) >> 16),
            str((ip_value & 0x0000ff00) >> 8),
            str((ip_value & 0x000000ff) >> 0)
        ]
        return ".".join(ip_segments)
    except:
        raise ValueError("Invalid IP Address")


def parse_ip(ip_address):
    try:
        qassert(re.compile(r'^(?:\d{1,3}[.]){3}\d{1,3}$').search(ip_address))
        ip_value = 0x00000000
        ip_segments = ip_address.split('.')
        for segment in ip_segments:
            qassert(0 <= int(segment) <= 255)
        ip_value |= (int(ip_segments[0]) & 0xff) << 24
        ip_value |= (int(ip_segments[1]) & 0xff) << 16
        ip_value |= (int(ip_segments[2]) & 0xff) << 8
        ip_value |= (int(ip_segments[3]) & 0xff) << 0
        return ip_value
    except:
        raise ValueError("Invalid IP Address")


def fancy_integer(value):
    result_string = ""
    num_string = str(value)
    (qot, rem) = divmod(len(num_string), 3)
    range_start = 0
    if rem:
        result_string += num_string[0:rem]
    else:
        result_string += num_string[0:3]
        range_start = 1
    for i in range(range_start, qot):
        index = rem + (i * 3)
        end = index + 3
        result_string += "," + num_string[index:end]
    return result_string


def verify_status_type(status_type, type_list, exclusions=[]):
    exclusions = [e.lower() for e in exclusions]
    match = []
    match_count = 0
    for type in type_list:
        if type.lower() in exclusions:
            continue
        if type.lower().find(status_type.lower()) == 0:
            match.append(type)
    if len(match) < 1:
        match = None
    return match


# def usage(local_usage=None, error_string=None, quit=True, exec_name=None):
#    if not exec_name:
#        exec_name = sys.argv[0]
#    if error_string:
#        print "E: %s" % error_string
#    if local_usage:
#        local_usage(exec_name)
#    if quit:
#        sys.exit(1)

# Verify input
def verify_int(value, limit_lower=None, limit_upper=None):
    result = False
    try:
        int_value = int(value)
        if limit_lower:
            qassert(int_value >= limit_lower)
        if limit_upper:
            qassert(int_value <= limit_upper)
        result = True
    except:
        pass
    return result


def verify_float(value, limit_lower=None, limit_upper=None):
    result = False
    try:
        float_value = float(value)
        if limit_lower:
            qassert(float_value >= limit_lower)
        if limit_upper:
            qassert(float_value <= limit_upper)
        result = True
    except:
        pass
    return result


def verify_party(value, valid_matches):
    result = False
    try:
        qassert(value in valid_matches)
        result = True
    except:
        pass
    return result


def verify_regex(value, expression):
    result = False
    try:
        qassert(re.compile(expression).match(value))
        result = True
    except:
        pass
    return result


def assert_arg_count(args, cmp_str, length):
    err_str = "wrong number of arguments"
    try:
        if cmp_str == 'eq':
            qassert(len(args) == length)
        elif cmp_str == 'lt':
            err_str = "too many arguments"
            qassert(len(args) < length)
        elif cmp_str == 'lte':
            err_str = "too many arguments"
            qassert(len(args) <= length)
        elif cmp_str == 'gt':
            err_str = "too few arguments"
            qassert(len(args) > length)
        elif cmp_str == 'gte':
            err_str = "too few arguments"
            qassert(len(args) >= length)
    except:
        raise QExArgs(err_str)


def qassert(assertion):
    if not assertion:
        raise AssertionError()


def hex_dump(bytes, width=16):
    total = 0
    count = 0
    string = ''
    result = "%08x " % total
    for b in map(int, struct.unpack(">%dB" % len(bytes), bytes)):
        result += " %02x" % b
        if 31 < b < 127:
            string += chr(b)
        else:
            string += '.'
        count += 1
        total += 1
        if (width == 16) and (count == 8):
            result += " "
        if count == width:
            result += "  " + string + "\n"
            result += "%08x " % total
            count = 0
            string = ''
    if len(string):
        while count < width:
            count += 1
            result += "   "
        result += "  " + string + "\n"
    return result


user_tag_regex = re.compile("^[0-9A-Z]{0,6}$")
user_tag_alpha = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"


def encode_user_tag(value):
    if type(value) != str:
        raise TypeError("Bad Type")
    value = value.upper()
    if not user_tag_regex.match(value):
        raise ValueError("Invalid Format")
    if value == "":
        return 0xffffffff
    return int(value, 36)


def decode_user_tag(value):
    if not isinstance(value, int):
        raise TypeError("Bad Type")
    value = int(value & 0xffffffff)
    if 0 > value > 2176782335:
        raise ValueError("Invalid Value")
    if value == 0xffffffff:
        return ""
    result = ""
    while value:
        value, r = divmod(value, 36)
        result = user_tag_alpha[r] + result
    return result or user_tag_alpha[0]


def parse_bool(string, format):
    if format in bool_format_map:
        pair = bool_format_map[format]
        if string in pair:
            if string == pair[0]:
                return True
            return False
        else:
            raise ValueError("Invalid Boolean Value '%s' (%s)" % (string, pair))
    else:
        raise ValueError('Invalid Format')


def expand_int_list(list_str):
    results = []
    regex_int_list = re.compile(r"^(\d{1,}(?:[-]\d{1,})?)(?:[,](\d{1,}(?:[-]\d{1,})?))*$")
    matches = regex_int_list.search(list_str)
    if matches is None:
        raise ValueError("")
    groups = list_str.split(',')
    for group in groups:
        pair = group.split('-')
        if len(pair) > 1:
            start = int(pair[0])
            end = int(pair[1]) + 1
            if start >= end:
                raise ValueError("")
            results.extend([int(i) for i in range(start, end)])
        else:
            results.append(int(pair[0]))
    return list(results)


def expand_int_list_set(list_str):
    return list(set(expand_int_list(list_str)))


bool_format_map = {
    'YN': ('YES', 'NO'),
    'Yn': ('Yes', 'No'),
    'yn': ('yes', 'no'),
    'NF': ('ON', 'OFF'),
    'Nf': ('On', 'Off'),
    'nf': ('on', 'off'),
    'TF': ('TRUE', 'FALSE'),
    'Tf': ('True', 'False'),
    'tf': ('true', 'false'),
    'ED': ('ENABLED', 'DISABLED'),
    'Ed': ('Enabled', 'Disabled'),
    'ed': ('enabled', 'disabled'),
}
