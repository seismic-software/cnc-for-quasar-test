
from QView import QView
from QTools import assert_arg_count, QExArgs
from QScript import QScript


class QSView(QScript):
    def __init__(self):
        self.option_list = [
            # optparse.make_option("-b", "--bytes", action="store", dest="bytes", help="number of bytes of memory to read"),
            # optparse.make_option("-o", "--offset", action="store", dest="offset", help="memory offset at which to start reading"),
        ]
        QScript.__init__(self)

    #     def local_usage(self):
    #         action_string = ''
    #         for key in sorted(QView.sections[self.comm.get_device_type()]):
    #             d = QView.section_map[key][0]
    #             action_string += "         %s : %s\n" % (key.ljust(5), d)
    #
    #         return """usage: %%prog [options] <device> <section>
    #
    # device  : integer value identifying the %s
    # section : which configuration to view
    #          (The following section keys are case insensitive)
    # %s
    # """ % (self.comm.get_device_type(), action_string)

    def _run(self):
        assert_arg_count(self.args, 'gt', 1)

        arg_device_id = self.args[0]
        self.comm.set_from_file(arg_device_id, self.options.reg_file)

        arg_sections = self.args[1:]
        action = QView()
        try:
            action.sections = arg_sections
        except TypeError as msg:
            raise QExArgs("invalid type for argument '%s'" % msg.__str__())
        except ValueError as msg:
            raise QExArgs("invalid value for argument '%s'" % msg.__str__())

        self.comm.register()
        self.comm.execute(action)
        self.comm.deregister()


script = QSView()
script.run()
