
from QClass import QClass
from QTools import qassert
from Quasar.Q330 import Q330


class QAction(QClass):
    def __init__(self):
        self._suppress_stdout = False
        self._result_buffer = ""
        self._result_data = []
        self._log = None
        QClass.__init__(self)

    def execute(self, q330):
        self._clear_results()
        qassert(isinstance(q330, Q330))
        if '_execute' not in dir(self):
            raise NotImplementedError("method _execute() is not implemented")
        self._execute(q330)

    def set_log_function(self, log_func):
        self._log = log_func

    def get_result_buffer(self):
        return self._result_buffer

    def get_result_data(self):
        return self._result_data

    def suppress_stdout(self, suppress=True):
        self._suppress_stdout = suppress   
    
    def log(self, string):
        try:
            self._log(string)
        except:
            pass

    def _print(self, string=None, to_stdout=True):
        if string is None:
            string = ""
        if string != "":
            if string[-1] == "\n":
                string = string[:-1]
        self._result_buffer += string + "\n"
        if to_stdout and not self._suppress_stdout:
            print(string)
        self.log(string)

    def _clear_results(self):
        self._result_data = []

    def _add_result(self, item):
        self._result_data.append(item)

    def _add_results(self, items):
        qassert(type(items) == list)
        self._result_data.extend(items)
