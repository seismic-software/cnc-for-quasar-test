import os
import socket

from QAction import QAction
from QTools import QExMessage, QExArgs, qassert
from Quasar.Site.Config import Config
from Quasar.Q330 import Q330, QDP_C1_CERR
from Quasar.QDPDevice import QDPDevice, TimeoutException

CONFIG_DEFAULT = 'qregister.config'
CONFIG_PATH_DEFAULT = '/etc/q330/' + CONFIG_DEFAULT
DEVICE_TYPES = ['Q330', 'Q335']


class SiteConf:
    def __init__(self):
        self.file = ''
        self.device_map = None
        self.selected_device = None

    def set_file(self, filename):
        if os.path.isfile(filename):
            self.file = filename
        else:
            raise QExMessage("Registration file %s does not exist" % filename)

    def parse_config(self):
        self.selected_device = None
        self.device_map = None
        if not self.file:
            raise QExMessage("Registration file not specified")
        if not os.path.isfile(self.file):
            raise QExMessage("Registration file %s does not exist" % self.file)
        config = Config(self.file)
        temp_map = config.getDeviceMaps()
        self.device_map = {}
        for type, map in list(temp_map.items()):
            if type not in DEVICE_TYPES:
                continue
            for id, device in list(map.items()):
                if id in self.device_map:
                    raise QExMessage("Config: Duplicate device ID %d")
                device['DeviceType'] = type
                self.device_map[id] = device

    def select_device(self, device_id):
        if not self.device_map:
            raise QExMessage("Config: device map is not populated")
        if device_id not in self.device_map:
            raise QExMessage("Config: Device ID %s not found" % device_id)
        self.selected_device = device_id

    def get_device_ids(self):
        return sorted(self.device_map.keys())

    def get_info(self, key):
        if not self.selected_device:
            raise QExMessage("Config: No device selected")
        elif self.selected_device not in self.device_map:
            raise QExMessage(f"Config: ID {self.selected_device} not found")
        elif not key:
            raise QExMessage(f"Config: ID {self.selected_device} > invalid key")
        elif key not in self.device_map[self.selected_device]:
            raise QExMessage(
                f"Config: ID {self.selected_device} > Key {key} not found")
        else:
            return self.device_map[self.selected_device][key]

    def get_ip(self):
        return self.get_info('IP')

    def get_port(self):
        return self.get_info('BasePort')

    def get_serial(self):
        return self.get_info('Serial')

    def get_auth_code(self):
        return self.get_info('AuthCode')

    def get_device_type(self):
        return self.get_info('DeviceType')


class QComm:
    def __init__(self):
        self.q330 = Q330(0, "0.0.0.0", 0)
        self.last_result = None
        self.last_success = False
        self.max_tries_default = True

        self.registered = False
        self.forced = False
        self.verbosity = 0

        self.device_ids = []
        self.file = ''
        if 'QCTL_CONFIG' in os.environ:
            self.file = os.environ['QCTL_CONFIG']
        if not os.path.isfile(self.file):
            self.file = CONFIG_PATH_DEFAULT
        if not os.path.isfile(self.file):
            self.file = CONFIG_DEFAULT
        # if not os.path.isfile(self.file):
        #    if os.environ.has_key('HOME'):
        #        self.file = os.environ['HOME'] + '/' + CONFIG_DEFAULT

    def set_bind_address(self, bind_address):
        if type(bind_address) != str:
            raise QExArgs("Invalid type for bind address")
        parts = bind_address.split(':')
        if len(parts) > 2:
            raise QExArgs("Invalid value for bind address")
        port = 0
        try:
            ip = socket.gethostbyname(parts[0])
            if len(parts) > 1:
                port = int(parts[1])
        except ValueError:
            raise QExArgs("Invalid value for bind address")
        self.q330.setBindAddress((ip, port))

    def set_max_tries(self, tries):
        self.q330.setMaxTries(tries)
        self.max_tries_default = False

    def get_max_tries(self):
        return self.q330.getMaxTries()

    def max_tries_modified(self):
        return not self.max_tries_default

    def set_file(self, filename):
        if os.path.isfile(filename):
            self.file = filename

    def get_file(self):
        return self.file

    def set_device_type(self, device_type):
        self.q330.setDeviceType(device_type)

    def get_device_type(self):
        return self.q330.getDeviceType()

    def set_verbosity(self, verbosity):
        qassert(type(verbosity) == type(int(1)))
        self.verbosity = verbosity

    def set_forced(self, forced=True):
        qassert(forced in (True, False))
        self.forced = forced

    def set_serial(self, serial):
        self.q330.setSerialNumber(serial)

    def set_address(self, address):
        self.q330.setIPAddress(address)

    def set_port(self, port):
        self.q330.setBasePort(port)

    def set_auth_code(self, auth_code):
        self.q330.setAuthCode(auth_code)

    def set_timeout(self, timeout):
        self.q330.setReceiveTimeout(timeout)

    def get_device_ids(self, filename=None):
        result = {}
        if not self.registered:
            config = SiteConf()
            if filename:
                config.set_file(filename)
            else:
                config.set_file(self.file)
            config.parse_config()
            result = config.get_device_ids()
        return result

    def set_from_file(self, device_id, filename=None):
        result = False
        if self.registered:
            raise QExMessage("Cannot change configuration while registered")
        try:
            int(device_id)
        except:
            raise QExArgs("Config: invalid device ID")
        config = SiteConf()
        if filename:
            config.set_file(filename)
        else:
            config.set_file(self.file)
        config.parse_config()
        config.select_device(device_id)

        device_type = config.get_device_type()
        if type(device_type) == list:
            raise QExMessage("Multiple entries for device %s type" % str(device_id))
        self.set_device_type(device_type)

        ip_address = config.get_ip()
        if type(ip_address) == list:
            raise QExMessage("Multiple entries for device %s ip address" % str(device_id))
        self.set_address(ip_address)

        base_port = config.get_port()
        if type(base_port) == list:
            raise QExMessage("Multiple entries for device %s base port" % str(device_id))
        self.set_port(base_port)

        serial_no = config.get_serial()
        if type(serial_no) == list:
            raise QExMessage("Multiple entries for device %s serial number" % str(device_id))
        self.set_serial(eval("0x%s" % serial_no))

        auth_code = config.get_auth_code()
        if type(auth_code) == list:
            raise QExMessage("Multiple entries for device %s authorization code" % str(device_id))
        self.set_auth_code(eval("0x%s" % auth_code))

        result = True
        return result

    def get_result(self):
        return self.last_result

    def get_success(self):
        return self.last_success

    def register(self):
        if self.q330 and not self.registered:
            try:
                self.q330.register()
                self.registered = True
            except TimeoutException as ex:
                raise QExMessage("Timeout Attempting to Register")
            except QDP_C1_CERR:
                raise QExMessage("Registration Failed")
            except:
                raise

    def deregister(self):
        if self.q330 and self.registered:
            try:
                self.registered = False
                self.q330.deregister()
            except TimeoutException as ex:
                raise QExMessage("Timeout Attempting to De-Register")
            except Exception as e:
                print("Exception type is: ", type(e))
                print("Exception is instance of: ", e.__class__.__name__)
                raise QExMessage("De-registration Failed %s" % e.__str__())

    def is_registered(self):
        return self.registered

    def execute(self, action):
        qassert(isinstance(action, QAction))
        try:
            action.execute(self.q330)
        except TimeoutException as ex:
            raise QExMessage(str(ex))
        except:
            raise
