
import optparse
import os
import sys

from QScript import QScript
from QClone import QClone
from Store import Store
from QTools import assert_arg_count, QExArgs, QExMessage, expand_int_list_set
from Quasar.Cloning import Profile


class QSClone(QScript):
    def __init__(self):
        self.option_list = [
            optparse.make_option("-c", "--config-file",
                                 action="store",
                                 dest="config_file",
                                 help="A flat file from which alternate configuration values should be retrieved."),
            optparse.make_option("--config-sqlite",
                                 action="store",
                                 dest="config_sqlite",
                                 metavar="<file>?<station>",
                                 help="An SQLite database from which alternate configuration values should be retrieved."),

            optparse.make_option("--config-mysql",
                                 action="store",
                                 dest="config_mysql",
                                 metavar="[user@]<host>[:port]/<database>?<station>",
                                 help="A MySQL database from which alternate configuration values should be retrieved. If you do not specify a user, you will be prompted for the username."),

            optparse.make_option("--config-text",
                                 action="store",
                                 dest="config_text",
                                 metavar="RAW_CONFIG_TEXT",
                                 help="A raw text string from which the alternate configuration should be parsed."),

            optparse.make_option("--mysql-passwd",
                                 action="store",
                                 dest="mysql_passwd",
                                 metavar="PASSWORD",
                                 help="The password for the MySQL database specified by the --config-mysql option. If you do not use this option, you will be prompted for the password."),

            optparse.make_option("-F", "--force",
                                 action="store_true",
                                 dest="force",
                                 help="Do not prompt for a confirmation when writing Q330 configuration."),

            optparse.make_option("-s", "--subset",
                                 action="store",
                                 dest="subset",
                                 help="Limit the operation to this subset of sections. This is a comma and hyphen separated list of section indices"),

            optparse.make_option("--write-base-port",
                                 action="store_true",
                                 dest="write_base_port",
                                 help="You must specify this flag in order to modify the Q330's base port, even if it is in the supplemental configuration."),

            optparse.make_option("--write-ethernet-ip-address",
                                 action="store_true",
                                 dest="write_ethernet_ip_address",
                                 help="You must specify this flag in order to modify the Q330's Ethernet IP address, even if it is in the supplemental configuration."),

            optparse.make_option("--write-ethernet-mac-address",
                                 action="store_true",
                                 dest="write_ethernet_mac_address",
                                 help="You must specify this flag in order to modify the Q330's Ethernet MAC address, even if it is in the supplemental configuration."),

            optparse.make_option("--write-slave-processor-parameters",
                                 action="store_true",
                                 dest="write_slave_processor_parameters",
                                 help="You must specify this flag in order to write the slave processor parameters to the Q330, even if it is in the subset list."),

            optparse.make_option("--write-web-page",
                                 action="store_true",
                                 dest="write_web_page",
                                 help="You must specify this flag in order to write the web page configuration to the Q330, even if it is in the subset list."),
        ]
        QScript.__init__(self)

    def local_usage(self):
        return """usage: %prog [options] <device> <action> <filename>
    (Applies the specified configuration to the indicated Q330.)
device   : integer value identifying the Q330
action   : operation to perform ('read', 'write')
filename : template file name

usage: %prog [options] <device> edit
    (Modifies the configuration in place if an supplemental configuration was
     supplied. This uses the Q330s own configuration as the template.)
device   : integer value identifying the Q330

usage: %prog [options] diff <file1> <file2>
    (Compares two template files on an item-by-item basis)
file1    : first template file
file2    : second template file

usage: %prog [options] list <filename>
    (Lists the sections contained within the specified clone file.)
filename : template file name

usage: %prog [options] indices
    (Lists all possible indices. The clone can be limited to a subset of these
     indices via the -s flag.)"""

    def _run(self):
        assert_arg_count(self.args, 'gte', 1)

        arg_count = 3
        if self.args[0] == 'indices':
            print("Section Indices:")
            for value in sorted(Profile.section_map.values()):
                index, _, description, _, _ = value
                print("  % 3d : %s" % (index, description))
            return
        elif self.args[0] == 'list':
            assert_arg_count(self.args, 'eq', 2)
            config_file = self.args[1]
            print("Reading clone from %s ..." % os.path.abspath(config_file))
            profile = Profile.getProfileFromFile(config_file)
            profile.listFileContents()
            return
        elif self.args[0] == 'diff':
            assert_arg_count(self.args, 'eq', 3)
            file1 = self.args[1]
            file2 = self.args[2]

            print("Reading configuration from %s..." % file1, end=' ')
            sys.stdout.flush()
            try:
                profile1 = Profile.getProfileFromFile(file1)
            except:
                print("Failed!")
                return
            print("Done.")

            print("Reading configuration from %s..." % file2, end=' ')
            sys.stdout.flush()
            try:
                profile2 = Profile.getProfileFromFile(file2)
            except:
                print("Failed!")
                return
            print("Done.")
            profile1.diff(profile2)
            return

        if (len(self.args) > 1) and (self.args[1] == 'edit'):
            arg_count = 2

        assert_arg_count(self.args, 'eq', arg_count)
        arg_device_id = self.args[0]
        try:
            int(arg_device_id)
        except:
            raise QExArgs("invalid type for argument 'device'")
        if not self.comm.set_from_file(arg_device_id, self.options.reg_file):
            raise QExArgs("invalid entry for Q330 #%d in configuration" % int(arg_device_id))

        config_count = 0
        config_type = ''
        config_subset = None
        mysql_password = ''
        if self.options.subset and len(self.options.subset):
            try:
                config_subset = expand_int_list_set(self.options.subset)
            except ValueError:
                raise QExArgs("invalid value(s) for option '--subset'")
        if self.options.config_file:
            config_info = self.options.config_file
            config_type = 'file'
            config_count += 1
        if self.options.config_sqlite:
            if not Store.SQLITE:
                raise QExMessage("SQLite support is not included with this python install")
            config_info = self.options.config_sqlite
            config_type = 'sqlite'
            config_count += 1
        if self.options.config_mysql:
            if not Store.MYSQL:
                raise QExMessage("MySQL support is not included with this python install")
            config_info = self.options.config_mysql
            config_type = 'mysql'
            config_count += 1
        if self.options.config_text:
            config_info = self.options.config_text
            config_type = 'text'
            config_count += 1
        if self.options.mysql_passwd:
            mysql_password = self.options.mysql_passwd

        try:
            action = QClone()
            action.action = self.args[1]
            if action.action != "edit":
                action.file = self.args[2]
            action.subset = config_subset
            action.verbosity = self.options.verbosity
            action.mysql_password = mysql_password
            if self.options.max_tries:
                action.tries = self.options.max_tries
            if self.options.force:
                action.confirm = False
            if config_count > 1:
                raise QExArgs("You may only select one of --config-[file|sqlite|mysql|text]")
            elif config_count == 1:
                action.config = (config_type, config_info)
            if self.options.write_base_port:
                action.write_base_port = True
            if self.options.write_ethernet_ip_address:
                action.write_ethernet_ip_address = True
            if self.options.write_ethernet_mac_address:
                action.write_ethernet_mac_address = True
            if self.options.write_slave_processor_parameters:
                action.write_slave_processor_parameters = True
            if self.options.write_web_page:
                action.write_web_page = True
        except TypeError as msg:
            raise QExArgs("invalid type for argument '%s'" % msg.__str__())
        except ValueError as msg:
            raise QExArgs("invalid value for argument '%s'" % msg.__str__())

        self.comm.register()
        self.comm.execute(action)
        self.comm.deregister()


script = QSClone()
script.run()
