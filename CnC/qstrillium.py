#!/usr/bin/python
import errno
import queue
import re
import sys
import threading
import time

import select
import serial

try:
    import msvcrt
    PLAT = 'WIN'
except:
    import sys, tty, termios
    PLAT = 'POSIX'

class Getch(object):
    def __init__(self, platform):
        if platform == 'WIN':
            self.getch = self.getch_win
        else:
            self.getch = self.getch_posix
            self.fd = sys.stdin.fileno()
            self.old_settings = termios.tcgetattr(self.fd)
        self.platform = platform
    
    def getch_posix(self):
        self.old_settings = termios.tcgetattr(self.fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(self.fd, termios.TCSADRAIN, self.old_settings)
        return ch

    def getch_win(self):
        return msvcrt.getch()

    def __del__(self):
        if self.platform != 'WIN':
            termios.tcsetattr(self.fd, termios.TCSADRAIN, self.old_settings)


class TrilliumSerialComm:
    def __init__(self, port, comm_timeout=0.2):
        self.serial = serial.Serial(port=port, xonxoff=1)
        self.message_queue = queue.Queue()
        self.running = False

        self.queue_to_display = queue.Queue()
        self.queue_from_display = queue.Queue()

        self._comm_timeout = comm_timeout

    def __del__(self):
        if self.serial:
            if self.serial.isOpen():
                self.serial.close()
            del self.serial

    def __loop(self):
        try:
            msg = ''
            while 1:
                msg = self.message_queue.get()
                if (msg == 'stop'):
                    break
                else:
                    sys.stdout.write("\n<[Message from a child thread: %s]>\n" % msg)
        except KeyboardInterrupt:
            print("Keyboard Interrupt")

        self.running = False

    def __waiting_display(self):
        message = ''
        while message != 'stop':
            sys.stdout.write('.')
            sys.stdout.flush()
            try:
                message = self.queue_to_display.get(True, 0.5)
            except queue.Empty:
                pass
            except Exception as err:
                sys.stdout.write("\n__waiting_display() exception: %s\n" % str(err))
        self.queue_from_display.put('done')

    def __initialize(self):
        # Enable serial communication with Trillium (Disables UVW control line)
        print("self.serial configuration:")
        print(self.serial)
        print("Initializing connection to Trillium")

        thread = threading.Thread(target=self.__waiting_display)
        thread.daemon = True
        thread.start()

        self.serial.write(b'Tx\r\n')
        ready = False
        response = ''
        remaining = 5.0
        end = time.time() + remaining
        while remaining > 0:
            try:
                ready,_,_ = self.__select([self.serial],[],[],remaining)
                if self.serial.inWaiting():
                    characters = self.serial.read(self.serial.inWaiting())
                    response += characters
                    for character in characters:
                        sys.stdout.write("[%s::%d]" % (character, ord(character)))
                    sys.stdout.flush()
                if len(response) and re.compile('Serial Transmit Enabled[\n][\r]').search(response):
                    self.queue_to_display.put('stop')
                    self.queue_from_display.get()
                    sys.stdout.write("\nInitialization complete\n\n")
                    return True
                remaining = end - time.time()
            except Exception as err:
                self.queue_to_display.put('stop')
                self.queue_from_display.get()
                sys.stdout.write("\nFailed to initialize\n")
                print("Failure due to exception: %s" % str(err))
                return False
        self.queue_to_display.put('stop')
        self.queue_from_display.get()
        sys.stdout.write("\nFailed to initialize\n")
        return False

    def __finalize(self):
        # Disable serial communication with Trillium (Re-enables UVW control line)
        self.serial.write(b'TxOff\r\n')

    def start(self):
        if (not self.serial) or (not self.serial.isOpen()):
            raise Exception("serial interface did not initialize")
        self.running = True

        self.read_thread = threading.Thread(target=self.read_loop)
        self.write_thread = threading.Thread(target=self.write_loop)

        self.read_thread.daemon = True
        self.write_thread.daemon = True

        if self.__initialize():
            self.read_thread.start()
            self.write_thread.start()
            self.__loop()
            self.__finalize()

    def __select (self, iwtd, owtd, ewtd, timeout=None):

        """This is a wrapper around select.select() that ignores signals. If
        select.select raises a select.error exception and errno is an EINTR
        error then it is ignored. Mainly this is used to ignore sigwinch
        (terminal resize). """

        # if select() is interrupted by a signal (errno==EINTR) then
        # we loop back and enter the select() again.
        if timeout is not None:
            end_time = time.time() + timeout
        while True:
            try:
                return select.select (iwtd, owtd, ewtd, timeout)
            except select.error as e:
                if e[0] == errno.EINTR:
                    # if we loop back we have to subtract the amount of time we already waited.
                    if timeout is not None:
                        timeout = end_time - time.time()
                        if timeout < 0:
                            return ([],[],[])
                else: # something else caused the select.error, so this really is an exception
                    raise


    def read_loop(self):
        while self.running:
            try:
                ready,_,_ = self.__select([self.serial],[],[],0.5)
                if self.serial.inWaiting():
                    bytes = self.serial.read(self.serial.inWaiting())
                    for byte in bytes:
                        sys.stdout.write(byte)
                    sys.stdout.flush()
            except Exception as err:
                self.message_queue.put("read thread exception: %s" % str(err))
                self.message_queue.put('stop')

    def _write(self, ch, append=True):
        self.seq = 0
        if append:
            self.line += ch
        self.serial.write(ch.encode())
        if ch == '\r':
            sys.stdout.write('\r\n')
        else:
            sys.stdout.write(ch)
        sys.stdout.flush()

    def write_loop(self):
        self.serial.write(b'\r')
        while self.running:
            try:
                ch = self.factory.getch()
                if ch in ('\r', '\n'):
                    if self.line.strip() == 'quit':
                        # Erase the last four characters written to the serial port 'quit'
                        for i in range(0,4): self.serial.write(chr(8).encode())
                        self.message_queue.put('stop')
                        sys.stdout.write('\r\n')
                        return
                    self.line = ''
                    self._write('\r', False)
                elif ord(ch) == 27:  # Escape (possible start of sequence)
                    self.seq = 1
                elif ord(ch) in (91,79):  # Left Bracket or Letter O (confirmed sequence)
                    if self.seq == 1:
                        self.seq = 2
                    else:
                        self._write(ch)
                elif ord(ch) == 126: # Tilde (end of numeric sequence)
                    if self.seq >= 3:
                        self.seq = 0
                    else:
                        self._write(ch)
                elif ord(ch) in (8,127): # Backspace, Delete (write backspace only)
                    if len(self.line):
                        self.line = self.line[:-1]
                    self._write(chr(8), False)
                elif ord(ch) == 3: # Ctrl + C (exit the e300 terminal)
                    self.message_queue.put('stop')
                    sys.stdout.write('\r\n')
                    return
                elif ord(ch) < 32: # Ignore all remaining control characters
                    self.seq = 0
                else:
                    if (self.seq >= 2):
                        if (self.seq == 2) and (ord(ch) > 57):
                            self.seq = 0
                        elif (47 < ord(ch) < 58):
                            self.seq += 1
                    else:
                        self._write(ch)
            except Exception as err:
                self.message_queue.put("write thread exception: %s" % str(err))
                self.message_queue.put('stop')
                raise


if __name__ == "__main__":

    # Start communication loop, exit on ctrl C
    port = '/dev/ttyS1'
    if len(sys.argv) > 1:
        port = sys.argv[1]
    comm = TrilliumSerialComm(port=port)
    comm.start()
