"""
This module simplifies logging.
"""

try:
    import calendar
    import os
    import sys
    import subprocess
    import time
except Exception as e:
    print("Exception: ", str(e))
    time.sleep(3)
    raise Exception("Logger could not load essential modules. Exception:").with_traceback(str(e))


class Logger:
    def __init__(self, directory='.', prefix='', postfix='', extension='log', note='', yday=False):
        self.log_to_file = True
        self.log_to_screen = False
        self.log_fh = None
        self.note_first = True
        self.note = note
        self.log_debug = False
        self.yday = yday

        self.categories = {
            'dbg': 'DEBUG',
            'debug': 'DEBUG',
            'err': 'ERROR',
            'error': 'ERROR',
            'info': 'INFO',
            'default': 'INFO',
            'warn': 'WARNING',
        }

        if directory == '':
            directory = '.'
        self.log_context = {
            'directory': directory,
            'prefix': prefix,
            'postfix': postfix,
            'extension': extension,
            'date': '',
        }

    # ===== Public Methods =============================================
    def log(self, string, cat_key="default"):
        category = "UNKNOWN"
        screen_fh = sys.stdout
        if cat_key in self.categories:
            category = self.categories[cat_key]

        if category == "ERROR":
            screen_fh = sys.stderr
        elif (category == "DEBUG") and (not self.log_debug):
            return

        if self.yday:
            timestamp_text = time.strftime("%Y,%j-%H:%M:%S", time.gmtime())
            file_date = timestamp_text[0:4] + '_' + timestamp_text[5:8]
        else:
            timestamp_text = time.strftime("%Y/%m/%d-%H:%M:%S", time.gmtime())
            file_date = timestamp_text[0:4] + timestamp_text[5:7] + timestamp_text[8:10]

        text = ""
        if self.note_first:
            if self.note:
                text = "[%s: %s] %s>" % (self.note, timestamp_text, category)
            else:
                text = "[%s] %s>" % (timestamp_text, category)
        else:
            text = "[%s: %s] %s>" % (category, timestamp_text, self.note)
        text += " %s\n" % string.strip('\n')

        if self.log_to_file:
            self._prepare_log(file_date)
            if self.log_fh:
                self.log_fh.write(text)
                self.log_fh.flush()
        if self.log_to_screen:
            screen_fh.write(text)
            screen_fh.flush()

    def set_log_to_file(self, enabled):
        self.log_to_file = enabled

    def set_log_to_screen(self, enabled):
        self.log_to_screen = enabled

    def set_log_debug(self, enabled):
        self.log_debug = enabled

    def set_note_first(self, enabled):
        self.note_first = enabled

    # ===== Internal Methods =============================================
    def _open_log(self):
        if not os.path.exists(self.log_context['directory']):
            os.makedirs(self.log_context['directory'])
        elif not os.path.isdir(self.log_context['directory']):
            raise IOError("Path '%(directory)s' exists, but is not a directory." % self.log_context)
        if self.log_context['extension']:
            log_file_name = "%(directory)s/%(prefix)s%(date)s%(postfix)s.%(extension)s" % self.log_context
        else:
            log_file_name = "%(directory)s/%(prefix)s%(date)s%(postfix)s" % self.log_context
        if os.path.exists(log_file_name) and (not os.path.isfile(log_file_name)):
            raise IOError("Path '%s' exists, but is not a regular file." % log_file_name)
        self.log_fh = open(log_file_name, 'a')

    def _close_log(self):
        if self.log_fh:
            self.log_fh.close()
        self.log_fh = None

    def _prepare_log(self, file_date):
        if (file_date == self.log_context['date']) and self.log_fh:
            return
        else:
            self.log_context['date'] = file_date
            self._close_log()
            self._open_log()
