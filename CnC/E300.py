
import queue
# import re
import threading
import time

import serial

from QLock import QLock, QExLocked
from QTools import qassert, QExMessage

E300_RAW = 0
E300_VELOCITY = 1
E300_ACCELERATION = 2


class E300Thread(threading.Thread):
    def __init__(self, print_method, remind_interval=None):
        threading.Thread.__init__(self)
        self.lock = QLock()
        self.lock.set_name('e300')
        self.lock.set_pid()

        qassert(callable(print_method))

        self.set_remind_interval(remind_interval)

        self.request_queue = queue.Queue(256)
        self.response_queue = queue.Queue(256)
        self.input = E300_RAW
        self.daemon = True
        self.state = 'init'
        self._print = print_method

    def set_remind_interval(self, remind_interval):
        if remind_interval is None:
            remind_interval = 1800
        if type(remind_interval) != int:
            raise TypeError("Interval is wrong type, int expected")
        if remind_interval < 15:
            raise ValueError("Interval is too small: %d" % remind_interval)
        self.__dict__['remind_interval'] = remind_interval

    def get_remind_interval(self):
        return self.__dict__['remind_interval']

    def halt(self):
        self.request_queue.put('halt')

    def error(self, message):
        self.response_queue.put_nowait("ERROR:" + message)
        return False

    def run(self):
        request = 'init'
        new_request = False
        try:
            while request != 'halt':
                if request == 'status':
                    self._print("E300Thread: reporting status to main thread", category='dbg')
                    try:
                        self.response_queue.put_nowait(self.state)
                    except queue.Full:
                        message = "E300Thread: thread response queue is full"
                        self._print(message)
                        raise QExMessage(message)

                    # Make sure we resume what we were doing
                    if self.state in ('connect', 'disconnect'):
                        request = self.state

                if request == 'connect':
                    self.cal_connect()
                    if new_request:
                        try:
                            self.response_queue.put_nowait(self.state)
                        except queue.Full:
                            message = "E300Thread: thread response queue is full"
                            self._print(message)
                            raise QExMessage(message)

                elif request == 'disconnect':
                    self.cal_disconnect()
                    if new_request:
                        try:
                            self.response_queue.put_nowait(self.state)
                        except queue.Full:
                            message = "E300Thread: thread response queue is full"
                            self._print(message)
                            raise QExMessage(message)

                if new_request:
                    try:
                        self.request_queue.task_done()
                    except ValueError:
                        self._print("E300Thread: Called task_done() too many times ", category='warn')

                try:
                    self._print("E300Thread: waiting on next request", category='dbg')
                    new_request = False
                    request = self.request_queue.get(block=True, timeout=self.get_remind_interval())
                    new_request = True
                    self._print("E300Thread: received a request ['%s']" % request, category='dbg')
                except queue.Empty:
                    self._print("E300Thread: repeating previous request", category='dbg')
                    request = self.state

            self.state = 'halt'
            try:
                request = self.response_queue.put_nowait('halt')
            except queue.Full:
                pass
            self.e300_cal(on=False)
        except QExMessage as e:
            return self.error(str(e))

    def cal_connect(self):
        self._print("E300Thread: connecting E300", category='dbg')
        prior_state = self.state
        try:
            self.e300_cal(on=True)
        except:
            self.state = prior_state

    def cal_disconnect(self):
        self._print("E300Thread: disconnecting E300", category='dbg')
        prior_state = self.state
        try:
            self.e300_cal(on=False)
        except:
            self.state = prior_state

    def e300_cal(self, on=True):
        try:
            self.lock.acquire()
        except QExLocked as e:
            message = "The E300 serial port is locked by another process [%s]. Calibration halted." % str(e)
            self._print(message, category='err')
            raise QExMessage(message)
        try:
            # login to the Terminal server
            # self._print("E300Thread: opening serial port")
            serial_conn = serial.Serial('/dev/ttyS1')

            if on:
                search_str = 'EXT CAL Input+/- Connected Directly'
                if self.input == E300_VELOCITY:
                    search_str = 'EXT CAL Input+/- Velocity Equivalent Source Connected'
                elif self.input == E300_ACCELERATION:
                    search_str = 'EXT CAL Input+/- Acceleration Equivalent Source Connected'

                max_attempts = 5
                attempt = 0
                started = False

                while (not started) and (attempt < max_attempts):
                    attempt += 1

                    # self._print("E300Thread: making sure we are at the MAIN menu")
                    serial_conn.write(bytes('RETURN\r'.encode('ascii')))
                    serial_conn.write(bytes('RETURN\r'.encode('ascii')))
                    # self._print("E300Thread: E300 returned:[%s]" % serial_conn.read(serial_conn.inWaiting()))

                    # self._print("E300Thread: enabling system controls")
                    serial_conn.write(bytes('ENABLEABCDEF'.encode('ascii')))
                    serial_conn.write(bytes('ENABLEABCDEF'.encode('ascii')))
                    # self._print("E300Thread: E300 returned:[%s]" % serial_conn.read(serial_conn.inWaiting()))

                    # self._print("E300Thread: changing to calibration menu")
                    serial_conn.write(bytes('CALIBRATE\r'.encode('ascii')))
                    serial_conn.write(bytes('CALIBRATE\r'.encode('ascii')))
                    # self._print("E300Thread: E300 returned:[%s]" % serial_conn.read(serial_conn.inWaiting()))

                    # self._print("E300Thread: connecting external calibration source")
                    if self.input == E300_VELOCITY:
                        serial_conn.write(bytes('EXTCALVELOCITY\r'.encode('ascii')))
                        serial_conn.write(bytes('EXTCALVELOCITY\r'.encode('ascii')))
                    elif self.input == E300_ACCELERATION:
                        serial_conn.write(bytes('EXTCALACCELERATION\r'.encode('ascii')))
                        serial_conn.write(bytes('EXTCALACCELERATION\r'.encode('ascii')))
                    else:
                        serial_conn.write(bytes('EXTCALCONNECT\r'.encode('ascii')))
                        serial_conn.write(bytes('EXTCALCONNECT\r'.encode('ascii')))

                    # self._print("E300Thread: DUDE ... 3")
                    # self._print("E300Thread: verifying ready for cals")
                    # regex = re.compile('EXT CAL Input[+][/][-] Connected Directly.+?[:]CALIBRATE[>]', re.S)
                    # regex = re.compile('EXT CAL Input[+][/][-] Connected Directly')
                    # self._print("E300Thread: verification regex compiled")
                    message = ''
                    for i in range(0, 50):
                        time.sleep(0.1)
                        # self._print("E300Thread: bytes in waiting = %d" % serial_conn.inWaiting())
                        if serial_conn.inWaiting():
                            message += "%s" % serial_conn.read(serial_conn.inWaiting())
                            if message.find(search_str) >= 0:
                                self._print("E300Thread: External Cal. Input Connected.")
                                self.state = 'connect'
                                started = True
                                break

                    if not started:
                        self._print("E300Thread: Cal. connect attempt %d of %d failed." % (attempt, max_attempts))

                if not started:
                    self._print("E300Thread: External Cal. Input NOT Connected.")
                    self.state = 'disconnect'
                    serial_conn.close()
                    del serial_conn
                    message = "Failed to remind E300"
                    self._print(message, category='err')
                    raise Exception(message)

            else:
                self._print("E300Thread: switching to SAFE mode", category='dbg')
                serial_conn.write(bytes('SAFE\r'.encode('ascii')))
                serial_conn.write(bytes('SAFE\r'.encode('ascii')))
                # self._print("E300Thread: E300 returned:[%s]" % serial_conn.read(serial_conn.inWaiting()))

            try:
                serial_conn.close()
                del serial_conn
            except:
                pass
            serial_conn = None
        except serial.SerialException as e:
            try:
                msg = str(e)
            except:
                msg = "unknown"
            message = "E300Thread: serial port error: %s" % msg
            self._print(message, category='err')
            self.response_queue.put(message)
            raise QExMessage(message)
        except Exception as e:
            try:
                msg = str(e)
            except:
                msg = 'Unknown'
            self._print("E300Thread: communication issue: %s" % msg, category='err')
            self.response_queue.put(message)
            self.lock.release()
            raise
        self.lock.release()
