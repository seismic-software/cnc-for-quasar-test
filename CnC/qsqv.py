
from QQuickView import QQuickView
from QTools import assert_arg_count, QExArgs
from QScript import QScript


class QSQuickView(QScript):
    def __init__(self):
        self.option_list = [
            # optparse.make_option("-n", "--now", action="store_true", dest="now"),
            # optparse.make_option("-e", "--e300", action="store_true", dest="e300"),
        ]

        QScript.__init__(self)

    def local_usage(self):
        return """usage: %prog [options] -- <device> <channel>

device              : integer value identifying the Q330
channel             : channels to activate (1 - 6)"""

    def _run(self):
        assert_arg_count(self.args, 'eq', 2)

        arg_device_id = self.args[0]
        self.comm.set_from_file(arg_device_id, self.options.reg_file)

        arg_channels = self.args[1]
        action = QQuickView()

        if self.options.verbosity:
            action.verbosity = self.options.verbosity
        try:
            action.channels = arg_channels
        except TypeError as msg:
            raise QExArgs("invalid type for argument '%s'" % msg.__str__())
        except ValueError as msg:
            raise QExArgs("invalid value for argument '%s'" % msg.__str__())

        self.comm.register()
        try:
            self.comm.execute(action)
        finally:
            self.comm.deregister()


script = QSQuickView()
script.run()
