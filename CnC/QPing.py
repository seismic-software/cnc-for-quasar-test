
from QAction import QAction
from QStatus import formatPingInfo, formatted
from QTools import verify_status_type, QExMessage, q330_to_unix_time, fancy_integer
from Quasar.Commands import c1_ping
from Quasar.Status import StatusBits, getBitmapFromKeys, getKeysFromBitmap, StatusDict

import calendar
import time


class QPing(QAction):
    def __init__(self, device_type='Q330', show_restricted=False, show_unavailable=False):
        self._raw_results = {}
        self._device_type = device_type
        self._show_restricted = show_restricted
        self._show_unavailable = show_restricted
        self._restricted_status = [
            'PowerSupply',
            'Thread',
        ]
        self._bitmapped_status = {
            'AuxiliaryBoard': 2,
            'DynamicIP': 1,
            'EnvironmentalProcessor': 6,
            'SerialSensor': 4,
        }
        self._unsupported_status = []
        self.private = [
            'action',
            'status'
        ]
        QAction.__init__(self)

    def get_raw_results(self):
        return self._raw_results

    def get_raw_result(self, key):
        result = None
        if key in self._raw_results:
            result = self._raw_results[key]
        return result

    def is_restricted(self, status):
        return status in self._restricted_status

    def remove_restricted(self, statuses):
        pruned = []
        for status in statuses:
            if status not in self._restricted_status:
                pruned.append(status)
        return pruned

    def val_action(self, value):
        if type(value) != str:
            raise TypeError('action')
        if value not in ['detect', 'info', 'status', 'monitor']:
            raise ValueError('action')
        return value

    def val_status(self, value):
        n_value = []
        if type(value) != list:
            raise TypeError('status')
        for status in value:
            type_list = list(StatusBits[self._device_type].keys())
            if status == "all":
                n_value = type_list
                if not self._show_restricted:
                    n_value = self.remove_restricted(n_value)
                break
            else:
                restrictions = self._restricted_status
                if self._show_restricted:
                    restrictions = []
                r_status = verify_status_type(status, type_list, exclusions=restrictions)
                if (r_status is None) and (status != 'all'):
                    raise ValueError('status')
                # if self.is_restricted(r_status):
                #    raise ValueError('status')
            n_value.extend(r_status)
        value = n_value
        return value

    def _execute(self, q330):
        method_name = '_exec_' + self.action
        getattr(self, method_name)(q330)

    def _exec_detect(self, q330):
        ping = c1_ping.c1_ping()
        time_start = time.time() * 1000.0
        response = q330.sendCommand(ping, 0)
        time_end = time.time() * 1000.0
        if isinstance(response, c1_ping.c1_ping) and (response.getType() == 1):
            self._print("Ping received a response. Delay was %.02f ms" % (time_end - time_start))
        else:
            raise QExMessage("Ping failed")

    def _exec_info(self, q330):
        ping_result = self._ping_info(q330)
        if ping_result is not None:
            sub_dict = formatPingInfo(ping_result)
            values = []
            for sub_key in list(sub_dict.keys()):
                values.append(sub_dict[sub_key])
            values.sort()
            main_title = "Ping Info"
            self._print("%s:" % main_title)
            self._print("".rjust(len(main_title), "="))
            max_title_len = max(list(map(len, [x[1] for x in values])))
            for (idx, title, value) in values:
                self._print("  %s: %s" % (title.rjust(max_title_len), value))
            self._add_result(sub_dict)
        else:
            raise QExMessage("Ping failed")

    def _ping_info(self, q330):
        result = None
        ping = c1_ping.c1_ping(type_value=4)
        response = q330.sendCommand(ping, 0)
        if isinstance(response, c1_ping.c1_ping) and (response.getType() == 5):
            result = response.asDictionary()
            self._raw_results['ping_info'] = result
        return result

    def _ping_status(self, q330):
        result = None
        ping = c1_ping.c1_ping(type_value=2)
        ping.setStatusRequestBitmap(0)
        response = q330.sendCommand(ping, 0)
        if isinstance(response, c1_ping.c1_ping) and (response.getType() == 3):
            result = response.asDictionary()
            self._raw_results['ping_status'] = result
        return result

    def _batch_ping(self, q330, status_types):
        include_ping_data = False
        include_ping_info = False
        bitmap_checks = []

        results = {}
        self.remaining = {}
        for status in status_types:
            if status == 'ping_status':
                include_ping_data = True
            elif status == 'ping_info':
                include_ping_info = True
            else:
                if status in self._bitmapped_status:
                    include_ping_info = True
                    bitmap_checks.append(status)
                self.remaining[status] = 1

        if include_ping_info:
            results['ping_info'] = self._ping_info(q330)

        self._unsupported_status = []
        if len(bitmap_checks):
            bitmap = results['ping_info']['Flags']
            for key in bitmap_checks:
                if (bitmap & (0x00000001 << self._bitmapped_status[key])) == 0:
                    if not self._show_unavailable:
                        del self.remaining[key]
                    self._unsupported_status.append(key)

        if len(self._unsupported_status):
            self._print("The following status are not available:")
            for s in self._unsupported_status:
                self._print("  %s" % s)
            self._print()

        while len(list(self.remaining.keys())):
            ping = c1_ping.c1_ping(type_value=2)
            request_keys = list(self.remaining.keys())
            bitmap = getBitmapFromKeys(request_keys, q330.getDeviceType())
            ping.setStatusRequestBitmap(bitmap)

            responses = q330.sendCommand(ping, receiveMultiple=1)
            for response in responses:
                status_types = []
                if response.getType() == 3:
                    status_types = getKeysFromBitmap(response.getStatusBitmap(), q330.getDeviceType())
                if include_ping_data:
                    result = response.asDictionary()
                    results['ping_status'] = result
                    self._raw_results['ping_status'] = result
                for status_type in status_types:
                    if status_type in self.remaining:
                        results[status_type] = response.getStatusClass().asDictionary()
                        del self.remaining[status_type]
        return results

    def _exec_status(self, q330):
        if self.status[0] == 'all':
            self.status = sorted(StatusBits[q330.getDeviceType()].keys())
        all_results = self._batch_ping(q330, self.status)
        for status_type in sorted(self.status):
            if status_type not in all_results:
                continue
            result_dict = all_results[status_type]
            # Store the raw results
            self._raw_results[status_type] = result_dict
            # Fomat the results
            results = formatted({status_type: result_dict})
            key = status_type
            main_title = f"{key} Status"
            self._print("%s:" % main_title)
            self._print("".rjust(len(main_title), "="))
            sub_dict = results[key]
            values = []
            for sub_key in list(sub_dict.keys()):
                values.append(sub_dict[sub_key])
            values.sort()
            max_title_len = max(list(map(len, [x[1] for x in values])))
            for (idx, title, value) in values:
                self._print("  %s: %s" % (title.rjust(max_title_len), value))
            self._print()
            self._add_result(results[key])

    def _exec_monitor(self, q330):
        status_types = ['ping_info',
                        'ping_status',
                        'Global',
                        'PLL',
                        'BoomPosition',
                        'DataPort1',
                        'DataPort2',
                        'DataPort3',
                        'DataPort4']

        # Collect all of the status information needed in order
        # to construct our results. This way all of our data 
        # is available at once, which is useful considering that
        # some of the information for sections in the print-out
        # are pulled from multiple status types.
        #
        # _batch_ping uses the minimum necessary number of request
        results = self._batch_ping(q330, status_types)

        # Ping Info Stats
        if 'ping_info' in results:
            result = results['ping_info']
            # Tag ID
            if 'KMIPropertyTag' in result and 'SystemSoftwareVersion' in result:
                version = result['SystemSoftwareVersion']
                version_major = version >> 8
                version_minor = version & 0x00ff
                self._print("        Tag ID: %d" % result['KMIPropertyTag'])
                self._print("      Firmware: %d.%d" % (version_major, version_minor))
        # Ping Status Stats
        if 'ping_status' in results:
            result = results['ping_status']
            # Time of Last Reboot
            if 'TimeOfLastReboot' in result:
                time_reboot = q330_to_unix_time(result['TimeOfLastReboot'])
                time_now = float(calendar.timegm(time.gmtime()))
                uptime = (time_now - time_reboot) / 86400.0
                self._print("     Boot Time: %s UTC (%.02f days uptime)" % (time.strftime("%Y/%m/%d %H:%M", time.gmtime(time_reboot)), uptime))
        # Global Stats
        if 'Global' in results:
            result = StatusDict(results['Global'], prefix='Global_')
            # Station Time
            if 'SecondsOffset' in result and 'USecOffset' in result and 'CurrentDataSequenceNumber' in result:
                seconds = result['SecondsOffset'] + result['CurrentDataSequenceNumber']
                usecs = result['USecOffset']
                # time_now = float(calendar.timegm(time.gmtime()))
                time_station = q330_to_unix_time(seconds)
                self._print("      GPS Time: %s.%06d" % (time.strftime("%Y/%m/%d %H:%M:%S", time.gmtime(time_station)), usecs))
            # Clock Quality
            if 'ClockQuality' in result:
                clock_quality = result['ClockQuality']
                result_string = ""
                if ((clock_quality & 0xc0) >> 6) == 1:
                    result_string += "PLL Hold"
                elif ((clock_quality & 0xc0) >> 6) == 2:
                    result_string += "PLL Tracking"
                elif ((clock_quality & 0xc0) >> 6) == 3:
                    result_string += "PLL Locked"
                else:
                    result_string += "PLL not enabled"
                if not (clock_quality & 0x01):
                    result_string += " (Clock has never been locked)"
                elif clock_quality & 0x20:
                    result_string += " (Speculative Lock based on internal clock)"
                elif clock_quality & 0x10:
                    result_string += " (Timemarks currently frozen due to filtering)"
                elif clock_quality & 0x04:
                    result_string += " (Clock has 3-D lock)"
                elif clock_quality & 0x02:
                    result_string += " (Clock has 2-D lock)"
                elif clock_quality & 0x08:
                    result_string += " (Clock has 1-D lock)"
                else:
                    result_string += " (Clock has been locked)"
                self._print(" Clock Quality: %s" % result_string)
        # PLL Stats
        if 'PLL' in results:
            result = StatusDict(results['PLL'], prefix='PLL_')
            # Phase
            if 'TimeError' in result:
                time_error = result['TimeError']
                result_string = "%dus" % int(time_error * 1000000)
                self._print("         Phase: %s" % result_string)
        # Boom Position Stats
        if 'BoomPosition' in results:
            result = StatusDict(results['BoomPosition'], prefix='BoomPosition_')
            # System Temperature
            if 'SystemTemperature' in result:
                self._print("   Temperature: %d C" % result['SystemTemperature'])
            # System Power
            if 'InputPower' in result and 'MainCurrent' in result:
                volts = result['InputPower'] * 0.15
                amps = result['MainCurrent'] / 1000.0
                watts = volts * amps
                self._print("   Input Power: %.02f VDC, %.02f W" % (volts, watts))
            if 'MainCurrent' in result:
                pass
            # Boom Positions
            for i in range(1, 7):
                key = "Channel%dBoom" % i
                position = "Unknown"
                if key in result:
                    position = "%d" % result[key]
                result_string = "        Boom %d: %s" % (i, position)
                self._print(result_string)

        # Data Port Stats
        if 'ping_info' in results:
            info = results['ping_info']
            data_ports = {}
            max_capacity_len = 0
            max_percent_len = 0
            max_packets_len = 0
            for i in range(1, 5):
                data_ports[i] = {}
                dp_info = data_ports[i]
                key = "DataPort%d" % i
                port_key = "DataPort%dPacketMemorySize" % i
                if key in results and port_key in info:
                    result = StatusDict(results[key], prefix=key+'_')
                    buffer_bytes = info[port_key]
                    buffer_capacity = buffer_bytes / 1048576.0
                    buffer_capacity_str = "%.02f" % buffer_capacity
                    max_capacity_len = max(len(buffer_capacity_str), max_capacity_len)
                    dp_info['capacity'] = buffer_capacity_str
                    # Buffer Percent Full
                    if 'BytesOfPacketCurrentlyUsed' in result:
                        buffer_used = float(result['BytesOfPacketCurrentlyUsed'])
                        buffer_percent = (buffer_used / buffer_bytes) * 100.0
                        buffer_percent_str = "%.02f%%" % buffer_percent
                        max_percent_len = max(len(buffer_percent_str), max_percent_len)
                        dp_info['percent'] = buffer_percent_str
                    # Packet Counts
                    if 'TotalDataPacketsSent' in result:
                        packet_count_str = fancy_integer(result['TotalDataPacketsSent'])
                        max_packets_len = max(len(packet_count_str), max_packets_len)
                        dp_info['packets'] = packet_count_str

            for i in range(1, 5):
                dp_info = data_ports[i]
                data_port_str = "   Data Port %d:" % i
                if 'capacity' in dp_info:
                    data_port_str += " %s MiB" % dp_info['capacity'].rjust(max_capacity_len)
                if 'percent' in dp_info:
                    data_port_str += " (%s full)." % dp_info['percent'].rjust(max_percent_len)
                if 'packets' in dp_info:
                    data_port_str += " Packets sent: %s" % dp_info['packets'].rjust(max_packets_len)

                self._print(data_port_str)

        self._add_result(results)
