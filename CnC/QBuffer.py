
import sys


class QBuffer:
    def __init__(self, err=False, console=True):
        self.clear()
        self._err = err
        self._console = console

    def __str__(self):
        return self.get_out()

    def get_err(self):
        return self._buffer_err

    def get_out(self):
        return self._buffer_out

    def clear(self):
        self.clear_err()
        self.clear_out()

    def clear_err(self):
        self._buffer_err = ""

    def clear_out(self):
        self._buffer_out = ""

    def write(self, string, console=None, err=None):
        if string is None:
            string = ""
        if console is None:
            console = self._console
        if err is None:
            err = self._err
        if string[-1] != '\n':
            string += '\n'
        if self._err:
            self._buffer_err += string
            if console:
                sys.stderr.write(string)
                sys.stderr.flush()
        else:
            self._buffer_out += string
            if console:
                sys.stdout.write(string)
                sys.stdout.flush()
