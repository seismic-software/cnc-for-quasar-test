
from QAction import QAction
from QTools import QExMessage
from Quasar.Commands.c1_cack import c1_cack


class QGps(QAction):
    def __init__(self):
        self.private = ['action']
        QAction.__init__(self)

    def val_action(self, value):
        if type(value) != str:
            raise TypeError('action')
        if value not in ('on', 'off', 'coldstart'):
            raise ValueError('action')
        return value

    def _execute(self, q330):
        if self.action == 'on':
            response = q330.turnOnGPS()
            if not isinstance(response, c1_cack):
                raise QExMessage("Failed to Power On GPS")
            self._print("Powering On GPS")
        elif self.action == 'off':
            response = q330.turnOffGPS()
            if not isinstance(response, c1_cack):
                raise QExMessage("Failed to Power Off GPS")
            self._print("Powering Off GPS")
        elif self.action == 'coldstart':
            response = q330.coldStartGPS()
            if not isinstance(response, c1_cack):
                raise QExMessage("GPS Coldstart Failed")
            self._print("GPS Coldstart Initialized")
