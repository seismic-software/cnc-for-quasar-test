
import os
import re

from QTools import QExMessage, test_dir

LOCK_SUPPORT = False
try:
    from lock_file import LockError, LockFile
    LOCK_SUPPORT = True
except:
    pass

LOCK_DIRECTORY = ''
if 'CNC_LOCK_DIRECTORY' in os.environ and test_dir(os.environ['CNC_LOCK_DIRECTORY'], 'w'):
    LOCK_DIRECTORY = os.environ['CNC_LOCK_DIRECTORY']
if not test_dir(LOCK_DIRECTORY, 'w'):
    LOCK_DIRECTORY = '/opt/util/var/lock'
if not test_dir(LOCK_DIRECTORY, 'w'):
    LOCK_DIRECTORY = '/home/jdedwards/quasar/trunk/util/var/lock'
if not test_dir(LOCK_DIRECTORY, 'w'):
    LOCK_DIRECTORY = '.'


class QExLocked(Exception):
    """signals that the lock could not be acquired"""


class QLock:
    def __init__(self, name=None):
        self.lock_dir = LOCK_DIRECTORY
        self.lock_path = None
        self.lock_pid = None
        self.lock = None
        if name:
            self.set_name(name)

    def set_directory(self, dir):
        if test_dir(dir, 'w'):
            self.lock_dir = dir
            return True
        return False

    def get_directory(self):
        return self.lock_dir

    def set_name(self, name):
        if type(name) != str:
            raise TypeError("string required")
        if not len(name):
            raise ValueError("empty string is invalid")
        self.lock_path = self.lock_dir + "/LCK.." + name

    def set_pid(self, pid=None):
        if pid is not None:
            if type(pid) != int:
                raise TypeError("integer required")
            if pid < 2:
                raise ValueError("PID must be an integer greater than 1")
            self.lock_pid = pid
        else:
            self.lock_pid = os.getpid()

    def find_proc(self, pid, name_reg=None):
        cmd = 'ps x -o pid,args'
        processes = dict([l for l in [s.strip().split(' ', 1) for s in os.popen(cmd).read().split('\n')] if len(l) == 2])

        if str(pid) in processes:
            if (name_reg is None) or re.compile(name_reg).search(processes[str(pid)]):
                raise ValueError(processes[str(pid)])

    def get_proc_id(self):
        pid = 0
        fd = os.open(self.lock_path, os.O_RDONLY)
        pid_str = os.read(fd, 11)
        os.close(fd)
        pid_str.decode().strip('\n')
        pid = int(pid_str)
        return pid

    def acquire(self, final=False):
        if not self.lock_path:
            raise ValueError("No path specified for lock file")
        if not self.lock_pid:
            raise ValueError("No PID set for lock file")

        if LOCK_SUPPORT:
            self._acquire(final=final)
        else:
            self._unsafe_acquire(final=final)

    def _unsafe_acquire(self, final=False):
        if os.path.exists(self.lock_path):
            raise QExLocked(-1)
        try:
            fh = open(self.lock_path, "w+")
            fh.write("-1")
            fh.close()
        except:
            raise Exception("-1")

    def _acquire(self, final=False):
        try:
            self.lock = LockFile(self.lock_path)
        except LockError as e:
            try:
                if final:
                    raise Exception("final round failed, giving up")
                pid = self.get_proc_id()
                try:
                    self.find_proc(pid)
                except ValueError as e:
                    raise Exception(str(pid))
                os.remove(self.path)
                self.acquire(self, final=True)
            except Exception as e:
                raise QExLocked(e)
        os.write(self.lock.fileno(), "%010d\n".encode() % self.lock_pid)

    def release(self):
        if LOCK_SUPPORT:
            self._release()
        else:
            self._unsafe_release()

    def _unsafe_release(self):
        if not os.path.exists(self.lock_path):
            raise QExMessage("The lock file is already released")
        else:
            self.lock = None

    def _release(self):
        if self.lock is None:
            raise QExMessage("The lock file is already released")
        try:
            self.lock.release()
            self.lock = None
        except ValueError as e:
            raise QExMessage(e.__str__())
