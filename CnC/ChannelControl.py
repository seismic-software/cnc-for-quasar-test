
from QClass import QClass


class ChannelControl(QClass):
    def __init__(self, group):
        self.private = [
            'group',
            'station',
            'location',
            'channel',
            'context',
            'state'
        ]
        QClass.__init__(self)
        self.group = group

    def val_group(self, value):
        if type(value) != str:
            raise TypeError('group')
        if (len(value) < 1) or (not value.isalnum()):
            raise ValueError('group')
        return value

    def val_station(self, value):
        if type(value) != str:
            raise TypeError('station')
        if len(value) > 5:
            raise ValueError('station')
        if (len(value) > 0) and (not value.isalnum()):
            raise ValueError('station')
        return value

    def val_location(self, value):
        if type(value) != str:
            raise TypeError('location')
        if len(value) > 2:
            raise ValueError('location')
        if (len(value) > 0) and (not value.isalnum()):
            raise ValueError('location')
        return value

    def val_channel(self, value):
        if type(value) != str:
            raise TypeError('channel')
        if len(value) > 3:
            raise ValueError('channel')
        if (len(value) > 0) and (not value.isalnum()):
            raise ValueError('channel')
        return value

    def val_context(self, value):
        if type(value) != str:
            raise TypeError('context')
        if value not in ('ARCHIVE', 'IDA'):
            raise ValueError('context')
        return value

    def val_state(self, value):
        if type(value) != str:
            raise TypeError('state')
        if value not in ('DEFAULT', 'OFF', 'ON'):
            raise ValueError('state')
        return value

    def command(self, id):
        return "CHANNELCONTROL-%s-%s-%s-%s-%s-%s" % (str(id), self.station, self.location, self.channel, self.context, self.state)

    def key(self):
        return "%s-%s-%s-%s-%s-%s" % (self.group, self.station, self.location, self.channel, self.context, self.state)

    def __eq__(self, other):
        return self.key() == other.key()

    def __ne__(self, other):
        return self.key() != other.key()
