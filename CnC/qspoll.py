
import optparse

from QPoll import QPoll
from QTools import assert_arg_count
from QScript import QScript


class QSPoll(QScript):
    def __init__(self):
        self.option_list = [
            optparse.make_option("-a", "--address", type="string", action="store", dest="address"),
            optparse.make_option("-T", "--target-host", type="string", action="store", dest="target_host"),
            optparse.make_option("-p", "--port", type="int", action="store", dest="port"),
        ]

        QScript.__init__(self)

    def local_usage(self):
        return """usage: %prog [options]"""

    def _run(self):
        assert_arg_count(self.args, 'eq', 0)

        target_address = None
        if self.options.address:
            target_address = self.options.address

        target_host = '255.255.255.255'
        if self.options.target_host:
            target_host = self.options.target_host
        port = 5330
        if self.options.port:
            port = self.options.port
        self.comm.set_address(target_host)
        self.comm.set_port(port)
        action = QPoll()
        self.comm.execute(action)

        for response in action.get_result_data():
            (ip, port) = response.getSourceIPAddressInfo()
            if not target_address or (target_address == ip):
                print("Q330 [%s]:" % str(response.getKMIPropertyTag()))
                print("    S/N:  %X" % response.getSerialNumber())
                print("    IP:   %s" % ip)
                print("    Port: %s" % port)
                print("")


script = QSPoll()
script.run()
