
from QAction import QAction
from Quasar.Tokens.TokenStructs import TokenStruct

TypeMaps = {
    "Q330": {
        0: "Flash Memory",
        1: "Configuration Memory for Data Port 1",
        2: "Configuration Memory for Data Port 2",
        3: "Configuration Memory for Data Port 3",
        4: "Configuration Memory for Data Port 4",
        5: "Web Page Memory",
        10: "Slave Processor EEPROM",
        11: "Slave Processor PIC Internal Memory",
        12: "Clock Chip RAM",
        13: "Calibrator PIC Internal Memory",
        14: "QAPCHP EEPROM",
        15: "Packet Buffer Memory",
        16: "DSP Program Memory",
        17: "DSP Data Memory"
    },
    "Q335": {
        1: "Configuration Memory for Data Port 1",
        2: "Configuration Memory for Data Port 2",
        3: "Configuration Memory for Data Port 3",
        4: "Configuration Memory for Data Port 4",
        5: "Web Page Memory",
        6: "Core Processor RAM",
        7: "Packet Memory",
        8: "Clock Chip",
        9: "Front-End Board 1 RAM",
        10: "Front-End Board 2 RAM",
    }
}

least = lambda a, b: ((a > b) and [b] or [a])[0]


class QConfig(QAction):
    def __init__(self, device_type='Q330'):
        self._device_type = device_type
        self.private = [
            'action',
            'type',
            'offset',
            'bytes'
        ]
        QAction.__init__(self)

        self.offset = 0
        self.bytes = 8192  # 8 KiB

        self.info = None
        self.get_info = False

        self.type_map = TypeMaps[self._device_type]

    def just_get_info(self, enabled):
        self.get_info = enabled == True

    def get_info(self):
        return self.info

    def val_action(self, value):
        if type(value) != str:
            raise TypeError('action')
        if value not in ['read', 'write']:
            raise ValueError('action')
        return value

    def val_type(self, value):
        try:
            value = int(value)
        except:
            raise TypeError('type')
        if value not in list(self.type_map.keys()):
            raise ValueError('type')
        return value

    def val_offset(self, value):
        try:
            value = int(value)
        except:
            raise TypeError('offset')
        if value < 0:
            raise ValueError('offset')
        return value

    def val_bytes(self, value):
        try:
            value = int(value)
        except:
            raise TypeError('bytes')
        if value < 0:
            raise ValueError('bytes')
        return value

    def _execute(self, q330):
        if self.action == 'read':
            if self.get_info:
                self.info = {'type': self.type, 'tokens': []}
            if self.type in (1, 2, 3, 4):
                token_set = q330.getTokenSet(self.type)
                for token in token_set.getTokens():
                    if self.get_info:
                        self.info['tokens'].append(TokenStruct(token.getTokenBytes()))
                    else:
                        print(TokenStruct(token.getTokenBytes()))
            elif self.type == 5:
                web_page = q330.getWebPage()
                if self.get_info:
                    self.info['WebPage'] = web_page
                else:
                    print("===== Q330 Web Page ==============================")
                    print(web_page, "\n")
            else:
                print("Reading from '%s' not supported yet." % self.type_map[self.type])
        elif self.action == 'write':
            self._print("Um, yeah...\nI don't think you really want to modify Q330 memory, so I refuse. Sorry.")
        self._print("Done.")
