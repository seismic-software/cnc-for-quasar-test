
from QAction import QAction
from Quasar.Commands import c2_sbpwr
from Quasar import QDPDevice

baler_map = {
    0: (3, "Ethernet"),
    1: (0, "Serial Port 1"),
    2: (1, "Serial Port 2"),
}


class QBaler(QAction):
    def __init__(self):
        self.private = [
            'interface',
            'action',
            'timeout',
        ]
        QAction.__init__(self)

        self.verbosity = 0

    def val_interface(self, value):
        if type(value) not in (int, str):
            raise TypeError('interface')
        try: 
            value = int(value)
        except:
            raise ValueError('interface')
        if value not in baler_map:
            raise ValueError('interface')
        value = baler_map[value][0]
        return value

    def val_action(self, value):
        if type(value) != str:
            raise TypeError('action')
        if value not in ['off', 'on', 'web']:
            raise ValueError('action')
        return value

    def val_timeout(self, value):
        try:
            value = int(value)
        except:
            raise TypeError('timeout')
        if 1 > value > 4095:
            raise ValueError('timeout')
        return value

    def _execute(self, q330):
        flag_time_map = self.timeout & 0x0fff

        if self.action == 'on':
            flag_time_map |= 0x1 << 12
            message = 'Turning baler power on for Q330 access...'
        elif self.action == 'web':
            flag_time_map |= 0x4 << 12
            message = 'Turning baler power on for web access...'
        else:  # 'off'
            flag_time_map |= 0xb << 12
            message = 'Turning baler power off...'

        if self.verbosity > 0:
            print("interface code = 0x%04x" % self.interface)
            print("flag + timeout = 0x%04x" % flag_time_map)

        ctrl = c2_sbpwr.c2_sbpwr()
        ctrl.setPhysicalInterface(self.interface)
        ctrl.setFlagAndTimeout(flag_time_map)

        try:
            print(message, end=' ')
            q330.sendCommand(ctrl, 0)
            print("[Success]")
        except QDPDevice.QDP_C1_CERR as e:
            code = e.getErrorCode()
            print("[FAILED]")
            print("    C2_SBPWR received C1_CERR [%d]: %s" % (code, str(e)))
            raise
