
import getpass
import os
import re
import time

from QAction import QAction
from QSupplement import QSupplement
from QTools import QExMessage, QExArgs
from Config import raw_to_config
from Store import StoreFile, StoreError, StoreMySQL, StoreSQLite
from Quasar.Cloning import Profile
from Quasar.Commands import c1_ctrl
from Quasar import QDPDevice


class QClone(QAction):
    def __init__(self):
        self.private = [
            'action',
            'file',
            'config',
            'subset',
        ]
        QAction.__init__(self)

        self.mysql_password = ''
        self.verbosity = 0
        self.confirm = True
        self.config = ('', '')
        self.tries = 3
        self.write_base_port = False
        self.write_ethernet_ip_address = False
        self.write_ethernet_mac_address = False
        self.write_slave_processor_parameters = False
        self.write_web_page = False

    def val_action(self, value):
        if type(value) != str:
            raise TypeError('action')
        if value not in ['read', 'write', 'edit']:
            raise ValueError('action')
        return value

    def val_file(self, value):
        if type(value) != str:
            raise TypeError('file')
        return value

    def val_config(self, value):
        if type(value) not in (tuple, list):
            raise TypeError('config')
        if len(value) != 2:
            raise TypeError('config')
        t, d = tuple(value)
        if type(t) != str:
            raise TypeError('config')
        if type(d) != str:
            raise TypeError('config')
        if t not in ('', 'text', 'file', 'sqlite', 'mysql'):
            raise ValueError('config')
        return t, d

    def val_subset(self, value):
        if value is None:
            return value
        if type(value) not in (tuple, list):
            raise TypeError('subset')
        if len(value) < 1:
            raise TypeError('subset')
        valid_indices = [i[0] for i in list(Profile.section_map.values())]
        for v in value:
            try:
                if int(v) not in valid_indices:
                    raise ValueError('subset')
            except:
                raise ValueError('subset')
        return value

    def _execute(self, q330):

        if self.action == 'read':
            if os.path.isfile(self.file):
                confirmation = 'M'
                first = True
                while confirmation.lower() not in ('y', 'n', ''):
                    if first:
                        first = False
                    else:
                        print("Please enter 'y' or 'n'")
                    confirmation = input("%s: file exists. Do you wish to overwrite it? (y/N): " % os.path.abspath(self.file))
                if confirmation.lower() != 'y':
                    print("Clone operation aborted.")
                    return
            elif os.path.exists(self.file):
                print("%s: path exists. Please select a different path." % os.path.abspath(self.file))
                return
            profile = Profile.Profile()
            profile.verbosity = self.verbosity
            profile.tries = self.tries
            profile.subset = self.subset
            print("Reading configuration from Q330...")
            profile.readFromQ330(q330)
            # print "====================== PROFILE ========================="
            # print(profile)
            print("Writing configuration to file...")
            Profile.writeProfileToFile(profile, fileName=self.file)
            if self.verbosity is not None:
                print(profile)
                with open('ccc.ccc', 'w') as fout:
                    fout.write(str(profile))
            print("Done.")
        elif self.action in ('write', 'edit'):
            if self.action == 'edit':
                print("Reading configuration from Q330...")
                profile = Profile.Profile()
                profile.readFromQ330(q330)
            else:
                print("Reading configuration from file...")
                profile = Profile.getProfileFromFile(self.file)
                with open('ccc.ccc', 'w') as fout:
                    fout.write(str(profile))
            profile.verbosity = self.verbosity
            profile.tries = self.tries
            profile.subset = self.subset
            profile.write_base_port = self.write_base_port
            profile.write_ethernet_ip_address = self.write_ethernet_ip_address
            profile.write_ethernet_mac_address = self.write_ethernet_mac_address
            profile.write_slave_processor_parameters = self.write_slave_processor_parameters
            profile.write_web_page = self.write_web_page

            # Modify the profile if supplemental configuration was supplied
            config_type, config_info = self.config
            supplement = {}
            if config_type == 'text':
                print("Parsing supplemental configuration from string.")
                supplement = raw_to_config(config_info)
            elif config_type == 'file':
                print(
                    f"Reading supplemental configuration from file '{config_info}'.")
                try:
                    store = StoreFile(config_info)
                    raw = store.read()
                except IOError as e:
                    raise QExMessage(
                        f"Error parsing supplemental configuration: {e}")
                supplement = raw_to_config(raw)
            elif config_type == 'sqlite':
                # 'file?station'
                parts = config_info.rsplit('?', 1)
                if len(parts) != 2:
                    raise QExArgs("Invalid specifier for SQLite database")
                file, station = parts
                if len(file) < 1:
                    raise QExArgs("Invalid specifier for SQLite database")
                if len(station) < 1:
                    raise QExArgs("Invalid specifier for SQLite database")

                if not os.path.exists(file):
                    raise QExMessage("%s: path does not exist" % file)
                if not os.path.isfile(file):
                    raise QExMessage("%s: path is not a regular file" % file)
                print("Acquiring supplemental configuration from SQLite database '%s'." % file)
                try:
                    store = StoreSQLite(file)
                    raw = store.read("SELECT config FROM configurations WHERE station=? ORDER BY timestamp DESC LIMIT 1", (station,))[0][0]
                except StoreError as e:
                    raise QExMessage(
                        f"Error retrieving SQLite supplemental configuration: {e}")
                supplement = raw_to_config(raw, b64=True)
            elif config_type == 'mysql':
                # '[user@]host[:port]/database?station'
                user = ''
                port = '3306'

                parts = config_info.rsplit('?', 1)
                if len(parts) != 2:
                    raise QExArgs("Invalid specifier for MySQL database")
                db, station = parts
                if len(station) < 1:
                    raise QExArgs("Invalid specifier for MySQL database")

                parts = db.rsplit('/', 1)
                if len(parts) != 2:
                    raise QExArgs("Invalid specifier for MySQL database")
                conn, database = parts
                if len(database) < 1:
                    raise QExArgs("Invalid specifier for MySQL database")

                parts = conn.split('@', 1)
                if len(parts) == 2:
                    user, addr = parts
                    if len(user) < 1:
                        raise QExArgs("Invalid specifier for MySQL database")
                else:
                    addr = conn
                    
                parts = addr.split(':', 1)
                if len(parts) == 2:
                    host, port = parts
                    if len(port) < 1:
                        raise QExArgs("Invalid specifier for MySQL database")
                else:
                    host = addr
                if len(host) < 1:
                    raise QExArgs("Invalid specifier for MySQL database")

                if user == '':
                    user = input('Username: ')
                
                password = self.mysql_password
                if password == '':
                    password = getpass.getpass('Password: ')

                try:
                    store = StoreMySQL(host, database, user=user, password=password, port=int(port))
                    raw = store.read("SELECT config FROM configurations WHERE station=%s ORDER BY timestamp DESC LIMIT 1", (station,))[0][0]
                except StoreError as e:
                    raise QExMessage(
                        f"Error retrieving MySQL supplemental configuration: {e}")
                supplement = raw_to_config(raw, b64=True)

            if self.verbosity > 1:
                print(supplement)
            modified_indices = []
            if len(supplement):
                print("Applying supplemental configuration...")
                modified_indices = QSupplement(supplement, self.verbosity, config_type, config_info).apply(profile)

            if self.verbosity > 2:
                print(profile)

            if self.action == 'edit':
                if len(modified_indices) < 1:
                    print("No modifications.")
                    return
                else:
                    # Intersection of the two lists
                    if profile.subset is None:
                        profile.subset = modified_indices
                    else:
                        profile.subset = list(set(modified_indices) & set(profile.subset))

            first = True
            confirmation = 'M'
            if not self.confirm:
                confirmation = 'yes'
            if self.action == 'edit':
                confirmation_message = "Do you really want to modify the Q330's configuration? (yes/No): "
            else:
                confirmation_message = "Do you really want to write this configuration to the Q330? (yes/No): "
            while confirmation.lower() not in ('yes', 'no', ''):
                if first:
                    first = False
                else:
                    print("Please type 'yes' or 'no'")
                confirmation = input(confirmation_message)
            if confirmation.lower() != 'yes':
                print("You have chosen wisely.")
                return

            self._write_to_q330(profile, q330)

    def _write_to_q330(self, profile, q330):
        keep_trying = 1
        print("Writing configuration to Q330...")
        while keep_trying:
            keep_trying -= 1
            try:
                profile.writeToQ330(q330)
                break
            except Exception as e:
                if not re.compile('Memory operation in progress').search(str(e)):
                    print("Caught exception while trying to write configuration.")
                    print(f"Exception: {e}")
                    raise
        # Save to EEPROM
        control_flags = 1
        ctrl = c1_ctrl.c1_ctrl()
        ctrl.setFlags(control_flags)
        tries = self.tries
        count = 0
        backoff = 1.0
        max_backoff = 60.0
        while tries: 
            tries -= 1
            count += 1
            try:
                try:
                    print("  try #%d: Saving Changes to EEPROM (C1_CTRL)" % (count,), end=' ')
                    q330.sendCommand(ctrl, 0)
                    print("[Success]")
                    tries = 0
                except QDPDevice.QDP_C1_CERR as e:
                    code = e.getErrorCode()
                    print("[FAILED]")
                    print("    C1_CTRL received C1_CERR [%d]: %s" % (code, str(e)))
                    if code in (8, 12):
                        if tries > 0:
                            time.sleep(backoff)
                            backoff = backoff * 2
                            if backoff > max_backoff:
                                backoff = max_backoff
                            continue
                    raise
            except Exception as e:
                raise QExMessage("Caught exception while trying to save to EEPROM. Changes to EEPROM will not be retained upon reboot...\n    Details: %s" % str(e))
            print()

        print("Configuration written, saved to EEPROM. Reboot Q330 to apply.")
