import queue
import optparse
import time

from QScript import QScript
from QTools import assert_arg_count, QExArgs, QExMessage
from E300 import E300_RAW, E300_VELOCITY, E300_ACCELERATION, E300Thread
from QCal import QCal
from QPing import QPing
from QCalStop import QCalStop
from Quasar.QDPDevice import TimeoutException

start_check_period = 5.0  # wait 5 seconds between checks
max_start_checks = 120  # try no more than 120 times (10 minutes)

stop_check_period = 5.0  # wait 5 seconds between checks
max_stop_checks = 12  # try no more than 12 times (1 minute)


class QSCal(QScript):
    def __init__(self):
        self.option_list = [
            optparse.make_option("-e", "--e300-raw", action="store_true", dest="e300_raw",
                                 help="E300 sends the raw signals through."),
            optparse.make_option("--e300-acceleration", action="store_true", dest="e300_acceleration",
                                 help="E300 adjusts input signals with respect to acceleration."),
            optparse.make_option("--e300-velocity", action="store_true", dest="e300_velocity",
                                 help="E300 adjusts input signals with respect to velocity."),
            optparse.make_option("--e300-remind-interval", action="store", dest="e300_remind_interval",
                                 help='E300 will be "reminded" to stay connected every INTERVAL seconds (default is 1800).',
                                 metavar="INTERVAL", type="int"),
            optparse.make_option("--capacitive-coupling", action="store_true", dest="capacitive",
                                 help="Runs a capacitive calibration instead of a resistive calibration"),
            optparse.make_option("-n", "--now", action="store_true", dest="now",
                                 help="Run the calibration immediately instead of scheduling it at the next minute mark."),
        ]

        QScript.__init__(self)

    def local_usage(self):
        return """usage: %prog [options] -- <device> <channels> <cal_monitor> <amplitude> <settle> <duration> <trailer> <wave_form> [wave_form_args]

usage: %prog [options] <device> status|stop


device              : integer value identifying the Q330
channels            : channels to activate (comma/hyphen seperated list in range 1-6)
cal_monitor         : calibration monitor (input) channel
                      (comma/hyphen separated list in range 1-6, 0 alone for none)
amplitude           : decibels [-42 to -6]
settle[s|m|h]       : time between enabling any sensor control lines
                      or relays and when the calibration signal starts
                      (max: 16383s, 273m, 4h)
duration[s|m|h]     : length of cal in seconds
                      (max: 16383s, 273m, 4h)
trailer[s|m|h]      : time after the calibration signal ends before"
                      any sensor control lines or relays are
                      restored to normal
                      (max: 16383s, 273m, 4h)
wave_form           : Q330: random, sine, step,
                      Q335: random, sine, step, timing
wave_form_args      : random <frequency> [noise]
                      sine <frequency>
                      step [polarity] 
  frequency (rand)  : Q330:
                        select frequencies in range [0.5 - 125.0]
                        OR divider (125 / divider) [1 - 255]d
                      Q335:
                        select frequencies in range [0.5 - 5000.0]
                        OR divider (125 / divider) [1 - 255]d
                        OR multiplier (125 * multiplier) [1 - 20]m
  noise (rand)      : 'red', 'white' or 'none' (default is 'none')
  frequency (sine)  : Q330: 
                        select frequencies in range [0.004 - 1.0]
                        OR period (1 / period) [1 - 255]p
                      Q335:
                        select frequencies in range [0.004 - 20.0]
                        OR period (1 / period) [1 - 255]p
                        OR multiplier (1 * multiplier) [1 - 20]m
  polarity (step)   : negative | positive"""

    def _print(self, string=None, category="info", to_stdout=True):
        QScript._print(self, string, to_stdout)

    def _print_ignore(self, string=None, category="info", to_stdout=False):
        pass

    def _run(self):
        # Check for the 'status' or 'stop' commands
        if (len(self.args) == 2) and (self.args[1] in ('status', 'stop')):
            try:
                arg_device_id = self.args[0]
            except:
                raise QExArgs("invalid type for argument 'device'")
            if not self.comm.set_from_file(arg_device_id, self.options.reg_file):
                raise QExArgs("invalid entry for Q330 #%d in configuration" % int(arg_device_id))

            if self.args[1] == 'stop':
                self._run_stop()
            else:
                self._run_status()
        else:
            self._run_start()

    def _run_stop(self):
        action = QCalStop()

        self.comm.register()
        self.comm.execute(action)
        self.comm.deregister()

    def _run_status(self):
        ping = QPing(device_type=self.comm.get_device_type())
        ping.suppress_stdout()
        ping.action = 'status'
        ping.status = ['Global']
        wait_count = 0

        self.comm.execute(ping)

        raw_cal_info = ping.get_raw_result("Global")
        from Quasar.Status import StatusDict
        result = StatusDict(raw_cal_info, prefix="Global_")
        cal_bitmap = result['CalibratorStatus']
        coil_value = "Disabled"
        signal_value = ""
        if cal_bitmap & 0x01:
            coil_value = "Enabled"
            signal_value = "  Signal:"
            if cal_bitmap & 0x04:
                signal_value += "Error"
            elif cal_bitmap & 0x02:
                signal_value += "On"
            else:
                signal_value += "Off"

        self._print("Calibration:%s%s" % (coil_value, signal_value))

    def _run_start(self):
        assert_arg_count(self.args, 'gte', 9)
        assert_arg_count(self.args, 'lte', 10)

        try:
            int(self.args[0])
            arg_device_id = self.args[0]
        except:
            raise QExArgs("invalid type for argument 'device'")
        if not self.comm.set_from_file(arg_device_id, self.options.reg_file):
            raise QExArgs("invalid entry for Q330 #%d in configuration" % int(arg_device_id))

        e300_type = None
        if self.options.e300_raw:
            if e300_type:
                raise QExArgs("You may only specify one of: --e300-raw | --e300-velocity | --e300-acceleration")
            e300_type = E300_RAW

        if self.options.e300_velocity:
            if e300_type:
                raise QExArgs("You may only specify one of: --e300-raw | --e300-velocity | --e300-acceleration")
            e300_type = E300_VELOCITY

        if self.options.e300_acceleration:
            if e300_type:
                raise QExArgs("You may only specify one of: --e300-raw | --e300-velocity | --e300-acceleration")
            e300_type = E300_ACCELERATION

        e300_remind_interval = None
        if self.options.e300_remind_interval:
            e300_remind_interval = self.options.e300_remind_interval

        try:
            arg_wave_form = self.args[7]
            action = QCal(arg_wave_form)
            action.channels = self.args[1]
            action.cal_monitor = self.args[2]
            action.amplitude = self.args[3]
            action.settle = self.args[4]
            action.duration = self.args[5]
            action.trailer = self.args[6]
            # self.args[10] <-- wave_form; set on QCal class instantiation
            if action.wave_form == 'sine':
                assert_arg_count(self.args, 'eq', 9)
                action.frequency = self.args[8]
            elif action.wave_form == 'step':
                assert_arg_count(self.args, 'eq', 9)
                action.polarity = self.args[8]
            elif action.wave_form == 'random':
                assert_arg_count(self.args, 'eq', 10)
                action.frequency = self.args[8]
                action.noise = self.args[9]
            action.coupling = 'R'
            if self.options.capacitive:
                action.coupling = 'C'
        except TypeError as msg:
            raise QExArgs("invalid type for argument '%s'" % msg.__str__())
        except ValueError as msg:
            raise QExArgs("invalid value for argument '%s'" % msg.__str__())

        total_duration = action.duration + action.settle + action.trailer

        if self.options.now:
            action.auto_timing = False
        if e300_type is not None:
            action.e300 = True
            action.e300_input = e300_type

        ping = QPing(device_type=self.comm.get_device_type())
        ping.suppress_stdout()
        ping.action = 'status'
        ping.status = ['Global']

        if action.e300:
            if self.options.verbosity > 0:
                self._threads['E300'] = E300Thread(self._print, remind_interval=e300_remind_interval)
            else:
                self._threads['E300'] = E300Thread(self._print_ignore, e300_remind_interval)
            if 'E300' not in self._threads or not self._threads['E300']:
                message = "Failed to create E300 thread. Autocal aborted"
                self._print(message)
                raise QExMessage(message)
            self._threads['E300'].input = action.e300_input
            self._threads['E300'].start()

        self.comm.register()
        self.comm.execute(action)
        self.comm.deregister()

        if action.e300:
            # Verify that the cal started, and record the start time
            wait_count = 0
            while 1:
                comm_timeout = False
                try:
                    self.comm.execute(ping)
                    cal_stat = ping.get_result_data()[0]['CalibratorStatus'][2]
                    station_time = ping.get_result_data()[0]['StationTime'][2]
                except TimeoutException as e:
                    comm_timeout = True

                if wait_count > max_start_checks:
                    message = "Calibration failed to start, max wait (%d seconds) exceeded" % (
                                start_check_period * max_start_checks)
                    raise QExMessage(message)
                if comm_timeout:
                    self._print("Timeout on start check %d" % (wait_count + 1,))
                elif (len(cal_stat) >= 7) and (cal_stat[0:7] == 'Enabled'):
                    self._print("Calibration has started, expected duration is %d seconds" % total_duration)
                    break
                time.sleep(start_check_period)
                wait_count += 1

            # This cal. involves the E300
            self._print("Requesting E300 connect")
            try:
                self._threads['E300'].request_queue.put_nowait('connect')
                connect_response = self._threads['E300'].response_queue.get()
                if connect_response != 'connect':
                    message = "E300 connect failed, aborting cal."
                    raise QExMessage(message)
                else:
                    self._print("E300 is connected")
                self._threads['E300'].response_queue.task_done()
            except queue.Full:
                message = "E300 thread request queue is full"
                raise QExMessage(message)
            except queue.Empty:
                message = "E300 thread response queue is empty"
                raise QExMessage(message)
            except ValueError:
                self._print("Called task_done() too many times on E300 thread response queue")

            # Wait until this cal has finished so we can tell the E300 to disconnect
            try:
                self._print("Waiting for calibration to complete...")
                e300_msg = self._threads['E300'].response_queue.get(block=True, timeout=total_duration)
                if e300_msg == 'halt':
                    raise QExMessage("E300 thread halted unexpectedly!")
                elif e300_msg.startswith('ERROR:'):
                    raise QExMessage("Error message from E300 thread: \"%s\"" % e300_msg.split(':')[1])
                else:
                    raise QExMessage("Unexpected message from E300 thread: \"%s\"" % e300_msg)
            except queue.Empty:
                pass

            # Verify that the cal has ended, and record the end time
            wait_count = 0
            while 1 and (self.comm is not None):
                comm_timeout = False
                try:
                    self.comm.execute(ping)
                    cal_stat = ping.get_result_data()[0]['CalibratorStatus'][2]
                    station_time = ping.get_result_data()[0]['StationTime'][2]
                except TimeoutException as e:
                    comm_timeout = True

                if wait_count > max_stop_checks:
                    message = "Could not confirm end of calibration, max wait (%d seconds) exceeded" % (
                    stop_check_period * max_stop_checks,)
                    self._print(message)
                    break
                if comm_timeout:
                    self._print("Timeout on calibration stop check %d" % (wait_count + 1))
                elif (len(cal_stat) >= 8) and (cal_stat[0:8] == 'Disabled'):
                    self._print("Calibration is complete")
                    break
                time.sleep(stop_check_period)
                wait_count += 1

            # Make sure the E300 thread is done
            if action.e300 and 'E300' in self._threads and self._threads['E300'] and self._threads['E300'].is_alive():
                try:
                    self._print("Halting the E300 thread.")
                    self._threads['E300'].request_queue.put_nowait('halt')
                    self._print("Waiting for E300 thread to finish...")
                    self._threads['E300'].join(60)
                    del self._threads['E300']
                except:
                    pass
                self._print("E300 thread is finished.")


script = QSCal()
script.run()
