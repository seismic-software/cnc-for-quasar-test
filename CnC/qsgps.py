
from QTools import assert_arg_count, QExArgs
from QScript import QScript
from QGps import QGps


class QSGps(QScript):
    def __init__(self):
        QScript.__init__(self)

    def local_usage(self):
        return """usage: %prog [options] <device> <action>

device:  integer value identifying the Q330
action:  what to do (on, off, coldstart)"""

    def _run(self):
        assert_arg_count(self.args, 'eq', 2)

        try:
            arg_device_id = self.args[0]
        except:
            raise QExArgs("invalid type for argument 'device'")
        if not self.comm.set_from_file(arg_device_id, self.options.reg_file):
            raise QExArgs("invalid entry for Q330 #%d in configuration" % int(arg_device_id))

        action = QGps()
        try:
            action.action = self.args[1]
        except TypeError as msg:
            raise QExArgs("invalid type for argument '%s'" % msg.__str__())
        except ValueError as msg:
            raise QExArgs("invalid value for argument '%s'" % msg.__str__())

        self.comm.register()
        self.comm.execute(action)
        self.comm.deregister()


script = QSGps()
script.run()
