import fnmatch
import getpass
import os
import re

from Store import StoreFile, StoreError, StoreSQLite, StoreMySQL
from QTools import QExMessage, encode_user_tag, parse_mac, verify_int, parse_ip, parse_bool
from Quasar.Tokens import TokenSet, Token, TokenStructs

rate_map = {
    1: 0,
    10: 1,
    20: 2,
    40: 3,
    50: 4,
    100: 5,
    200: 6,
}


class QSupplement:
    def __init__(self, supplement=None, verbosity=0, config_type='', config_info=''):
        self.supplement = supplement
        self.verbosity = verbosity
        self.config = (config_type, config_info)

    # there used to be some methods here for writing this data to either a
    # database or file, which were removed as they were not usable, due to
    # being out-of-date with the rest of the codebase.
    # It appears (famous last words, I know) that these operations are managed
    # in QClone and thus wouldn't be used here anyway, probably why the issues
    # with them went undetected for so long.

    def verify(self):
        self._supplement(None, False)

    def apply(self, profile):
        return self._supplement(profile, True)

    def _supplement(self, profile, apply):
        if self.supplement is None:
            QExMessage("No Supplement has been Supplied")

        # Track the modified indices (for the 'edit' option)
        modified = {}  # MODIFIED INDEX
        supplement = self.supplement

        # ---- C1_SGLOB ----
        if 'User_Tag' in supplement:
            tag = supplement['User_Tag']
            try:
                encoded_tag = encode_user_tag(tag)
            except ValueError as e:
                raise QExMessage("Supplemental Key '%s' Error: invalid user tag '%s'" % ("User_Tag", tag))
            print("User Tag: '%s' [%d]" % (tag, encoded_tag))
            if apply:
                section = profile.getGlob()
                if section:
                    section.setUserTag(encoded_tag)

        # ---- C2_SGPS ----
        if 'GPS_Power_Mode' in supplement:
            mode_map = {
                'CONTINUOUS': 0,
                'MAX_TIME': 1,
                'MAX_TIME_OR_PLL_LOCK': 2,
                'MAX_TIME_OR_GPS_LOCK': 3,
            }
            mode = supplement['GPS_Power_Mode'].upper()
            if mode not in mode_map:
                raise QExMessage(
                    "Supplement Key '%s' Error: invalid GPS power mode '%s'" % ('GPS_Power_Mode', mode))
            if apply:
                if profile.getGps():
                    profile.getGps().setPowerCycleMode(mode_map[mode])
                    modified[profile.getIndexOfGps()] = True  # MODIFIED INDEX
        if 'GPS_Max_On_Time' in supplement:
            try:
                max_time = int(supplement['GPS_Max_On_Time'])
            except ValueError as e:
                raise QExMessage("Supplement Key '%s' Error: invalid value '%s'" % ('GPS_Max_On_Time', max_time))
            if apply:
                if profile.getGps():
                    profile.getGps().setGpsMaximumOnTime(max_time)
                    modified[profile.getIndexOfGps()] = True  # MODIFIED INDEX

        for p in ('S1', 'S2', 'S3', 'Eth'):
            port_name = {'S1': 'Ser1', 'S2': 'Ser2', 'S3': 'Ser3', 'Eth': 'Ether'}[p]
            port_index = {'S1': 0, 'S2': 1, 'S3': 2, 'Eth': 3}[p]
            interface_name = {'S1': 'SerialInterface1',
                              'S2': 'SerialInterface1',
                              'S3': 'SerialInterface1',
                              'Eth': 'Ethernet'}[p]
            ip_key = p + "_IP_Address"
            dp_key = p + "_Data_Port"
            poc_key = p + "_POC_IP_Address"
            icmp_key = p + "_Block_Non_POC_ICMP"
            tcp_key = p + "_Block_Non_POC_TCP"
            qdp_key = p + "_Block_Non_POC_QDP"
            baler_key = p + "_Baler_Power_Mode"

            # ---- C1_SPHY ----
            if p == 'Eth':
                mac_key = "Eth_MAC_Address"
                if mac_key in supplement:
                    try:
                        mac_h_32, mac_l_16 = parse_mac(supplement[mac_key])
                        if apply:
                            if profile.getPhy():
                                section = profile.getPhy()
                                section.setEthernetMACAddressHigh32(mac_h_32)
                                section.setEthernetMACAddressLow16(mac_l_16)
                                modified[profile.getIndexOfPhy()] = True  # MODIFIED INDEX
                    except ValueError as e:
                        raise QExMessage("Supplement Key '%s' Error: %s" % (mac_key, e))

                bp_key = "Base_Port"
                if bp_key in supplement:
                    try:
                        verify_int(supplement[bp_key], 1, 65430)
                        base_port = int(supplement[bp_key])
                        if apply:
                            if profile.getPhy():
                                section = profile.getPhy()
                                section.setBasePort(base_port)
                                modified[profile.getIndexOfPhy()] = True  # MODIFIED INDEX
                    except ValueError as e:
                        raise QExMessage("Supplement Key '%s' Error: %s" % (bp_key, e))
                    except AssertionError as e:
                        raise QExMessage("Supplement Key '%s' Error: %s" % (bp_key, e))

            if ip_key in supplement:
                try:
                    ip = parse_ip(supplement[ip_key])
                    if apply:
                        if profile.getPhy():
                            section = profile.getPhy()
                            section.getSetFunction(interface_name + "IPAddress")(ip)
                            modified[profile.getIndexOfPhy()] = True  # MODIFIED INDEX
                except ValueError as e:
                    raise QExMessage("Supplement Key '%s' Error: %s" % (ip_key, e))
            # set interface flags
            flag_changes = {}
            if icmp_key in supplement:
                try:
                    flag_changes['icmp'] = parse_bool(supplement[icmp_key], 'YN')
                except ValueError as e:
                    raise QExMessage("Supplement Key '%s' Error: %s" % (icmp_key, e))
            if tcp_key in supplement:
                try:
                    flag_changes['tcp'] = parse_bool(supplement[tcp_key], 'YN')
                except ValueError as e:
                    raise QExMessage("Supplement Key '%s' Error: %s" % (tcp_key, e))
            if qdp_key in supplement:
                try:
                    flag_changes['qdp'] = parse_bool(supplement[qdp_key], 'YN')
                except ValueError as e:
                    raise QExMessage("Supplement Key '%s' Error: %s" % (qdp_key, e))

            if len(list(flag_changes.keys())) and apply:
                section = profile.getPhy()
                if section:
                    flags = section.getGetFunction(interface_name + "Flags")()
                    if 'icmp' in flag_changes:
                        if flag_changes['icmp']:
                            flags |= 0x0010
                        else:
                            flags &= 0xffef
                    if 'tcp' in flag_changes:
                        if flag_changes['tcp']:
                            flags |= 0x0020
                        else:
                            flags &= 0xffdf
                    if 'qdp' in flag_changes:
                        if flag_changes['qdp']:
                            flags |= 0x0080
                        else:
                            flags &= 0xff7f
                    section.getSetFunction(interface_name + "Flags")(flags)
                    modified[profile.getIndexOfPhy()] = True  # MODIFIED INDEX

            # ---- C2_SPHY ----
            if dp_key in supplement:
                try:
                    temp_value = int(supplement[dp_key])
                    if apply:
                        section = profile.getGetFunction("Phy%s" % port_name)()
                        if section:
                            section.setDataPortNumber(temp_value)
                            modified[profile.getGetFunction("IndexOfPhy%s" % port_name)()] = True  # MODIFIED INDEX
                except ValueError as e:
                    raise QExMessage(f"Supplement Key '{dp_key}' Error: {e}")
            if poc_key in supplement:
                try:
                    ip = parse_ip(supplement[poc_key])
                    if apply:
                        section = profile.getGetFunction("Phy%s" % port_name)()
                        if section:
                            section.setPointOfContactOrBalerIPAddress(ip)
                            modified[profile.getGetFunction("IndexOfPhy%s" % port_name)()] = True  # MODIFIED INDEX
                except ValueError as e:
                    raise QExMessage("Supplement Key '%s' Error: %s" % (poc_key, e))
            if baler_key in supplement:
                mode_map = {
                    'NO_BALER': 0,
                    'DTR_CONTROL': 1,
                    'BREAK_CONTROL': 2,
                    'CONTINUOUS': 3,
                }
                mode = supplement[baler_key].upper()
                if mode not in mode_map:
                    raise QExMessage("Supplement Key '%s' Error: invalid value '%s'" % (baler_key, mode))
                if apply:
                    section = profile.getGetFunction("Phy%s" % port_name)()
                    if section:
                        flags = section.getFlags()
                        flags &= 0xffcf
                        flags |= mode_map[mode] << 4
                        section.setFlags(flags)
                        modified[profile.getGetFunction("IndexOfPhy%s" % port_name)()] = True  # MODIFIED INDEX

        # ---- Sensor Control Mapping ----
        sc_map = {
            'A': {
                'CALIBRATION': 1,
                'CENTERING': 2,
                'CAPACITIVE': 3,
                'LOCK': 7,
                'UNLOCK': 8,
                'AUX1': 9,
                'AUX2': 10,
            },
            'B': {
                'CALIBRATION': 4,
                'CENTERING': 5,
                'CAPACITIVE': 6,
                'LOCK': 11,
                'UNLOCK': 12,
                'AUX1': 13,
                'AUX2': 14,
            }
        }
        for i in range(1, 9):
            key = "Sensor_Control_%d" % i
            if key not in supplement:
                continue
            sc_str = supplement[key]
            definition = 0
            if sc_str.strip() != '':
                parts = [s.strip().upper() for s in sc_str.split(',', 2)]
                if len(parts) != 3:
                    raise QExMessage("Supplement Key '%s' Error: invalid format '%s'" % (key, sc_str))
                sensor, polarity, action = parts
                action_value = 0
                try:
                    action_value = sc_map[sensor][action]
                except KeyError as e:
                    raise QExMessage("Supplement Key '%s' Error: invalid sensor or action '%s'" % (key, sc_str))
                if polarity == 'HIGH':
                    definition = action_value | 0x0100
                elif polarity == 'LOW':
                    definition = action_value
                else:
                    raise QExMessage(
                        "Supplement Key '%s' Error: invalid value for polarity '%s'" % (key, polarity))

            if apply:
                section = profile.getSc()
                if section:
                    section.getSetFunction("SensorOutput%dDefinition" % i)(definition)
                    modified[profile.getIndexOfSc()] = True  # MODIFIED INDEX

        # ---- C1_SLOG ----
        for i in range(1, 5):
            flag_changes = {}
            freeze_output_key = "DP%d_Freeze_Output" % i
            freeze_buffering_key = "DP%d_Freeze_Buffering" % i
            buffer_oldest_key = "DP%d_Buffer_Oldest" % i
            hot_swap_key = "DP%d_Hot_Swap" % i
            base_96_key = "DP%d_Base_96" % i

            if freeze_output_key in supplement:
                try:
                    flag_changes['freeze_output'] = parse_bool(supplement[freeze_output_key], 'YN')
                except ValueError as e:
                    raise QExMessage("Supplement Key '%s' Error: %s" % (freeze_output_key, e))
            if freeze_buffering_key in supplement:
                try:
                    flag_changes['freeze_buffering'] = parse_bool(supplement[freeze_buffering_key], 'YN')
                except ValueError as e:
                    raise QExMessage("Supplement Key '%s' Error: %s" % (freeze_buffering_key, e))
            if buffer_oldest_key in supplement:
                try:
                    flag_changes['buffer_oldest'] = parse_bool(supplement[buffer_oldest_key], 'NF')
                except ValueError as e:
                    raise QExMessage("Supplement Key '%s' Error: %s" % (buffer_oldest_key, e))
            if hot_swap_key in supplement:
                try:
                    flag_changes['hot_swap'] = parse_bool(supplement[hot_swap_key], 'NF')
                except ValueError as e:
                    raise QExMessage("Supplement Key '%s' Error: %s" % (hot_swap_key, e))
            if base_96_key in supplement:
                try:
                    flag_changes['base_96'] = parse_bool(supplement[base_96_key], 'NF')
                except ValueError as e:
                    raise QExMessage("Supplement Key '%s' Error: %s" % (base_96_key, e))

            if len(list(flag_changes.keys())) and apply:
                section = profile.getGetFunction("Log%d" % i)()
                if section:
                    flags = section.getFlags()
                    if 'freeze_output' in flag_changes:
                        if flag_changes['freeze_output']:
                            flags |= 0x0004
                        else:
                            flags &= 0xfffb
                    if 'freeze_buffering' in flag_changes:
                        if flag_changes['freeze_buffering']:
                            flags |= 0x0008
                        else:
                            flags &= 0xfff7
                    if 'buffer_oldest' in flag_changes:
                        if flag_changes['buffer_oldest']:
                            flags |= 0x0010
                        else:
                            flags &= 0xffef
                    if 'hot_swap' in flag_changes:
                        if flag_changes['hot_swap']:
                            flags |= 0x0400
                        else:
                            flags &= 0xfbff
                    if 'base_96' in flag_changes:
                        if flag_changes['base_96']:
                            flags |= 0x8000
                        else:
                            flags &= 0x7fff
                    section.setFlags(flags)
                    modified[profile.getGetFunction("IndexOfLog%d" % i)()] = True  # MODIFIED INDEX

        # ---- Info Channel Configuration ----
        msg_logs = [[], [], [], []]
        time_logs = [[], [], [], []]
        cfg_streams = [[], [], [], []]
        regex_loc = re.compile('^[0-9A-Za-z]{2}$')
        regex_chan = re.compile('^[0-9A-Za-z]{3}$')
        for key in ('MsgLog', 'TimeLog', 'CfgStream'):
            if key == 'MsgLog':
                group = msg_logs
                group_name = "Message Log"
            elif key == 'TimeLog':
                group = time_logs
                group_name = ""
            else:
                group = cfg_streams
                group_name = "Message Log"

            if key in supplement:
                count = 0
                for cfg in supplement[key]:
                    count += 1
                    parts = tuple([s.strip() for s in cfg.split(',')])
                    if len(parts) != 3:
                        raise QExMessage("Invalid %s Config (#%d): %s" % (group_name, count, cfg))
                    dp, loc, chan = parts

                    try:
                        if dp == '':
                            pass
                        elif not (0 < int(dp) < 5):
                            raise ValueError("Invalid Data Port")
                    except:
                        raise QExMessage("Invalid Data Port for %s Config (#%d): %s" % (group_name, count, cfg))

                    if loc == '':
                        pass
                    elif not regex_loc.match(loc):
                        raise QExMessage(
                            "Invalid Location Code for %s Config (#%d): %s" % (group_name, count, cfg))

                    if chan == '':
                        pass
                    elif not regex_chan.match(chan):
                        raise QExMessage(
                            "Invalid Channel Name for %s Config (#%d): %s" % (group_name, count, cfg))

                    if dp == '':
                        for i in range(0, 4):
                            group[i].append(parts[1:])
                    else:
                        group[int(dp) - 1].append(parts[1:])

        # ---- LCQ Definitions ----
        lcqs = []
        if 'LCQ' in supplement:
            lcqs = supplement['LCQ']
        data_ports = [[], [], [], []]
        count = 0
        regex_channel = re.compile('^[0-9A-Za-z?*+]{1,3}$')
        regex_location = re.compile('^[0-9A-Za-z?*+]{1,2}$')
        regex_newloc = re.compile('^[0-9A-Za-z]{2}$')
        regex_newchan = re.compile('^[0-9A-Za-z]{3}$')
        for lcq in lcqs:
            count += 1
            parts = tuple([s.strip() for s in lcq.split(',')])

            if len(parts) != 8:
                raise QExMessage("Invalid LCQ sequence (#%d): %s" % (count, lcq))

            dp, loc, chan, src, rate, tel, newloc, newchan = parts
            try:
                if dp == '':
                    pass
                elif not (0 < int(dp) < 5):
                    raise ValueError("Invalid Data Port")
            except:
                raise QExMessage("Invalid Data Port for LCQ sequence (#%d): %s" % (count, lcq))

            if loc == '':
                pass
            elif not regex_location.match(loc):
                raise QExMessage("Invalid Location Code for LCQ sequence (#%d): %s" % (count, lcq))

            if not regex_channel.match(chan):
                raise QExMessage("Invalid Channel Name for LCQ sequence (#%d): %s" % (count, lcq))

            try:
                if src == '':
                    pass
                elif not (0 < int(src) < 7):
                    raise ValueError("Invalid Source")
            except:
                raise QExMessage("Invalid Source for LCQ sequence (#%d): %s" % (count, lcq))

            try:
                if rate == '':
                    pass
                elif int(rate) not in (1, 10, 20, 40, 50, 100, 200):
                    raise ValueError("Invalid Sample Rate")
            except:
                raise QExMessage("Invalid Sample Rate for LCQ sequence (#%d): %s" % (count, lcq))

            if tel == '':
                pass
            elif tel.lower() not in ('on', 'off'):
                raise QExMessage("Invalid Telemetry State for LCQ sequence (#%d): %s" % (count, lcq))

            if newloc == '':
                pass
            elif not regex_newloc.match(newloc):
                raise QExMessage("Invalid New Location Code for LCQ sequence (#%d): %s" % (count, lcq))

            if newchan == '':
                pass
            elif not regex_newchan.match(newchan):
                raise QExMessage("Invalid New Channel Name for LCQ sequence (#%d): %s" % (count, lcq))

            if dp == '':
                for i in range(0, 4):
                    data_ports[i].append(parts[1:])
            else:
                data_ports[int(dp) - 1].append(parts[1:])

        if apply:
            for i in range(0, 4):
                print("Assembling tokens for Data Port", i + 1)
                token_set = profile.getGetFunction("TokenSet%d" % (i + 1,))()
                if not token_set:
                    continue
                tokens = token_set.getTokens()
                new_tokens = []
                lcqs = data_ports[i]
                msg_log = msg_logs[i]
                time_log = time_logs[i]
                cfg_stream = cfg_streams[i]
                if self.verbosity > 0:
                    print("LCQ Supplement:")
                    for lcq in lcqs:
                        print(" ", lcq)
                for token in tokens:
                    id, length = Token.getTokenIDAndLength(token.getTokenBytes())
                    if id == 2:
                        token_struct = TokenStructs.TokenStruct(token.getTokenBytes())
                        network = token_struct.getNetwork()
                        station = token_struct.getStation()
                        network_key = "DP%d_Network" % (i + 1,)
                        station_key = "DP%d_Station" % (i + 1,)
                        changed = False
                        if network_key in supplement:
                            network = supplement[network_key]
                            token_struct.setNetwork(network.ljust(2, ' '))
                            changed = True
                        if station_key in supplement:
                            station = supplement[station_key]
                            token_struct.setStation(station.ljust(5, ' '))
                            changed = True
                        if changed:
                            new_token = Token.Token(token_struct.packData())
                            new_tokens.append(new_token)
                            if self.verbosity > 1:
                                print("pre:\n", token)
                                print("post:\n", new_token)
                            modified[profile.getGetFunction("IndexOfTokenSet%d" % (i + 1,))()] = True  # MODIFIED INDEX
                        else:
                            new_tokens.append(token)
                        if self.verbosity > 1:
                            print("  adding token of type", id)
                            print("    Network:", network)
                            print("    Station:", station)
                    elif id == 7:
                        token_struct = TokenStructs.TokenStruct(token.getTokenBytes())

                        msg_loc = token_struct.getMessageLogSeedLocation()
                        msg_chan = token_struct.getMessageLogSeedName()
                        for cfg in msg_log:
                            loc, chan = cfg
                            if msg_loc != loc:
                                msg_loc = loc
                            if msg_chan != chan:
                                msg_chan = chan

                        time_loc = token_struct.getTimingLogSeedLocation()
                        time_chan = token_struct.getTimingLogSeedName()
                        for cfg in time_log:
                            loc, chan = cfg
                            if time_loc != loc:
                                time_loc = loc
                            if time_chan != chan:
                                time_chan = chan

                        changed = False
                        if token_struct.getMessageLogSeedLocation() != msg_loc:
                            token_struct.setMessageLogSeedLocation(msg_loc)
                            changed = True
                        if token_struct.getMessageLogSeedName() != msg_chan:
                            token_struct.setMessageLogSeedName(msg_chan)
                            changed = True
                        if token_struct.getTimingLogSeedLocation() != time_loc:
                            token_struct.setTimingLogSeedLocation(time_loc)
                            changed = True
                        if token_struct.getTimingLogSeedName() != time_chan:
                            token_struct.setTimingLogSeedName(time_chan)
                            changed = True

                        if changed:
                            new_token = Token.Token(token_struct.packData())
                            new_tokens.append(new_token)
                            if self.verbosity > 1:
                                print("pre:\n", token)
                                print("post:\n", new_token)
                            modified[profile.getGetFunction("IndexOfTokenSet%d" % (i + 1,))()] = True  # MODIFIED INDEX
                        else:
                            new_tokens.append(token)
                    elif id == 8:
                        token_struct = TokenStructs.TokenStruct(token.getTokenBytes())

                        cfg_loc = token_struct.getConfigurationStreamSeedLocation()
                        cfg_chan = token_struct.getConfigurationStreamSeedName()
                        for cfg in cfg_stream:
                            loc, chan = cfg
                            if cfg_loc != loc:
                                cfg_loc = loc
                            if cfg_chan != chan:
                                cfg_chan = chan

                        changed = False
                        if token_struct.getConfigurationStreamSeedLocation() != cfg_loc:
                            token_struct.setConfigurationStreamSeedLocation(cfg_loc)
                            changed = True
                        if token_struct.getConfigurationStreamSeedName() != cfg_chan:
                            token_struct.setConfigurationStreamSeedName(cfg_chan)
                            changed = True

                        if changed:
                            new_token = Token.Token(token_struct.packData())
                            new_tokens.append(new_token)
                            if self.verbosity > 1:
                                print("pre:\n", token)
                                print("post:\n", new_token)
                            modified[profile.getGetFunction("IndexOfTokenSet%d" % (i + 1,))()] = True  # MODIFIED INDEX
                        else:
                            new_tokens.append(token)
                    elif id == 128:
                        token_struct = TokenStructs.TokenStruct(token.getTokenBytes())
                        location = token_struct.getLocationCode().strip()
                        channel = token_struct.getSeedname().strip()
                        # print "%s-%s" % (location, channel)
                        action = "ignoring"
                        # print lcqs
                        matched = False
                        for lcq in lcqs:
                            loc, chan, src, rate, tel, newloc, newchan = lcq

                            if not fnmatch.fnmatch(channel, chan):
                                continue
                            if not fnmatch.fnmatch(location, loc):
                                continue
                            if self.verbosity > 1:
                                print("match on: %s ~= %s && %s ~= %s" % (loc, location, chan, channel))
                            matched = True

                            if (src != '') or (rate != ''):
                                # If the source or the sample rate changed, we need to update both

                                source = token_struct.getSource()
                                if not (0xdf < ((source >> 8) & 0xff) < 0xe6):
                                    raise QExMessage(
                                        "Can only modify rate/source of LCQs which use the main digitizer as their source.")
                                if (source & 0xff) > 6:
                                    raise QExMessage(
                                        "Invalid sample rate found for LCQ #%d" % token_struct.getLCQReferenceNumber())
                                if rate != '':
                                    if self.verbosity > 1:
                                        print("%s-%s setting data rate to %s Hz" % (location, channel, rate))
                                    # print "New Rate: %s Hz" % rate
                                    rate = int(rate)
                                else:
                                    rate = token_struct.getRate()
                                if src != '':
                                    if self.verbosity > 1:
                                        print("%s-%s setting source to channel %s" % (location, channel, src))
                                    src = int(src)
                                    # print "New Source: 0x%4x" % (((0xe0 + int(src) - 1) << 8) | rate_map[token_struct.getRate()],)
                                else:
                                    src = ((token_struct.getSource() >> 8) & 0x07) + 1

                                token_struct.setRate(int(rate))
                                token_struct.setSource(((0xe0 + int(src) - 1) << 8) | rate_map[rate])

                            if tel.lower() in ('on', 'off'):
                                options = token_struct.getOptionBits()
                                if tel.lower() == 'on':
                                    action = "enabling"
                                    options &= 0xfffff7ff
                                else:
                                    action = "disabling"
                                    options |= 0x00000800
                                token_struct.setOptionBits(options)

                            if newloc != '':
                                token_struct.setLocationCode(newloc)
                            if newchan != '':
                                token_struct.setSeedname(newchan)
                            modified[profile.getGetFunction("IndexOfTokenSet%d" % (i + 1,))()] = True  # MODIFIED INDEX

                        new_token = Token.Token(token_struct.packData())
                        new_tokens.append(new_token)
                        if self.verbosity > 1:
                            print("  adding token of type", id)
                            print(" ", action, "lcq", token_struct.getLCQReferenceNumber(), "output")
                            print("pre:\n", token)
                            print("post:\n", new_token)
                    else:
                        if self.verbosity > 1:
                            print("  adding token of type", id)
                        new_tokens.append(token)
                new_set = TokenSet.TokenSet()
                new_set.setMemoryType(token_set.getMemoryType())
                new_set.setTokens(new_tokens)
                new_set.rebuildChunks()
                profile.getSetFunction("TokenSet%d" % (i + 1,))(new_set)

        return modified
