import optparse
import signal
import sys
import threading
import traceback

from QTools import QExMessage, qassert, QExArgs, QExReboot, QException
from Quasar.QDPDevice import TimeoutException, QDP_C1_CERR
from QComm import QComm
from QVersion import version as qversion

"""
This will be used to wrap all general script functionality and simplify
the script creation process...

Usage:
    Class should inherite QScript
    To add more arguments populated 'self.option_list' with desired Option classes
    The '_run()' method must be created
"""


class QScript:
    def __init__(self):
        if qversion:
            self.version = qversion
        else:
            self.version = '?.?.?'
        self._result_buffer = ""
        if self.__class__.__name__ == 'QScript':
            raise NotImplementedError("QScript is an abstract class")

        self.max_tries = 3

        self._suppress_stdout = False
        self._terminating = threading.Lock()
        self._rebooting = False
        self._threads = {}
        self.comm = QComm()
        self.comm.set_max_tries(self.max_tries)

        # Catch signal 15 (SIGTERM) and shut down gracefully
        signal.signal(signal.SIGTERM, self.terminate)

        self.args = None
        self.options = None
        self.parser = None
        if 'usage' not in self.__dict__:
            self.usage = None

        if ('option_list' not in self.__dict__) or (type(self.option_list) != list):
            self.option_list = []
        self.option_list.append(
            optparse.make_option("-b", "--bind-address", type="string", action="store", dest="bind_address",
                                 help="the host/ip and optional port to bind to for this operation (format is host[:port])"))
        self.option_list.append(optparse.make_option("-m", "--max-tries", type="int", action="store", dest="max_tries",
                                                     help="maximum number of tries for each QDP packet (useful for slow connections). Timeout is increased by 50%% for every subsequent try (default=%d)" % self.max_tries))
        self.option_list.append(
            optparse.make_option("-r", "--registration-file", type="string", action="store", dest="reg_file",
                                 help="use this registration file instead of the default"))
        self.option_list.append(optparse.make_option("-t", "--timeout", type="float", action="store", dest="timeout",
                                                     help="specify the connection timeout in seconds"))
        self.option_list.append(optparse.make_option("-v", action="count", dest="verbosity",
                                                     help="specify multiple times to increase verbosity"))

        self.parser = optparse.OptionParser(option_list=self.option_list)
        if hasattr(self, 'local_usage') and callable(getattr(self, 'local_usage')):
            self.parser.set_usage(self.local_usage())
        if hasattr(self, 'local_args') and callable(getattr(self, 'local_args')):
            self.parser.set_description(self.local_args())
        self.options, self.args = self.parser.parse_args()

    def set_log_function(self, log_func):
        self._log = log_func

    def add_option(self, option):
        qassert(type(option) == optparse.Option)
        self.option_list.append(option)

    def print_usage(self, error_msg=None):
        self._print_usage(error_string=error_msg, quit=True)

    def _print_usage(self, error_string=None, quit=False):
        if error_string and error_string != 'too few arguments':
            print("E: %s" % error_string)
        print(f'Version: {self.version}\n')
        self.parser.print_help()
        if quit:
            sys.exit(1)

    def get_result_buffer(self):
        return self._result_buffer

    def log(self, string):
        try:
            self._log(string)
        except:
            pass

    def _print(self, string=None, to_stdout=True):
        if string is None:
            string = ""
        if string != "":
            if string[-1] == "\n":
                string = string[:-1]
        self._result_buffer += string + "\n"
        if to_stdout and not self._suppress_stdout:
            sys.stdout.write(string + "\n")
            sys.stdout.flush()
        self.log(string)

    def suppress_stdout(self, suppress=True):
        self._suppress_stdout = suppress

    def _run(self):
        raise NotImplementedError("'_run' must be overridden by the child class")

    def run(self):
        try:
            if self.options.bind_address:
                self.comm.set_bind_address(self.options.bind_address)
            if self.options.max_tries:
                self.max_tries = self.options.max_tries
                self.comm.set_max_tries(int(self.options.max_tries))
            if self.options.timeout:
                self.comm.set_timeout(float(self.options.timeout))
            if self.options.verbosity:
                self.comm.set_verbosity(self.options.verbosity)
            if self.options.reg_file:
                self.comm.set_file(self.options.reg_file)
            if self.options.verbosity and self.options.verbosity > 0:
                self._print("registration file: %s" % self.comm.get_file())
            self._run()
        except QDP_C1_CERR as e:
            self.log("C1_CERR: %s" % str(e))
            print("C1_CERR: %s" % str(e))
            (ex_f, ex_s, trace) = sys.exc_info()
            traceback.print_tb(trace)
        except TimeoutException as e:
            self.log("Command Timed Out")
            print("Command Timed Out")
            # (ex_f, ex_s, trace) = sys.exc_info()
            # traceback.print_tb(trace)
        except QExMessage as e:
            self.log(str(e))
            print(str(e))
        except QExArgs as e:
            self.print_usage(error_msg=str(e))
        except QExReboot as e:
            self._rebooting = True
            self.log(str(e))
            print(str(e))
        except QException as e:
            self.print_usage(error_msg=str(e))
        except KeyboardInterrupt:
            print()
            pass
        except SystemExit:
            pass
        except Exception as e:
            self.log("[Exception]> %s" % str(e))
            print("[Exception]> %s" % str(e))
            (ex_f, ex_s, trace) = sys.exc_info()
            traceback.print_tb(trace)
        self.terminate()

    def stop_threads(self):
        for k, v in list(self._threads.items()):
            try:
                if v and v.is_alive():
                    v.halt()
                    v.join(60)
                del self._threads[k]
            except:
                pass

    def deregister(self):
        if self.comm and self.comm.is_registered():
            self.comm.deregister()

    def terminate(self, signal=None, frame=None):
        if not self._terminating.acquire(False):
            return
        self.stop_threads()
        if not self._rebooting:
            self.deregister()
        sys.exit(1)
