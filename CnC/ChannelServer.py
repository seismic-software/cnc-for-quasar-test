
from QClass import QClass
from QTools import parse_ip


class ChannelServer(QClass):
    def __init__(self):
        self.private = [
            'ip',
            'port'
        ]
        QClass.__init__(self)

    def val_ip(self, value):
        if type(value) != str:
            raise TypeError('ip')
        try:
            parse_ip(value)
        except ValueError:
            raise ValueError('ip')
        return value

    def val_port(self, value):
        if type(value) not in (str, int):
            raise TypeError('port')
        try:
            value = int(value)
        except:
            raise ValueError('port')
        if 1 > value > 65535:
            raise ValueError('port')
        return value

    def address(self):
        return (self.ip, self.port)

    def __repr__(self):
        return "%s:%d" % self.address()
