
import optparse
import os

from QTools import assert_arg_count, QExArgs
from QScript import QScript
from QModule import QModule


class QSModule(QScript):
    def __init__(self):
        self.option_list = [
            optparse.make_option("-f", "--module-file", action="store", dest="module_file",
                                 help="name of the modu    le file to use for upgrade (will prompt if none is specified)"),
        ]
        QScript.__init__(self)

    def local_usage(self):
        return """usage: %prog [options] <device> <action>

device : integer value identifying the Q330
action : operation to perform 
         - erase-all : remove all modules (reverts to boot block)
         - erase-new : remove the newest modules
         - erase-old : remove all modules older than the running set
         - install   : install a new set of modules
         - list      : list all installed modules
         - running   : list the currently running modules"""

    def _run(self):
        assert_arg_count(self.args, 'eq', 2)

        try:
            arg_device_id = self.args[0]
        except:
            raise QExArgs("invalid type for argument 'device'")
        if not self.comm.set_from_file(arg_device_id, self.options.reg_file):
            raise QExArgs("invalid entry for Q330 #%d in configuration" % int(arg_device_id))

        try:
            action = QModule()
            action.action = self.args[1]

            if self.options.module_file:
                action.module_file = self.options.module_file
            elif action.action == 'install':
                module_file = ""
                while not os.path.isfile(module_file):
                    module_file = input("core module file: ")
                action.module_file = module_file

        except TypeError as msg:
            raise QExArgs("invalid type for argument '%s'" % msg.__str__())
        except ValueError as msg:
            raise QExArgs("invalid value for argument '%s'" % msg.__str__())

        self.comm.register()
        self.comm.execute(action)
        self.comm.deregister()


script = QSModule()
script.run()
