
from QReboot import QReboot
from QTools import assert_arg_count, QExArgs
from QScript import QScript


class QSReboot(QScript):
    def __init__(self):
        QScript.__init__(self)

    def local_usage(self):
        return """usage: %prog [options] <device>

device:  integer value identifying the Q330"""

    def _run(self):
        assert_arg_count(self.args, 'eq', 1)

        try:
            arg_device_id = self.args[0]
        except:
            raise QExArgs("invalid type for argument 'device'")
        if not self.comm.set_from_file(arg_device_id, self.options.reg_file):
            raise QExArgs("invalid entry for Q330 #%d in configuration" % int(arg_device_id))

        action = QReboot()

        self.comm.register()
        self.comm.execute(action)


script = QSReboot()
script.run()
