
from QPulse import QPulse
from QScript import QScript
from QTools import assert_arg_count, QExArgs


class QSUnlock(QScript):
    def __init__(self):
        QScript.__init__(self)

    def local_usage(self):
        return """usage: %prog [options] <device> <sensor> <duration>

device:    integer value identifying the Q330
sensor:    'A' or 'B'; specifies which sensor to unlock
duration:  length of pulse in centiseconds (min 1, max 1000)"""

    def _run(self):
        assert_arg_count(self.args, 'eq', 3)

        try:
            arg_device_id = self.args[0]
        except:
            raise QExArgs("invalid type for argument 'device'")
        if not self.comm.set_from_file(arg_device_id, self.options.reg_file):
            raise QExArgs("invalid entry for Q330 #%d in configuration" % int(arg_device_id))

        action = QPulse()
        try:
            action.action = 'unlock'
            action.sensor = self.args[1]
            action.duration = self.args[2]
        except TypeError as msg:
            raise QExArgs("invalid type for argument '%s'" % msg.__str__())
        except ValueError as msg:
            raise QExArgs("invalid value for argument '%s'" % msg.__str__())

        self.comm.register()
        self.comm.execute(action)
        self.comm.deregister()


script = QSUnlock()
script.run()
