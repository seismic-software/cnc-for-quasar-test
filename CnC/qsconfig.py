
import optparse

from QTools import assert_arg_count, QExArgs

from QScript import QScript
from QConfig import QConfig, TypeMaps


class QSConfig(QScript):
    def __init__(self):
        self.option_list = [
            optparse.make_option("-B", "--bytes", action="store", dest="bytes",
                                 help="number of bytes of memory to read"),
            optparse.make_option("-o", "--offset", action="store", dest="offset",
                                 help="memory offset at which to start reading"),
        ]
        QScript.__init__(self)

    def local_usage(self):
        types = ""
        for key in sorted(TypeMaps[self.comm.get_device_type()].keys()):
            types += "          %2d = %s\n" % (key, TypeMaps[self.comm.get_device_type()][key])
        types = types[:-1]
        return """usage: %%prog [options] <device> <action> <type>

device : integer value identifying the Q330
action : operation to perform ('read', 'write')
type   : memory type to read/write
%s""" % types

    def _run(self):
        assert_arg_count(self.args, 'eq', 3)

        try:
            arg_device_id = self.args[0]
        except:
            raise QExArgs("invalid type for argument 'device'")
        if not self.comm.set_from_file(arg_device_id, self.options.reg_file):
            raise QExArgs("invalid entry for Q330 #%d in configuration" % int(arg_device_id))

        try:
            action = QConfig()
            action.action = self.args[1]
            action.type = self.args[2]
            if self.options.offset:
                action.offset = self.options.offset
            if self.options.bytes:
                action.bytes = self.options.bytes
        except TypeError as msg:
            raise QExArgs("invalid type for argument '%s'" % msg.__str__())
        except ValueError as msg:
            raise QExArgs("invalid value for argument '%s'" % msg.__str__())

        self.comm.register()
        self.comm.execute(action)
        self.comm.deregister()


script = QSConfig()
script.run()
