import asyncio
import queue
import socket
import struct
import threading

from QTools import qassert, Counter


# Writes to itself to force asyncore to re-evaluated file descriptors
# This is necessary both when commands are added to the queue.
class Notifier:
    def __init__(self):
        self.sock_in = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock_in.bind(('', 0))
        self.address = ('127.0.0.1', self.sock_in.getsockname()[1])
        self.sock_out = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def notify(self):
        # sendto expects first param (data to send) to be bytes
        self.sock_out.sendto(b'CHANGED', self.address)

    def get_socket(self):
        return self.sock_in

    async def read(self):
        self.sock_in.connect(self.address)
        receive, _ = \
            asyncio.open_connection(sock=self.sock_in)
        msg = await receive.read(7)
        self.sock_in.close()
        return len(msg)

    def writable(self):
        return False


# Handles queueing of packets before writing and after reading.
# Mirrors Queue class' get and put methods.
class ControlSocket:
    # TODO: this may need better handling of socket open/close
    def __init__(self, address):
        self._recv_queue = queue.Queue()
        self._send_queue = queue.Queue()
        self._address = address
        self._current_packet = ""
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.bind(('', 0))
        self._socket.connect(self._address)

    def getsockname(self):
        return self._socket.getsockname()

    def get_address(self):
        return self._socket.getsockname()

    def get_socket(self):
        return self._socket

    # Add a packet to the transmission queue
    def put(self, packet, block=True, timeout=None):
        result = False
        try:
            self._send_queue.put(packet, block, timeout)
            result = True
        except queue.Full:
            # print "Send Queue is full"
            pass
        return result

    # Request a packet from the response queue
    def get(self, block=True, timeout=None):
        result = None
        try:
            result = self._recv_queue.get(block, timeout)
        except queue.Empty:
            # print "Receive Queue is empty"
            pass
        return result

    def handle_connect(self):
        # print "Connection Established"
        pass

    async def read(self):
        try:
            receive, _ = \
                asyncio.open_connection(sock=self._socket)
            packet = await receive.read(512)
        except socket.error as e:
            print("Socket Error: %s" % str(e))
            return 0
        if not packet:
            return 0
        self._recv_queue.put(packet)
        return len(packet)

    async def handle_write(self):
        try:
            if self._current_packet == "":
                self._current_packet = self._send_queue.get_nowait()
            _, send = \
                asyncio.open_connection(sock=self._socket)
            encoded = self._current_packet.encode()
            bytes_sent = len(encoded)
            send.write(encoded)
            await send.drain()
        except queue.Empty:
            return 0
        except socket.error as e:
            print("Socket Error: %s" % str(e))
            return 0
        return bytes_sent

    def handle_close(self):
        # print "Connection Closed"
        pass

    def writable(self):
        if self._current_packet != "":
            return True
        if not self._send_queue.empty():
            return True
        return False


class ChannelThread(threading.Thread):
    def __init__(self, print_method, server):
        threading.Thread.__init__(self)
        qassert(callable(print_method))

        self.request_queue = queue.Queue(256)
        self.response_queue = queue.Queue(256)
        self.clean_complete = threading.Condition()
        self.cleaning = False
        self.daemon = True
        self.running = False
        self.halting = False

        self._print = print_method

        self.server = server
        self.ctrl_socket = None
        self.notifier = Notifier()

        self.id_counter = Counter(negatives=False, wrap=99999999)
        self.outstanding = {}
        self.applied = {}
        self.clean_requests = {}

    def put(self, packet, block=True, timeout=None):
        result = False
        try:
            self.request_queue.put(packet, block, timeout)
            self.notifier.notify()
            result = True
        except queue.Full:
            # print "Request Queue is full"
            pass
        return result

    def get(self, block=True, timeout=None):
        result = None
        try:
            result = self.response_queue.get(block, timeout)
        except queue.Empty:
            # print "ResponsePacket Queue is empty"
            pass
        return result

    def clean_wait(self, timeout=None):
        self.clean()
        self.clean_complete.acquire()
        self.clean_complete.wait(timeout)

    def clean(self):
        self.request_queue.put(('CLEAN', None))
        self.notifier.notify()

    def is_clean(self):
        return (len(list(self.outstanding.keys())) == 0) and (len(list(self.applied.keys())) == 0) and (
                    (not self.cleaning) or (len(list(self.clean_requests.keys())) == 0))

    def halt_clean(self):
        self.request_queue.put(('CLEAN', None))
        self.halting = True
        self.notifier.notify()

    def halt(self):
        self.request_queue.put(('HALT', None))
        self.notifier.notify()

    def halt_now(self):
        self.running = False
        self.halt()

    def _cmd_to_socket(self, control):
        msg_id = self.id_counter.inc()
        request = control.command(msg_id)
        rq_padded = request.ljust(512, chr(0))
        raw = struct.pack(">%ds" % len(rq_padded), rq_padded)
        try:
            self.ctrl_socket.put(raw, block=False)
            if self.cleaning:
                self.clean_requests[msg_id] = control
            else:
                self.outstanding[msg_id] = control
        except queue.Full:
            self._print("ChannelThread: failed to pass command '%s' to socket" % request, category='err')

    def _process_request(self, action, data):
        # Always allow a halt to shut us down immediately, even if
        # we are in the middle of performing a clean operation
        if action == 'HALT':
            self.running = False
        # Clean was already initialized, don't try again and 
        # prevent new commands from being processed
        elif self.cleaning:
            return False
        # Remove all customized channel modifications
        elif action == 'CLEAN':
            self._print("ChannelThread: Cleaning > %d applied controls %s" % (len(list(self.applied.keys())), str(list(self.applied.keys()))), category='info')
            self._print("ChannelThread: Cleaning > %d outstanding controls %s" % (len(list(self.outstanding.keys())), str(list(self.outstanding.keys()))), category='info')
            self.cleaning = True
            for key in list(self.applied.keys()):
                control = self.applied[key]
                control.state = "DEFAULT"
                self._cmd_to_socket(control)
                del self.applied[key]
            for key in list(self.outstanding.keys()):
                control = self.outstanding[key]
                control.state = "DEFAULT"
                self._cmd_to_socket(control)
                del self.outstanding[key]
        elif action == 'CONTROL':
            self._cmd_to_socket(data)
        return True

    def _process_response(self, response):
        if response is None:
            # self._print("ChannelThread: Null response 'None'", category='dbg')
            return
        response = response.strip("\x00")
        responses = response.split('.')
        for rsp in responses:
            idx = rsp.find("CHANNELCONTROL-")
            if idx == -1:
                # self._print("ChannelThread: unrecognized response '%s'" % rsp, category='info')
                continue
            msg = rsp[idx:]
            # For some reason a standard strip() will not remove this character...
            if msg.find("CHANNELCONTROL-INVALID") > -1:
                self._print("ChannelThread: invalid command reported", category='err')
                continue
            try:
                _, id_str, result = [s.strip() for s in msg.split('-', 2)]
                msg_id = int(id_str)
            except:
                self._print("ChannelThread: invalid response '%s'" % msg, category='info')
                continue

            if self.cleaning:
                if msg_id not in self.clean_requests:
                    self._print("ChannelThread: unrecognized message id '%d'" % msg_id, category='info')
                    continue
                control = self.clean_requests[msg_id]
                del self.clean_requests[msg_id]
            else:
                if msg_id not in self.outstanding:
                    self._print("ChannelThread: unrecognized message id '%d'" % msg_id, category='info')
                    continue
                control = self.outstanding[msg_id]
                del self.outstanding[msg_id]
            if result == "OKAY":
                self.response_queue.put(("OKAY", control))
                if control.state != "DEFAULT":
                    self.applied[msg_id] = control
                message = "ChannelThread: archive server applied #%%d '%(context)s-%(state)s' to channel '%(station)s-%(location)s-%(channel)s'" % control
                self._print(message % msg_id, category='info')
            else:
                self.response_queue.put(("FAIL", control))
                message = "ChannelThread: archive server could not apply #%%d '%(context)s-%(state)s' to channel '%(station)s-%(location)s-%(channel)s'" % control
                self._print(message % msg_id, category='err')

    async def run(self):
        timeout = 5.0
        self.running = True
        new_request = False

        self.ctrl_socket = ControlSocket(self.server.address())

        self._print("ChannelThread: bound to localhost:%d" % self.ctrl_socket.getsockname()[1], category='info')
        while self.running:
            # self._print("ChannelThread: waiting on next event", category='dbg')
            # Monitor the comm socket and the notifier. The notifier
            # pulls us out of the select statement in order to check
            # for commands in the request_queue.
            # TODO: this will likely require further testing
            #  (and it's possible this works but is not efficient)
            try:
                await self.notifier.read()
                self.request_queue.put(await self.ctrl_socket.read())
            except socket.error as e:
                self._print(
                    f"ChannelThread: asyncore.loop() caught an exception: {str(e)}", category='err')
            # Process all new commands in the request_queue
            cmd = "..."
            while cmd:
                cmd, data = ("", None)
                try:
                    cmd, data = self.request_queue.get_nowait()
                except queue.Empty:
                    # self._print("ChannelThread: request queue empty", category='dbg')
                    pass
                if cmd:
                    self._print(f"ChannelThread: received command '{cmd}'", category='dbg')
                    if not self._process_request(cmd, data):
                        continue  # Process the next request

            # Process all responses from the connection
            rsp = "..."
            while rsp is not None:
                rsp = None
                try:
                    rsp = self.ctrl_socket.get(block=False)
                    # self._print("ChannelThread: response received '%s'" % rsp, category='dbg')
                except queue.Empty:
                    # self._print("ChannelThread: socket response queue empty", category='dbg')
                    continue

                self._process_response(rsp)

            if self.is_clean():
                if self.halting:
                    self._print("ChannelThread: halting thread now that we are clean", category='dbg')
                    # If there are no tasks remaining, the halting process is complete
                    self.running = False
                if self.cleaning:
                    # If cleaning is complete, allow new commands
                    self._print("ChannelThread: cleaning complete, releasing waiting threads", category='info')
                    self.cleaning = False
                    self.clean_complete.acquire()
                    self.clean_complete.notify_all()
                    self.clean_complete.release()

        # Inform the main thread that we are shutting down
        try:
            request = self.response_queue.put_nowait(("DONE", None))
            self._print("ChannelThread: thread complete", category='info')
        except queue.Full:
            pass
