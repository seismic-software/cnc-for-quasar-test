
from QAction import QAction
from QTools import QExReboot


class QReboot(QAction):
    def __init__(self):
        QAction.__init__(self)

    def _execute(self, q330):
        response = q330.reboot()
        raise QExReboot("Q330 Rebooting, Ping to Verify")
