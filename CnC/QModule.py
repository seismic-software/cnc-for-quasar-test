
import os

from QAction import QAction
from QTools import QExMessage

least = lambda a, b: ((a > b) and [b] or [a])[0]


class QModule(QAction):
    def __init__(self):
        self.private = [
            'action',
            'module_file',
        ]
        QAction.__init__(self)

        self.offset = 0
        self.bytes = 8192  # 8 KiB

    def val_action(self, value):
        if type(value) != str:
            raise TypeError('action')
        if value not in ['list', 'erase-all', 'erase-old', 'erase-new', 'install', 'running']:
            raise ValueError('action')
        return value

    def val_module_file(self, value):
        if type(value) != str:
            raise TypeError('module_file')
        if not os.path.exists(value):
            raise ValueError('module_file')
        return value

    def _execute(self, q330):
        if q330.getDeviceType() == 'Q335':
            raise QExMessage("Q335 architecture does not use modules.")

        # Download list of modules first so we can process them
        # We will need this information no matter what we are doing:
        # list      - show all modules
        # install   - verify there is sufficient room
        # erase     - determine which sectors contain modules

        if self.action == 'list':
            print("%s: Operation not implemented" % self.action)
            # print "All Q330 Modules:"
            # print
            # print q330.getAllModules()
        elif self.action == 'running':
            print("Running Q330 Modules:")
            print()
            print(q330.listRunningModules())
        else:
            print("%s: Operation not implemented" % self.action)
            runningModules = q330.getRunningModules()
            # allModules = q330.getAllModules()
            if self.action == 'install':
                print("%s: Operation not implemented" % self.action)
            elif self.action.startswith('erase-'):
                if self.action == 'erase-all':
                    print("%s: Operation not implemented" % self.action)
                elif self.action == 'old':
                    print("%s: Operation not implemented" % self.action)
                elif self.action == 'new':
                    print("%s: Operation not implemented" % self.action)
        self._print("Done.")
