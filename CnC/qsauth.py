
import optparse

from QScript import QScript
from QAuth import QAuth
from QTools import assert_arg_count, QExArgs


class QSAuth(QScript):
    def __init__(self):
        self.option_list = [
            optparse.make_option("-f", "--file", type="string", action="store", dest="file",
                                 help="auth codes are stored in this file according to the AUTH_FILE format above"),
        ]

        QScript.__init__(self)

    def local_usage(self):
        return """usage: %prog [options] <device>

    qauth will prompt the user to input the Q330 serial number and
    authorization codes from the command line unless an AUTH_FILE
    is supplied via the -f/--file argument.

AUTH_FILE   SER = <q330_serial_number>
            CFG = <config_port_auth_code>
            SPF = <special_functions_port_auth_code>
            DP1 = <data_port_1_auth_code>
            DP2 = <data_port_2_auth_code>
            DP3 = <data_port_3_auth_code>
            DP4 = <data_port_4_auth_code>

            The authorization codes and the Q330's serial number can be
            up to 16 hexidecimal digits. The serial number must match that
            of the Q330 being set. This is a safeguard against accidently
            overwriting the wrong Q330's authorization codes."""

    def _run(self):
        assert_arg_count(self.args, 'eq', 1)

        arg_device_id = self.args[0]
        self.comm.set_from_file(arg_device_id, self.options.reg_file)

        action = QAuth()
        try:
            if self.options.file:
                action.file = self.options.file
        except TypeError as msg:
            raise QExArgs("invalid type for argument '%s'" % msg.__str__())
        except ValueError as msg:
            raise QExArgs("invalid value for argument '%s'" % msg.__str__())

        self.comm.register()
        self.comm.execute(action)


script = QSAuth()
script.run()
