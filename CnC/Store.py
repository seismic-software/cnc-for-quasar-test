import os
from abc import abstractmethod

try:
    sqlite = None
    import MySQLdb as mysql
except ImportError:
    mysql = None
    try:
        # Try to import the built-in SQLite (Python 2.5+)
        import sqlite3 as sqlite
    except ImportError:
        sqlite = None

class StoreError(Exception):
    pass


class Store(object):
    MYSQL = (mysql is not None)
    SQLITE = (sqlite is not None)

    def __init__(self):
        if self.__class__.__name__ == 'Store':
            raise NotImplementedError("Store is an abstract class")

    @abstractmethod
    def read(self, data):
        raise NotImplementedError("Store::read is an abstract method")

    @abstractmethod
    def write(self, data):
        raise NotImplementedError("Store::write is an abstract method")


class StoreDB(Store):
    def __init__(self):
        if self.__class__.__name__ == 'StoreDB':
            raise NotImplementedError("StoreDB is an abstract class")
        self._prepare_database()
        Store.__init__(self)

    def _prepare_database(self):
        raise NotImplementedError("StoreDB::_prepare_database is an abstract method")

    def write(self, query, data):
        self.cur.execute(query, data)
        self.cur.commit()

    def read(self, query, data=None):
        if data:
            self.cur.execute(query, data)
        else:
            self.cur.execute(query)
        return self.cur.fetchall()


class StoreMySQL(StoreDB):
    def __init__(self, host, database, user='', password='', port=3306):
        if mysql is not None:
            raise Exception("MySQL module not found")
        self.host = host
        self.database = database
        self.user = user
        self.password = password
        self.port = port
        StoreDB.__init__(self)

        self._prepare_database()

    def _prepare_database(self):
        try:
            self.db = mysql.connect(host=self.host,
                                    user=self.user,
                                    passwd=self.password,
                                    db=self.database,
                                    port=int(self.port))
        except Exception as e:
            raise StoreError("StoreMySQL caught exception %s: %s" % (e.__class__.__name__, str(e)))
        self.cur = self.db.cursor()


class StoreSQLite(StoreDB):
    def __init__(self, dbfile):
        if sqlite is not None:
            raise Exception("SQLite module not found")
        self.dbfile = dbfile
        StoreDB.__init__(self)

    def _prepare_database(self):
        try:
            self.db = sqlite.connect(self.dbfile)
        except Exception as e:
            raise StoreError("StoreSQLite caught exception %s: %s" % (e.__class__.__name__, str(e)))
        self.cur = self.db.cursor()


class StoreFile(Store):
    def __init__(self, file):
        self.file = file
        Store.__init__(self)

    def write(self, data, force=False):
        if os.path.isfile(self.file):
            if not force:
                raise IOError("%s: file exists")
        elif os.path.exists(self.file):
            raise IOError("%s: path exists, but it is not a regular file")
        fh = open(self.file, 'w+')
        fh.write(data)
        fh.close()

    def read(self, data=None):
        fh = open(self.file, 'r')
        data = fh.read()
        fh.close()
        return data
