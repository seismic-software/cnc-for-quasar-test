#!/usr/bin/python
import compileall
import os
import sys

def compile_dir(directory):
    if os.path.exists(directory):
        compileall.compile_dir(directory)
    else:
        raise IOError("%s: no such file or directory" % directory)

if __name__ == '__main__':
    directory = os.getcwd() + "/" + "util"
    compile_dir(directory)

