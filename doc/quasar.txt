
Slate Layout:
/opt/util/lib/python/isti/
/opt/util/lib/python/Quasar/
/opt/util/lib/python/QComm.py
/opt/util/lib/python/QStatus.py
/opt/util/lib/python/QTools.py
/opt/util/bin/
/home/maint/.bash_profile 
/etc/q330/qregister.config


            isti -- should only be updated in the event that a bug is detected, 
                    very rarely if ever.
          Quasar -- will see updates when changes are made to the Q330 
                    communication protocol.
       QClass.py -- base class for all CnC operation classes
        QComm.py -- supplies access to all the Q330 control and status commands.
      QScript.py -- base class for all CnC scripts
      QStatus.py -- formats the results of status requests such that they are 
                    human readible.
       QTools.py -- provides a useful suite of tools for formatting and 
                    converting data.
qregister.config -- contains the information necessary to register with a Q330.
   .bash_profile -- requires the following lines:
                        export PYTHONPATH="/opt/util/lib/python"
                        export PATH="${PATH}:/opt/util/bin"

Qu@sar Updates:
- c1_stat  - needs some work on the "Baler/Dial Status and Timeouts" section
             from what I can see the documentation is a little fuzzy, bit values are
             reversed or don't match up in the most recent documentation (dates back
             to changes in v14)
           - have added support for DynamicIPAddress, AuxBoard, and SerialSensor
- c1_ping  - updated to handle the new packet variations
- c1_fix   - updated flags
- c1_sman  - updated flags
- c1_web   - now allows for a second command format
- c1_free  - second structure "blocks 248-255" should be "blocks 224-255"
- c2_snapt - created class for this packet which was added in version 17
- c2_sphy  - consider creating helper functions for setting and clearing flags
- c2_sbpwr - consider creating helper functions for setting and clearing flags
- c2_annc  - consider creating helper functions for printing flags

In v13
------
C2_TERC
C2_TERR

In v20
------
C2_DEP
C2_DEPR

