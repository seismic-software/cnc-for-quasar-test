Q330 Communications Protocol

[Q->D]dt_data       - Data Record
[Q->D]dt_fill       - Fill Packet
[D->Q]dt_dack       - Data Acknowledge
[D->Q]dt_open       - Open Data Port

[Q->D]c1_cack       - Command Acknowledge (command succeeded)
[D->Q]c1_rqsrv      - Request Server Registration
[Q->D]c1_srvch      - Server Challenge
[D->Q]c1_srvrsp     - Server Response
[Q->D]c1_cerr       - Command Error
[D->Q]c1_dsrv       - Delete Server
[D->Q]c1_sauth      - Set Authorization Codes
[D->Q]c1_pollsn     - Poll for Serial Number
[Q->D]c1_mysn       - My Serial Number (response to c1_pollsn)
[D->Q]c1_sphy       - Set Physical Interfaces
[D->Q]c1_rqphy      - Request Physical Interfaces
[Q->D]c1_phy        - Physical Interfaces (response to c1_rqphy)
[D->Q]c1_slog       - Set Data Port
[D->Q]c1_rqlog      - Request Data Port
[Q->D]c1_log        - Data Port (response to c1_rqlog)
[D->Q]c1_ctrl       - Control Q330 Operation
[D->Q]c1_sglob      - Set Global Programming
[D->Q]c1_rqglob     - Request GLobal Programming
[Q->D]c1_glob       - Global Programming (response to c1_rqglob)
[D->Q]c1_rqfix      - Rqeust Fixed Values after Reboot
[Q->D]c1_fix        - Fixed Values after Reboot (response to c1_rqfix)
[D->Q]c1_sman       - Set Manufacturer's Area
[D->Q]c1_rqman      - Request Manufacturer's Area
[Q->D]c1_man        - Manufacturer's Area (response to c1_rqman)
[D->Q]c1_rqstat     - Request Status
[Q->D]c1_stat       - Status (response to c1_rqstat)
[D->Q]c1_wstat      - Write to Status Port
[D->Q]c1_vco        - Set VCO
[D->Q]c1_pulse      - Pulse sensor control line(s)
[D->Q]c1_qcal       - Start QCAL330 Calibration
[D->Q]c1_stop       - Stop Calibration
[D->Q]c1_rqrt       - Request Routing Table
[Q->D]c1_rt         - Routing Table (response to c1_rqrt)
[D->Q]c1_mrt        - Modify Routing Table
[D->Q]c1_rqthn      - Request Thread Names
[Q->D]c1_thn        - Thread Names (response to c1_rqthn)
[D->Q]c1_rqgid      - Request GPS ID Strings
[Q->D]c1_gid        - GPS ID Strings (response to cq_rqgid)
[D->Q]c1_scnp       - Send CNP Message
[Q->D]c1_rcnp       - CNP Reply Message (response to c1_scnp)
[D->Q]c1_srtc       - Set Real Time Clock
[D->Q]c1_sout       - Set Output Bits
[D->Q]c1_sspp       - Set Slave Processor Parameters
[D->Q]c1_rqssp      - Request Slave Processor Parameters
[Q->D]c1_ssp        - Slave Processor Parameters (response to c1_rqssp)
[D->Q]c1_ssc        - Set Sensor Control Mapping
[D->Q]c1_rqsc       - Request Sensor Control Mapping
[Q->D]c1_sc         - Sensor Control Mapping (response to c1_rqsc)
[D->Q]c1_umsg       - Send User Message
[D->Q]c1_web        - Set Web Server Link
[D->Q]c1_rqflgs     - Request Combination Packet
[Q->D]c1_flgs       - Combination Packet (response to c1_rqflgs)
[D->Q]c1_rqdcp      - Request Digitizer Calibration Packet
[Q->D]c1_dcp        - Digitizer Calibration Packet (response to c1_rqdcp)
[D->Q]c1_rqdev      - Request CNP Device Info
[Q->D]c1_dev        - CNP Device Info (response to c1_rqdev)
[D->Q]c1_sdev       - Set Device Options
[D->Q]c1_ping       - PING Q330
[D->Q]c1_smem       - Set Memory Contents
[D->Q]c1_rqmem      - Request Memory Contents
[Q->D]c1_mem        - Memory Contents (response to c1_mem)
[D->Q]c1_erase      - Erase Flash Sectors
[D->Q]c1_rqmod      - Request Memory Module Map
[Q->D]c1_mod        - Memory Module Map (response to c1_mod)
[D->Q]c1_rqfree     - Request Free Flash Memory
[Q->D]c1_free       - Free Flash Memory (response to c1_free)
                    
[D->Q]c2_sphy       - Set Physical Interface
[D->Q]c2_rqphy      - Request Physical Interface
[Q->D]c2_phy        - Physical Interface (response to c2_rqphy)
[D->Q]c2_sgps       - Set GPS Parameters
[D->Q]c2_rqgps      - Request GPS Parameters
[Q->D]c2_gps        - GPS Parameters (response to c2_rqgps)
[D->Q]c2_swin       - Set Recording Windows
[D->Q]c2_rqwin      - Request Recording Windows
[Q->D]c2_win        - Recording Windows (response to c2_rqwin)
[D->Q]c2_samass     - Automatic Mass Re-centering
[D->Q]c2_rqamass    - Request Automatic Mass Re-centering
[Q->D]c2_amass      - Automatic Mass Re-centering (response to c2_rqamass)
[D->Q]c2_sbpwr      - Set Baler Power and Dialer Control
[Q->P]c2_poc        - Point of Contact 
[Q->D]c2_back       - Baler Acknowledge 
[V->D]c2_vack       - Vacuum Acknowledge 
[D->Q|V]c2_brdy     - Baler Ready 
[B->Q]c2_boff       - Baler Off 
[V->D]c2_bcmd       - Baler Command 
[D->V]c2_bresp      - Baler Response 
[D->Q]c2_regchk     - Registration Check
[Q->D]c2_regresp    - Registration Response (response to c2_regchk)
[I->Q]c2_inst       - Installer Command
[D->Q]c2_rqqv       - QuickView Request
[Q->D]c2_qv         - QuickView Response
[D->Q]c2_rqmd5      - Request MD5 Result
[Q->D]c2_md5        - MD5 Result (response to c2_rqmd5)
[B->Q]c2_snapt      - Set NAPT Ports
[D->Q]c2_terc       - Tertiary Commands
[Q->D]c2_terr       - Tertiary Responses (response to c2_terc)

[Q->D]c3_annc       - Announce Structure
[D->Q]c3_rqannc     - Request Announce Structure
[D->Q]c3_sannc      - Set Announce Structure

