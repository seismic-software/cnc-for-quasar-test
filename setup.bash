#!/usr/bin/env bash

VENV=venv

echo "Removing any old virtual environment"
rm -rf $VENV

echo "Install virtualenv on system"
pip3 install --find-links=wheel virtualenv

echo "Setting up virtualenv"
virtualenv -p "$(command -v python3.11 || command -v python3.10 || command -v python3.9 || command -v python3.8 )" ${VENV}

source $VENV/bin/activate

# Install pyserial
pip3 pypi.org install --find-links=wheel pyserial

# Build and install quasar
cd quasar
pip install .

deactivate
